using Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class Config
    {
        //Algorithm
        public static readonly double RefVolumeUsd = 10;
        public static readonly int MaxEdgesInCycle = 4;
        public static readonly double NumericEpsilon = 1e-12;
        public static readonly double MaxFlowSizeAllowedUsd = 1000;
        public static double AskReplySafetyFactor = 0.9;
        public static readonly double MinUsdProfitForExecution = 0.005;
        public static readonly double MinRateForExecution = 1.001;
        public static readonly TimeSpan CooldownPeriod = TimeSpan.FromSeconds(2);
        public static readonly int MaxQueueBeforeExecution = 1000;
        public static readonly bool SendExecutionRequests = false;
        public static readonly bool UseDummyFunds = false;
        public static readonly bool PreferShorterCycles = false;
        public static readonly int MaxFlowCalculationLimit = 10;
        public static readonly double MinFundSourceThresholdUsd = 0.5;

        //API
        public static bool WebsocketsEnabled = true;
        public static readonly double WebsocketHighLatencyThresholdMsec = 1000.0;
        public static readonly int MaxOrdersInExecution = 20;
        public static readonly int MaxHttpBufferLen = (int)Math.Pow(2, 10); // 1KB
        public static readonly int HttpMaxRetries = 4;
        public static readonly int NumOfDuplicatesAfterAskReply = 2;
        public static readonly bool ContinueExecutionOnFailedOrders = true;
        public static readonly int MaxNumOrderUpdates = 10;
        public static readonly string BalanceFixerMarketCurrency = "BTC";

        //Sampling
        public static readonly bool SampleEdgesInExecution = true;
        public static readonly int NumberOfPricesToSample = 4;
        public static readonly TimeSpan SampleWindowDuration = TimeSpan.FromSeconds(2);

        //Graph filters
        public static readonly bool ExecuteInternalCycleGroups = false;
        public static readonly bool RestrictAskReplyToFinalCmd = false;

        public static List<ExchangeName> EnabledExchanges = new List<ExchangeName>
        {
            ExchangeName.HitBTC,
            ExchangeName.Binance,
            ExchangeName.Huobi,
            ExchangeName.OKEX,
        };

        public static readonly HashSet<ExchangeName> ExchangesWithMarketCmd = new HashSet<ExchangeName>()
        {
            ExchangeName.Binance, ExchangeName.HitBTC
        };



        public static readonly Dictionary<ExchangeName, HashSet<string>> ExcludedNodes = new Dictionary<ExchangeName, HashSet<string>>
        {
            { ExchangeName.HitBTC, new HashSet<string> { "BTM", "NANO", "TRX", "ICX", "STEEM" } },
            { ExchangeName.Binance, new HashSet<string> { "NANO", "TRX", "STEEM", "GAS", "BCN", "HOT", "BCD", "BCX", "SBTC" } },
            { ExchangeName.Huobi, new HashSet<string> { "BTG", "BTM", "TRX", "ICX", "BCD", "BCX", "SBTC" } },
            { ExchangeName.OKEX, new HashSet<string> { "HOT", "SBTC", "BCD", "BCX", "YOYO", "YOYOW", "NULS", "LSK", "BCN" } },
        };

        public static readonly bool ExecuteMarketCurrenciesCycleGroups = true;
        public static readonly HashSet<string> MarketCurrencies = new HashSet<string>()
        {
            "BTC", "ETH", "USDT"
        };

        public static readonly bool FilterByAllowedCrossNodes = false;
        public static readonly HashSet<HashSet<NodeKey>> AllowedCrossNodesGroups = new HashSet<HashSet<NodeKey>>(HashSet<NodeKey>.CreateSetComparer())
        {
            new HashSet<NodeKey> { new NodeKey(ExchangeName.Huobi, "BTC"), new NodeKey(ExchangeName.OKEX, "LET") },
        };

        public static readonly bool FilterByRequiredCrossNodes = false;
        public static readonly HashSet<NodeKey> RequiredCrossNodesGroups = new HashSet<NodeKey>()
        {
            new NodeKey(ExchangeName.Binance, "GTO"),
            new NodeKey(ExchangeName.Binance, "SALT"),
            new NodeKey(ExchangeName.Binance, "OAX"),
            new NodeKey(ExchangeName.Binance, "EOS"),
            new NodeKey(ExchangeName.Binance, "NAS"),
            new NodeKey(ExchangeName.Binance, "LTC"),

            new NodeKey(ExchangeName.HitBTC, "GTO"),
            new NodeKey(ExchangeName.HitBTC, "SALT"),
            new NodeKey(ExchangeName.HitBTC, "OAX"),
            new NodeKey(ExchangeName.HitBTC, "EOS"),
            new NodeKey(ExchangeName.HitBTC, "NAS"),
            new NodeKey(ExchangeName.HitBTC, "LTC"),

            new NodeKey(ExchangeName.Huobi, "GTO"),
            new NodeKey(ExchangeName.Huobi, "SALT"),
            new NodeKey(ExchangeName.Huobi, "OAX"),
            new NodeKey(ExchangeName.Huobi, "EOS"),
            new NodeKey(ExchangeName.Huobi, "NAS"),
            new NodeKey(ExchangeName.Huobi, "LTC"),

            new NodeKey(ExchangeName.OKEX, "GTO"),
            new NodeKey(ExchangeName.OKEX, "SALT"),
            new NodeKey(ExchangeName.OKEX, "OAX"),
            new NodeKey(ExchangeName.OKEX, "EOS"),
            new NodeKey(ExchangeName.OKEX, "NAS"),
            new NodeKey(ExchangeName.OKEX, "LTC"),
        };

    }
}
