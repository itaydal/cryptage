﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Common
{
    public class ConsoleWriter
    {
        private static ConsoleWriter _instance = new ConsoleWriter();
        private Thread _messageThread;

        private ConcurrentQueue<(string, bool)> _consoleWriteQueue = new ConcurrentQueue<(string, bool)>();

        public ConsoleWriter()
        {
            _messageThread = new Thread(MessageLoop);
            _messageThread.Start();
        }

        private void MessageLoop()
        {
            while (true)
            {
                if (_consoleWriteQueue.TryDequeue(out (string, bool) msg))
                {
                    if (msg.Item2)
                        Console.WriteLine(msg.Item1);
                    else
                        Console.Write(msg.Item1);
                }
                else
                    Thread.Sleep(1);
            }
        }

        public static void Write(string s)
        {
            _instance._consoleWriteQueue.Enqueue((s, false));
        }
        public static void Write(string s, object arg0)
        {
            _instance._consoleWriteQueue.Enqueue((string.Format(s, arg0), false));
        }
        public static void Write(string s, object arg0, object arg1)
        {
            _instance._consoleWriteQueue.Enqueue((string.Format(s, arg0, arg1), false));
        }
        public static void Write(string s, object arg0, object arg1, object arg2)
        {
            _instance._consoleWriteQueue.Enqueue((string.Format(s, arg0, arg1, arg2), false));
        }
        public static void Write(string s, params object[] args)
        {
            _instance._consoleWriteQueue.Enqueue((string.Format(s, args), false));
        }
        public static void Write(object obj)
        {
            _instance._consoleWriteQueue.Enqueue((obj.ToString(), false));
        }

        public static void WriteLine(string s)
        {
            _instance._consoleWriteQueue.Enqueue((s, true));
        }
        public static void WriteLine(string s, object arg0)
        {
            _instance._consoleWriteQueue.Enqueue((string.Format(s, arg0), true));
        }
        public static void WriteLine(string s, object arg0, object arg1)
        {
            _instance._consoleWriteQueue.Enqueue((string.Format(s, arg0, arg1), true));
        }
        public static void WriteLine(string s, object arg0, object arg1, object arg2)
        {
            _instance._consoleWriteQueue.Enqueue((string.Format(s, arg0, arg1, arg2), true));
        }
        public static void WriteLine(string s, params object[] args)
        {
            _instance._consoleWriteQueue.Enqueue((string.Format(s, args), true));
        }
        public static void WriteLine(object obj)
        {
            _instance._consoleWriteQueue.Enqueue((obj.ToString(), true));
        }
        public static void WriteLine()
        {
            _instance._consoleWriteQueue.Enqueue((string.Empty, true));
        }
    }
}
