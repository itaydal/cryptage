﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using GuerrillaNtp;

namespace Common
{
    public class NtpAgent
    {
        private static NtpAgent _instance = new NtpAgent();
        public static NtpAgent GetInstance()
        {
            return _instance;
        }

        private static string NtpServerAddress = "pool.ntp.org";
        private static int QueryIntervalMinutes = 2;

        private long accurateTimeOffset = 0;

        public static DateTime GetAccurateCurrentUnixTime()
        {
            return DateTime.UtcNow.AddMilliseconds(Interlocked.Read(ref _instance.accurateTimeOffset));
        }

        private NtpAgent()
        {
            new Thread(() =>
            {
                using (var ntp = new NtpClient(Dns.GetHostAddresses(NtpServerAddress)[0]))
                {
                    while (true)
                    {
                        int retries = 3;
                        TimeSpan offset = TimeSpan.Zero;
                        while (retries > 0)
                        {
                            try
                            {
                                offset = ntp.GetCorrectionOffset();
                                break;
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("{0}: NtpAgent: {1}", DateTime.Now, e.Message);
                                retries--;
                            }
                        }
                        accurateTimeOffset = (long)offset.TotalMilliseconds;

                        Thread.Sleep(QueryIntervalMinutes * 60 * 1000);
                    }
                }
            }).Start();
            
        }
    }
}
