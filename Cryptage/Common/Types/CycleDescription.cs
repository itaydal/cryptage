﻿using System;
using System.Text;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace Common.Types
{
    public class CycleDescription
    {
        [BsonId]
        public int cacheId = -1;
        private int hashCode = 0;

        [BsonIgnore]
        public EdgeKey[] EdgeKeys { get; private set; }
        public int[] EdgeKeysIds { get; private set; }


        public CycleDescription()
        {

        }
        public CycleDescription(int edgeCount)
        {
            this.EdgeKeys = new EdgeKey[edgeCount];
        }

        public void SetEdges(IEnumerable<EdgeKey> edgeKeys)
        {
            this.EdgeKeys = edgeKeys.ToArray();
            FinalizeCycle();
        }

        public void SetEdgeIds(IEnumerable<int> edgeKeyIds)
        {
            this.EdgeKeysIds = edgeKeyIds.ToArray();
        }

        private void FinalizeCycle()
        {
            var firstEdgeIdx = EdgeKeys.Select((e, i) => (e.ToString(), i)).Min().Item2;
            Utils.LeftShiftArray(EdgeKeys, firstEdgeIdx);
            if (EdgeKeysIds != null)
                Utils.LeftShiftArray(EdgeKeysIds, firstEdgeIdx);

            hashCode = ToString().GetHashCode();
        }

        public void PushEdge(EdgeKey edge, int position)
        {
            EdgeKeys[position] = edge;

            if (position == 0)
                FinalizeCycle();
        }

        public override string ToString()
        {
            ToStringCounter++; //TODO: remove

            int EdgeCount = EdgeKeys.Length;
            if (EdgeCount == 2)
            {
                return string.Format(TwoEdgeCycleFormat,
                    EdgeKeys[0].SrcNode.Currency,
                    EdgeKeys[0].Exchange, EdgeKeys[0].IsBidBook ? "Bid" : "Ask",
                    EdgeKeys[0].DstNode.Currency,
                    EdgeKeys[1].Exchange, EdgeKeys[1].IsBidBook ? "Bid" : "Ask",
                    EdgeKeys[1].DstNode.Currency);
            }
            else if (EdgeCount == 3)
            {
                return string.Format(ThreeEdgeCycleFormat,
                    EdgeKeys[0].SrcNode.Currency,
                    EdgeKeys[0].Exchange, EdgeKeys[0].IsBidBook ? "Bid" : "Ask",
                    EdgeKeys[0].DstNode.Currency,
                    EdgeKeys[1].Exchange, EdgeKeys[1].IsBidBook ? "Bid" : "Ask",
                    EdgeKeys[1].DstNode.Currency,
                    EdgeKeys[2].Exchange, EdgeKeys[2].IsBidBook ? "Bid" : "Ask",
                    EdgeKeys[2].DstNode.Currency);
            }
            else if (EdgeCount == 4)
            {
                return string.Format(FourEdgeCycleFormat,
                    EdgeKeys[0].SrcNode.Currency,
                    EdgeKeys[0].Exchange, EdgeKeys[0].IsBidBook ? "Bid" : "Ask",
                    EdgeKeys[0].DstNode.Currency,
                    EdgeKeys[1].Exchange, EdgeKeys[1].IsBidBook ? "Bid" : "Ask",
                    EdgeKeys[1].DstNode.Currency,
                    EdgeKeys[2].Exchange, EdgeKeys[2].IsBidBook ? "Bid" : "Ask",
                    EdgeKeys[2].DstNode.Currency,
                    EdgeKeys[3].Exchange, EdgeKeys[3].IsBidBook ? "Bid" : "Ask",
                    EdgeKeys[3].DstNode.Currency);
            }
            else
            {
                var sb = new StringBuilder();
                sb.Append(EdgeKeys[0].SrcNode.Currency);
                foreach (var e in EdgeKeys)
                {
                    sb.Append("-->");
                    sb.Append(String.Format("({0}-{1})", e.Exchange, e.IsBidBook ? "Bid" : "Ask"));
                    sb.Append("-->");
                    sb.Append(e.DstNode.Currency);
                }
                return sb.ToString();
            }
        }

        public override bool Equals(object obj)
        {
            return obj.GetHashCode() == this.GetHashCode() && this.ToString().Equals(obj.ToString());
        }

        public override int GetHashCode()
        {
            return this.hashCode;
        }

        public static int ToStringCounter = 0;

        private static readonly string TwoEdgeCycleFormat = "{0}-->({1}-{2})-->{3}-->({4}-{5})-->{6}";
        private static readonly string ThreeEdgeCycleFormat = "{0}-->({1}-{2})-->{3}-->({4}-{5})-->{6}-->({7}-{8})-->{9}";
        private static readonly string FourEdgeCycleFormat = "{0}-->({1}-{2})-->{3}-->({4}-{5})-->{6}-->({7}-{8})-->{9}-->({10}-{11})-->{12}";
    }
}
