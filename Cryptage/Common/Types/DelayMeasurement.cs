﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Types
{
    public class DelayMeasurement
    {
        public DateTime ArrivalTime { get; private set; }
        public string Host { get; private set; }
        private List<(string stage, double delay)> _measurements = new List<(string stage, double delay)>();
        private static readonly double DelayThresholdMsec = 5000.0;

        public DelayMeasurement(string host)
        {
            Host = host;
            ArrivalTime = NtpAgent.GetAccurateCurrentUnixTime();
        }
        public DelayMeasurement(string host, DateTime arrivalTime)
        {
            Host = host;
            ArrivalTime = arrivalTime;
        }

        public void Add(string stageName)
        {
            (string stage, double delay) measurment = 
                (stageName, (NtpAgent.GetAccurateCurrentUnixTime() - ArrivalTime).TotalMilliseconds);
            if (measurment.delay > DelayThresholdMsec)
                ConsoleWriter.WriteLine("{0}: Large delay ({1}[msec]) at stage {2}", Host, measurment.delay, measurment.stage);
            _measurements.Add(measurment);
        }

        public override string ToString()
        {
            string res = string.Format("{0}: Arrival Time: {1}\r\n", Host, ArrivalTime);
            foreach (var (stage, delay) in _measurements)
            {
                res += string.Format("{0}: {1}[msec]\r\n", stage, delay);
            }
            return res;
        }


    }
}
