﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Types;

namespace Common.Types
{
    public class EdgeInfo
    {
        public double MinSize { get; private set; }
        public double FeeFactor { get; private set; }

        public EdgeInfo(double minSize, double feeFactor)
        {
            this.MinSize = minSize;
            this.FeeFactor = feeFactor;
        }
    }

    public class EdgeInfoUpdate
    {
        public Dictionary<EdgeKey, EdgeInfo> EdgeInfos { get; set; }

        public EdgeInfoUpdate(Dictionary<EdgeKey, EdgeInfo> edgeInfos)
        {
            EdgeInfos = edgeInfos;
        }
    }
}
