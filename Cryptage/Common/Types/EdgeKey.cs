﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Common.Types
{
    public class EdgeKey
    {
        [BsonId]
        public int cacheId = -1;

        private bool privateFieldsSet = false;
        private int hashCode = 0;
        private string stringRep;

        [BsonRepresentation(BsonType.String)]
        public ExchangeName Exchange { get; private set; }
        public string BaseCurrency { get; private set; }
        public string QuoteCurrency { get; private set; }
        public bool IsBidBook { get; private set; }
        public NodeKey SrcNode { get; private set; }
        public string SrcCurrency { get; private set; }
        public NodeKey DstNode { get; private set; }
        public string DstCurrency { get; private set; }

        public EdgeKey(ExchangeName exchange, string baseCurrency, string quoteCurrency, bool isBidBook)
        {
            Exchange = exchange;
            BaseCurrency = baseCurrency;
            QuoteCurrency = quoteCurrency;
            IsBidBook = isBidBook;


            SrcCurrency = IsBidBook ? BaseCurrency : QuoteCurrency;
            SrcNode = new NodeKey(Exchange, SrcCurrency);
            DstCurrency = !IsBidBook ? BaseCurrency : QuoteCurrency;
            DstNode = new NodeKey(Exchange, DstCurrency);

            SetPrivateFields();
        }

        private void SetPrivateFields()
        {
            if (privateFieldsSet)
                return;

            this.stringRep = string.Format("Edge: {0}-{1} in exchange {2}, {3}", 
                                           BaseCurrency, QuoteCurrency, Exchange, 
                                           IsBidBook ? "Bid Book" : "Ask Book");
            this.hashCode = this.stringRep.GetHashCode();
            this.privateFieldsSet = true;
        }

        public override int GetHashCode()
        {
            //TODO: make sure that haveing multiple objects with the same hash code does not mess with GC
            if (!privateFieldsSet)
                SetPrivateFields();

            return hashCode;
        }

        public override bool Equals(object obj)
        {
            if (!privateFieldsSet)
                SetPrivateFields();

            return obj.GetHashCode() == hashCode && this.ToString().Equals(obj.ToString());
        }

        public override string ToString()
        {
            if (!privateFieldsSet)
                SetPrivateFields();

            return stringRep;
        }

    }
}
