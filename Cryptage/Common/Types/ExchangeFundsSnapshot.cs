﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Types
{
    public class ExchangeFundsSnapshot
    {
        public ExchangeName Exchange { get; private set; }
        public Dictionary<string, double> CurrencyFunds { get; private set; }

        public ExchangeFundsSnapshot(ExchangeName exchange, Dictionary<string, double> currencyFunds)
        {
            this.Exchange = exchange;
            this.CurrencyFunds = currencyFunds;
        }
        
    }
}
