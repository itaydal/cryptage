﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Types
{
    public enum ExchangeName : byte
    {
        HitBTC,
        Poloniex,
        Bitfinex,
        Binance,
        Bittrex,
        OKEX,
        Huobi,
        Bibox,
        CoinTiger,
        BitBay
    }

    public class ExchangeNameTranslator
    {
        private static Dictionary<string, ExchangeName> StringToExchangeNameDict = new Dictionary<string, ExchangeName>
        {
            {"HitBTC", ExchangeName.HitBTC },
            {"Poloniex", ExchangeName.Poloniex },
            {"Bitfinex", ExchangeName.Bitfinex },
            {"Binance", ExchangeName.Binance },
            {"Bittrex", ExchangeName.Bittrex },
            {"OKEX", ExchangeName.OKEX },
            {"Huobi", ExchangeName.Huobi },
            {"Bibox", ExchangeName.Bibox },
            {"CoinTiger", ExchangeName.CoinTiger },
            {"BitBay", ExchangeName.BitBay }
        };

        public static List<ExchangeName> ToList()
        {
            return StringToExchangeNameDict.Values.ToList();
        }

        public static ExchangeName StringToExchangeName(string name)
        {
            return StringToExchangeNameDict[name];
        }

        public static string ExchangeNameToString(ExchangeName exchangeName)
        {
            foreach (var item in StringToExchangeNameDict)
            {
                if (item.Value == exchangeName)
                    return item.Key;
            }
            return null;
        }
    }
}
