﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Types;

namespace Common.Types
{
    public class ExecutionRequest
    {
        public Guid ExecutionCycleId { get; set; }
        public ExchangeName Exchange { get; private set; }
        public List<ExecutionCommand> Commands { get; private set; }
        public DelayMeasurement TriggerUpdateMeasurement { get; private set; }

        public ExecutionRequest(Guid executionCycleId, ExchangeName exchange, DelayMeasurement triggerUpdateMeasurement=null)
        {
            this.ExecutionCycleId = executionCycleId;
            this.Exchange = exchange;
            this.Commands = new List<ExecutionCommand>();
            this.TriggerUpdateMeasurement = triggerUpdateMeasurement;
        }
    }

    public class ExecutionCommand
    {
        public EdgeKey Edge;
        public RateItem AdjustedRateItem;
        public RateItem NonAdjustedRateItem;
        public bool IsStandaloneFundedSrc;

        public ExecutionCommand(EdgeKey edge, RateItem adjustedRateItem, RateItem nonAdjustedRateItem, bool isStandaloneFundedSrc)
        {
            this.Edge = edge;
            this.AdjustedRateItem = adjustedRateItem;
            this.NonAdjustedRateItem = nonAdjustedRateItem;
            this.IsStandaloneFundedSrc = isStandaloneFundedSrc;
        }
    }

    public class ExecutionReport
    {
        public Guid ExecutionCycleId { get; set; }
        public ExchangeName Exchange { get; private set; }
        public List<Tuple<EdgeKey, RateItem>> ExecutionResult { get; private set; }
        public bool IsSuccessful { get; set; }

        public ExecutionReport(Guid executionCycleId, ExchangeName exchange, 
            List<Tuple<EdgeKey, RateItem>> executionResult, bool isSuccessful)
        {
            ExecutionCycleId = executionCycleId;
            Exchange = exchange;
            ExecutionResult = executionResult;
            IsSuccessful = isSuccessful;
            //TODO: add enum to specify possible failiure reasone, e.g. InsufficientFunds, NetworkError, etc..
        }
    }

}
