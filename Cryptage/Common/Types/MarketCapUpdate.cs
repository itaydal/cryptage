﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Types
{
    public class MarketCapItem
    {
        public string Symbol { get; private set; }
        public double PriceUsd { get; private set; }
        public double PriceBTC { get; private set; }
        public double RelativeMarketCap { get; private set; }
        public int Rank { get; private set; }
        public bool IsCrypto { get; private set; }

        public MarketCapItem(string symbol, double priceUsd, double priceBTC, double relativeMarketCap, int rank, bool isCrypto)
        {
            Symbol = symbol;
            PriceUsd = priceUsd;
            PriceBTC = priceBTC;
            RelativeMarketCap = relativeMarketCap;
            Rank = rank;
            IsCrypto = isCrypto;
        }
    }

    public class MarketCapUpdate
    {
        public Dictionary<string, MarketCapItem> MarketCapItems { get; private set; }

        public MarketCapUpdate(Dictionary<string, MarketCapItem> marketCapItems)
        {
            MarketCapItems = marketCapItems;
        }
    }
}
