﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Common.Types
{
    public class NodeKey
    {
        private bool privateFieldsSet = false;
        private int hashCode = 0;
        private string stringRep;

        [BsonRepresentation(BsonType.String)]
        public ExchangeName Exchange { get; private set; }
        public string Currency { get; private set; }

        public NodeKey(ExchangeName exchange, string currency)
        {
            this.Exchange = exchange;
            this.Currency = currency;

            SetPrivateFields();
        }

        private void SetPrivateFields()
        {
            if (privateFieldsSet)
                return;

            this.stringRep = String.Format("Node: {0} ({1})", Currency, Exchange);
            this.hashCode = this.stringRep.GetHashCode();
            this.privateFieldsSet = true;
        }

        public override int GetHashCode()
        {
            if (!privateFieldsSet)
                SetPrivateFields();

            return hashCode;
        }

        public override bool Equals(object obj)
        {
            if (!privateFieldsSet)
                SetPrivateFields();

            return obj.GetHashCode() == hashCode && this.ToString().Equals(obj.ToString());
        }

        public override string ToString()
        {
            if (!privateFieldsSet)
                SetPrivateFields();

            return stringRep;
        }
    }
}
