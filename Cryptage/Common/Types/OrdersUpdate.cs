﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Types;

namespace Common.Types
{
    public class OrdersUpdate
    {
        public List<Tuple<EdgeKey, IDictionary<double, double>>> Orders { get; private set; }
        public bool OverrideOrderbooks { get; set; }
        public DelayMeasurement Measurement { get; private set; }
        public TimeSpan Latency { get; private set; }

        public bool IsDiffUpdate { get; private set; }

        public void Add(EdgeKey edgeKey, IDictionary<double, double> update)
        {
            Orders.Add(new Tuple<EdgeKey, IDictionary<double, double>>(edgeKey, update));
        }

        public OrdersUpdate(bool overrideOrderbooks, DelayMeasurement measurement, TimeSpan latency, bool isDiffUpdate=false)
        {
            if (overrideOrderbooks && isDiffUpdate)
                throw new Exception("Snapshot update cannot be differential");

            Orders = new List<Tuple<EdgeKey, IDictionary<double, double>>>();
            OverrideOrderbooks = overrideOrderbooks;
            Measurement = measurement;
            Latency = latency;
            IsDiffUpdate = isDiffUpdate;
        }
    }
}
