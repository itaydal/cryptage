﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Types
{
    public class QuantizedRateItem
    {
        public bool IsBidBook { get; private set; }
        public bool IsBaseQuantized { get; private set; }
        public double StepSize { get; private set; }
        public int StepsNum { get; private set; }
        public double SrcSize { get; private set; }
        public double DstSize { get; private set; }
        public double Rate { get; private set; }
        public int Precision { get; private set; }
        public string SrcSizeString { get { return SrcSize.ToString(string.Format("N{0}", Precision)); } }
        public string DstSizeString { get { return DstSize.ToString(string.Format("N{0}", Precision)); } }

        public QuantizedRateItem(bool isBidBook, bool isBaseQuantized, double stepSize, int stepsNum, double rate, int precision)
        {
            IsBidBook = isBidBook;
            IsBaseQuantized = isBaseQuantized;
            this.StepSize = stepSize;
            StepsNum = stepsNum;
            Rate = rate;
            Precision = precision;

            var quantizedSize = stepsNum * stepSize;
            SrcSize = (isBaseQuantized == isBidBook) ? quantizedSize : (rate > 10e-12 ? quantizedSize / rate : 0.0);
            DstSize = (isBaseQuantized == isBidBook) ? quantizedSize * rate : quantizedSize;
        }
    }
}
