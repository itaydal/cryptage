﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Types
{
    public class RateItem
    {
        private double _srcSize;
        private double _dstSize;
        private double _rate;

        public double SrcSize { get { return _srcSize; } set { _srcSize = value; _rate = _dstSize / _srcSize; } }
        public double DstSize { get { return _dstSize; } set { _dstSize = value; _rate = _dstSize / _srcSize; } }
        public double Rate { get { return _rate; } }
        public string ExecutionSize { get; private set; }
        public string ExecutionPrice { get; private set; }

        public RateItem(double srcSize, double dstSize, string executionSize = null, string executionPrice = null)
        {
            _srcSize = srcSize;
            _dstSize = dstSize;
            _rate = _srcSize > Config.NumericEpsilon ? _dstSize / _srcSize : 0.0;
            this.ExecutionSize = executionSize;
            this.ExecutionPrice = executionPrice;
        }

        public void ConsumeSrcSize(double srcVolumeConsumed)
        {
            this._srcSize -= srcVolumeConsumed;
            this._dstSize -= srcVolumeConsumed * _rate;
            this._rate = Config.NumericEpsilon < _srcSize ? this._rate : 0.0;
        }

        public void SetSrcSizeFixedRate(double newSrcSize)
        {
            this._srcSize = newSrcSize;
            this._dstSize = newSrcSize * _rate;
            this._rate = Config.NumericEpsilon < _srcSize ? this._rate : 0.0;
        }
    }
}
