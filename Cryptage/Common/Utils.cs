﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.IO;
using Common.Types;
using Newtonsoft.Json.Linq;
using System.Net.Security;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;
using System.Linq;
using System.Threading;
using Common;

namespace Common
{
    public static class Utils
    {
        public static void PrintRateItem(string prefix, EdgeKey edgeKey, RateItem rateItem)
        {
            Console.WriteLine("{0}:\t{1} {2}-->(*{3})-->{4} {5} in exchange {6}, {7}",
                prefix,
                rateItem.SrcSize, edgeKey.SrcNode.Currency,
                rateItem.Rate,
                rateItem.DstSize, edgeKey.DstNode.Currency,
                edgeKey.Exchange,
                edgeKey.IsBidBook ? "Bid book" : "Ask book");
        }

        public static string HttpGet(string uri, int delaySecs = 0)
        {
            var res = String.Empty;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                res = reader.ReadToEnd();
            }

            if (delaySecs > 0)
                Thread.Sleep(1000 * delaySecs);
            return res;
        }

        public static void SslDispose(SslStream ssl, NetworkStream netstream)
        {
            ssl.Dispose();
            netstream.Dispose();
        }

        public static (SslStream, NetworkStream) SslConnectApi(string hostname, List<IPAddress> AllowedIPs, int port = 443, bool prioritySend = true)
        {
            var tcp = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
            {
                ReceiveBufferSize = 1 << 20, // 1MB
                NoDelay = (prioritySend == true)
            };
            tcp.Connect(hostname, port);

            var netstream = new NetworkStream(tcp, ownsSocket: true);
            var ssl = new SslStream(netstream, true,
                new RemoteCertificateValidationCallback((sender, certificate, chain, sslPolicyErrors) =>
                {
                    switch (sslPolicyErrors)
                    {
                        case SslPolicyErrors.None:
                            return true;
                        case SslPolicyErrors.RemoteCertificateChainErrors:
                            return false;
                        case SslPolicyErrors.RemoteCertificateNameMismatch:
                            IPAddress ip = IPAddress.Parse(((HttpWebRequest)sender).RequestUri.Host);
                            if (AllowedIPs.Contains(ip))
                            {
                                string subjecthostname = certificate.Subject.Split(',')[0].Split('=')[1];
                                Regex regex = new Regex(subjecthostname.Replace(".", "\\.").Replace("*", "."));
                                if (regex.IsMatch(hostname))
                                    return true;
                            }
                            return false;
                        default:
                            return false;
                    }
                }), null);

            ssl.AuthenticateAsClient(hostname);

            return (ssl, netstream);
        }

        public static string StreamReadUntilCRLF(Stream stream)
        {
            string line = "";
            int curr = 0, prev = 0, readnum;
            byte[] buff = { 0 };
            while (true)
            {
                readnum = stream.Read(buff, 0, 1);
                if (readnum == 0)
                    throw new EndOfStreamException();
                curr = buff[0];
                if (prev == '\r' && curr == '\n')
                    return line;
                if (curr != '\r')
                    line += (char)curr;
                prev = curr;
            }
            throw new EndOfStreamException();
        }

        public static void StreamReadExact(Stream stream, byte[] buffer, int count, int offset=0)
        {
            var index = 0;
            while (index < count)
            {
                int readnum = stream.Read(buffer, offset + index, count - index);
                if (readnum == 0)
                    throw new EndOfStreamException();
                index += readnum;
            }
        }

        public static void WriteSslStream(SslStream stream, byte[] buffer, int writeSize)
        {
            stream.Write(buffer, 0, writeSize);
            stream.Flush();
        }

        public static int CreateHttpRequest(string url, Dictionary<string, object> requestParams,
            byte[] buffer, int writeOffset = 0, int numDuplicates = 0)
        {
            var method = requestParams.ContainsKey("method") ? (string)requestParams["method"] : "GET";
            byte[] content = requestParams.ContainsKey("content") ?
                Encoding.UTF8.GetBytes((string)requestParams["content"]) : null;

            var CRLF = "\r\n";
            var uri = new Uri(url);
            var httpString = string.Format("{0} {1} HTTP/1.1", method, uri.PathAndQuery) + CRLF;
            httpString += string.Format("Host: {0}", uri.Host) + CRLF;

            if (content != null)
            {
                var contentType = requestParams.ContainsKey("content_type") ?
                    requestParams["content_type"] : "application/x-www-form-urlencoded";
                httpString += "Content-Type: " + contentType + CRLF;
                httpString += string.Format("Content-Length: {0}", content.Count()) + CRLF;
            }

            if (requestParams.ContainsKey("headers"))
                foreach (var item in (Dictionary<string, string>)requestParams["headers"])
                    httpString += item.Key + ": " + item.Value + CRLF;

            httpString += CRLF;

            for (int i = 0; i < 1 + numDuplicates; i++)
            {
                Encoding.ASCII.GetBytes(httpString, 0, httpString.Length, buffer, writeOffset);
                writeOffset += httpString.Length;
                if (content != null)
                {
                    Buffer.BlockCopy(content, 0, buffer, writeOffset, content.Count());
                    writeOffset += content.Count();
                }
            }
            return writeOffset;
        }

        public static (HttpStatusCode statusCode, Dictionary<string, string> headers, string data) 
            ReceiveHttpResponse(Stream stream)
        {
            bool isFirstLine = true;
            bool isChunked = false;
            HttpStatusCode statusCode = HttpStatusCode.RequestTimeout;
            var contentLength = 0;
            var headers = new Dictionary<string, string>();
            while (true)
            {
                var line = StreamReadUntilCRLF(stream);
                if (line.Equals(""))
                {
                    if (isFirstLine)
                        continue; // bug ??
                    break;
                }

                if (isFirstLine)
                {
                    statusCode = (HttpStatusCode)int.Parse(line.Split(' ')[1]);
                    isFirstLine = false;
                }
                else
                {
                    string[] sep = { ": " };
                    var header = line.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    headers[header[0].ToLower()] = header[1];
                }
            }
            foreach (var item in headers)
            {
                if (item.Key.ToLower().Equals("content-length"))
                    contentLength = int.Parse(item.Value);
                if (item.Key.ToLower().Equals("transfer-encoding"))
                {
                    if (item.Value.ToLower().Equals("chunked"))
                        isChunked = true;
                }
            }


            string responseData = "";
            if (!isChunked)
            {
                if (contentLength > 0)
                {
                    var responseBuffer = new byte[contentLength];
                    StreamReadExact(stream, responseBuffer, contentLength);
                    responseData = Encoding.UTF8.GetString(responseBuffer);
                }
            }
            else
            {
                while (true)
                {
                    var line = StreamReadUntilCRLF(stream);
                    if (line.Equals(""))
                        continue;

                    int chunkLength = Convert.ToInt32(line, 16);
                    if (chunkLength == 0)
                    {
                        StreamReadUntilCRLF(stream); // read last CRLF
                        break;
                    }

                    byte[] chunk = new byte[chunkLength];
                    StreamReadExact(stream, chunk, chunkLength);
                    responseData += Encoding.UTF8.GetString(chunk);
                }
            }
            return (statusCode, headers, responseData);
        }

        // "consts" to help understand calculations
        const int bytesperlong = 4; // 32 / 8
        const int bitsperbyte = 8;

        public static bool SetKeepAlive(Socket sock, ulong time, ulong interval)
        {
            try
            {
                // resulting structure
                byte[] SIO_KEEPALIVE_VALS = new byte[3 * bytesperlong];

                // array to hold input values
                ulong[] input = new ulong[3];

                // put input arguments in input array
                if (time == 0 || interval == 0) // enable disable keep-alive
                    input[0] = (0UL); // off
                else
                    input[0] = (1UL); // on

                input[1] = (time); // time millis
                input[2] = (interval); // interval millis

                // pack input into byte struct
                for (int i = 0; i < input.Length; i++)
                {
                    SIO_KEEPALIVE_VALS[i * bytesperlong + 3] = (byte)(input[i] >> ((bytesperlong - 1) * bitsperbyte) & 0xff);
                    SIO_KEEPALIVE_VALS[i * bytesperlong + 2] = (byte)(input[i] >> ((bytesperlong - 2) * bitsperbyte) & 0xff);
                    SIO_KEEPALIVE_VALS[i * bytesperlong + 1] = (byte)(input[i] >> ((bytesperlong - 3) * bitsperbyte) & 0xff);
                    SIO_KEEPALIVE_VALS[i * bytesperlong + 0] = (byte)(input[i] >> ((bytesperlong - 4) * bitsperbyte) & 0xff);
                }
                // create bytestruct for result (bytes pending on server socket)
                byte[] result = BitConverter.GetBytes(0);
                // write SIO_VALS to Socket IOControl
                sock.IOControl(IOControlCode.KeepAliveValues, SIO_KEEPALIVE_VALS, result);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public static void LeftShiftArray<T>(T[] arr, int shift)
        {
            shift = shift % arr.Length;
            T[] buffer = new T[shift];
            Array.Copy(arr, buffer, shift);
            Array.Copy(arr, shift, arr, 0, arr.Length - shift);
            Array.Copy(buffer, 0, arr, arr.Length - shift, shift);
        }
    }
}

