﻿using Common;
using Common.Types;
using ExchangeAPI;
using ExchangeAPI.MarketCapProviders;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Cryptage
{
    class BalanceFixer
    {
        private static Dictionary<EdgeKey, EdgeInfo> edgeInfos = new Dictionary<EdgeKey, EdgeInfo>();
        private static Dictionary<ExchangeName, Dictionary<string, double>> funds = 
            new Dictionary<ExchangeName, Dictionary<string, double>>();
        private static HashSet<ExchangeName> exchangeFixDone = new HashSet<ExchangeName>();

        private static ConcurrentQueue<ExecutionReport> reportQueue = new ConcurrentQueue<ExecutionReport>();
        private static ConcurrentQueue<ExchangeFundsSnapshot> fundsQueue = new ConcurrentQueue<ExchangeFundsSnapshot>();

        private static void PrintBalance(Exchange currExchange, Dictionary<string, double> currExchangeFundsUsdWorth,
            Dictionary<string, double> currExchangeFunds)
        {
            Console.WriteLine(currExchange.Name);
            foreach (var currency in currExchangeFundsUsdWorth.OrderByDescending(x => x.Value).Select(x => x.Key))
            {
                Console.WriteLine("{0} {1} = {2} USD", currExchangeFunds[currency], currency,
                    currExchangeFundsUsdWorth[currency]);
            }
            Console.WriteLine();
        }

        public static void FixBalance(Dictionary<ExchangeName, HashSet<string>> desiredFundedNodes)
        {
            Config.EnabledExchanges = desiredFundedNodes.Keys.ToList();

            MessageBroker.EdgeInfoUpdateEvent += ((eiu) => {
                foreach (var e in eiu.EdgeInfos)
                    edgeInfos.Add(e.Key, e.Value);
            });

            MessageBroker.ExchangeFundsSnapshotEvent += ((s) => fundsQueue.Enqueue(s));
            MessageBroker.ExecutionReportEvent += 
                ((rep) => {
                    foreach (var er in rep.ExecutionResult) {
                        Utils.PrintRateItem("Actual", er.Item1, er.Item2);
                    }
                    reportQueue.Enqueue(rep);
                });

            var marketCap = MarketCapAPI.UpdateMarketCap();
            Exchange.LoadExchanges();
            Thread.Sleep(40 * 1000);

            while (Config.EnabledExchanges.Any(e => !exchangeFixDone.Contains(e)))
            {
                if (fundsQueue.TryDequeue(out ExchangeFundsSnapshot fundsSnapshot))
                    funds.Add(fundsSnapshot.Exchange, fundsSnapshot.CurrencyFunds);

                if (funds.Count == 0)
                {
                    Console.WriteLine("Waiting for funds update...");
                    Thread.Sleep(2000);
                    continue;
                }

                var first = funds.First();
                var currExchangeName = first.Key;
                var currExchange = Exchange.GetInstance(currExchangeName);
                var currExchangeFunds = first.Value;
                var currExchangeFundsUsdWorth = 
                    currExchangeFunds.ToDictionary(f => f.Key, f => f.Value * marketCap[f.Key].PriceUsd);
                PrintBalance(currExchange, currExchangeFundsUsdWorth, currExchangeFunds);

                var currExchangeDesiredFundedNodes = desiredFundedNodes[currExchangeName].ToList()
                    .FindAll(c1 => c1.Equals(Config.BalanceFixerMarketCurrency) ||
                    currExchange.ProductToSymbol.Keys.Contains(c1 + "-" + Config.BalanceFixerMarketCurrency) ||
                    currExchange.ProductToSymbol.Keys.Contains(Config.BalanceFixerMarketCurrency + "-" + c1))
                    .OrderBy(c => currExchangeFundsUsdWorth.ContainsKey(c) ? currExchangeFundsUsdWorth[c] : 0.0)
                    .ToList();

                GatherSplitFunds(currExchange, currExchangeFunds, currExchangeDesiredFundedNodes, gather: true);
                GatherSplitFunds(currExchange, currExchangeFunds, currExchangeDesiredFundedNodes, gather: false);

                exchangeFixDone.Add(currExchange.Name);
                funds.Remove(currExchange.Name);
                currExchangeFundsUsdWorth =
                    currExchangeFunds.ToDictionary(f => f.Key, f => f.Value * marketCap[f.Key].PriceUsd);
                PrintBalance(currExchange, currExchangeFundsUsdWorth, currExchangeFunds);
            }
            Console.WriteLine("Finished balancing");
        }

        private static ExecutionCommand CreateRateItem(EdgeKey edgeKey, double srcSize, double desiredDstSize)
        {
            var desiredRateItem = new RateItem(srcSize, desiredDstSize);
            var adjustResult = Exchange.AdjustRateItem(edgeKey, desiredRateItem, out RateItem adjustedRateItem);
            return adjustResult ? new ExecutionCommand(edgeKey, adjustedRateItem, desiredRateItem, true) : null;
        }

        private static EdgeKey GetEdgeKey(ExchangeName exchange, string src, string dst)
        {
            foreach (var e in edgeInfos.Keys)
            {
                if (e.Exchange == exchange && e.SrcNode.Currency == src && e.DstNode.Currency == dst)
                    return e;
            }
            Console.WriteLine("Can't find edge for {0}->{1} in {2}", src, dst, exchange);
            return null;
        }

        private static void GatherSplitFunds(Exchange currExchange, Dictionary<string, double> currExchangeFunds,
            List<string> desiredNodes, bool gather = true)
        {
            Dictionary<string, double> fundUpdates = new Dictionary<string, double>();
            var totalUsdWorth = 0.0;
            foreach (var c in currExchangeFunds.Keys)
                totalUsdWorth += 
                    MarketCapAPI.MarketCap[c].PriceUsd * (currExchangeFunds.ContainsKey(c) ? currExchangeFunds[c] : 0.0);
                
            double desiredUsdWorth = totalUsdWorth / desiredNodes.Count;

            var currencies = gather ? currExchangeFunds.Keys.ToList() : desiredNodes;
            foreach (var currency in currencies)
            {
                (string srcCurrency, string dstCurrency) = 
                    gather ? (currency, Config.BalanceFixerMarketCurrency) : (Config.BalanceFixerMarketCurrency, currency);
                if (srcCurrency.Equals(dstCurrency))
                    continue;

                EdgeKey edgeKey = GetEdgeKey(currExchange.Name, srcCurrency, dstCurrency);
                if (edgeKey == null)
                    continue;

                var srcSize = currExchangeFunds.ContainsKey(srcCurrency) ? currExchangeFunds[srcCurrency] : 0.0;
                if (gather)
                {
                    if (desiredNodes.Contains(srcCurrency))
                    {
                        var srcUsdWorth = MarketCapAPI.MarketCap[srcCurrency].PriceUsd * srcSize;
                        if (srcUsdWorth < desiredUsdWorth)
                            continue; // don't gather desired node with less than desired amount

                        // gather only the amount above the desired amount
                        srcSize = Math.Min(srcSize, (srcUsdWorth - desiredUsdWorth) / MarketCapAPI.MarketCap[srcCurrency].PriceUsd);
                    }
                }
                else
                {
                    if (!desiredNodes.Contains(dstCurrency))
                        continue; // don't split to non-desired node

                    var dstUsdWorth = MarketCapAPI.MarketCap[dstCurrency].PriceUsd *
                        (currExchangeFunds.ContainsKey(dstCurrency) ? currExchangeFunds[dstCurrency] : 0.0);

                    if (dstUsdWorth >= desiredUsdWorth)
                        continue;
                    var srcUsdWorth = desiredUsdWorth - dstUsdWorth;
                    srcSize = Math.Min(srcSize, srcUsdWorth / MarketCapAPI.MarketCap[srcCurrency].PriceUsd);
                }

                var executeCounter = 4;
                double desiredDstSize = 1.0;
                if (!edgeKey.IsBidBook)
                {
                    var product = edgeKey.BaseCurrency + "-" + edgeKey.QuoteCurrency;
                    var bestRate = 1 / PriceQuery.GetBestPrice(edgeKey.Exchange, product, out var sizes).bestAsk;
                    desiredDstSize = srcSize * bestRate;
                }
                //var desiredDstSize = desiredUsdWorth / MarketCapAPI.MarketCap[dstCurrency].PriceUsd;
                while (executeCounter > 0)
                {
                    var command = CreateRateItem(edgeKey, srcSize, desiredDstSize);
                    if (command == null)
                    {
                        Console.WriteLine("Can't move {0}->{1} in {2}", srcCurrency, dstCurrency, currExchange.Name);
                        break;
                    }
                    ExecutionRequest executionRequest = new ExecutionRequest(Guid.NewGuid(), currExchange.Name);
                    executionRequest.Commands.Add(command);

                    Utils.PrintRateItem("Expected", command.Edge, command.AdjustedRateItem);
                    Exchange.HandleExecutionRequests(new List<ExecutionRequest> { executionRequest });

                    ExecutionReport executionReport;
                    while (!reportQueue.TryDequeue(out executionReport)) { }
                    if (executionReport.IsSuccessful)
                    {
                        foreach (var result in executionReport.ExecutionResult)
                        {
                            var src = result.Item1.SrcNode.Currency;
                            var dst = result.Item1.DstNode.Currency;
                            if (!fundUpdates.ContainsKey(src))
                                fundUpdates[src] = 0.0;
                            if (!fundUpdates.ContainsKey(dst))
                                fundUpdates[dst] = 0.0;
                            fundUpdates[src] -= result.Item2.SrcSize;
                            fundUpdates[dst] += result.Item2.DstSize;
                        }
                        break;
                    }
                    executeCounter--;
                    desiredDstSize *= 0.98;
                    Console.WriteLine("Execution retry: {0}->{1} in {2}", srcCurrency, dstCurrency, currExchange.Name);
                }

            }

            foreach (var update in fundUpdates)
                currExchangeFunds[update.Key] = update.Value +
                    (currExchangeFunds.ContainsKey(update.Key) ? currExchangeFunds[update.Key] : 0.0);
        }

        private static bool ReadYN(string question)
        {
            return true;
            Console.WriteLine("{0} Y/N", question);
            while (true)
            {
                var keyRead = Console.ReadLine();
                if (keyRead.Equals("Y") || keyRead.Equals("y"))
                {
                    return true;
                }
                if (keyRead.Equals("N") || keyRead.Equals("n"))
                {
                    return false;
                }
            }
        }
    }
}
