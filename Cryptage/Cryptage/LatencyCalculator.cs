﻿using Common;
using Common.Types;
using ExchangeAPI;
using ExchangeAPI.MarketCapProviders;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cryptage
{
    class LatencyCalculator
    {
        public static Dictionary<ExchangeName, (double updateLatencyMeanMsec, double updateLatencyStdMsec)> 
            CalculateWebsocketLatency(string product, int numTests, double UsdSize)
        {
            var result = new Dictionary<ExchangeName, (double updateLatencyMeanMsec, double updateLatencyStdMsec)>();
            string baseCurrency = Exchange.GetBase(product);
            string quoteCurrency = Exchange.GetQuote(product);
            MarketCapAPI.UpdateMarketCap();
            MarketCapAPI.marketCap = new Dictionary<string, MarketCapItem>
            {
                { baseCurrency, MarketCapAPI.marketCap[baseCurrency] },
                { quoteCurrency, MarketCapAPI.marketCap[quoteCurrency] }
            };

            ConcurrentQueue<OrdersUpdate> updatesQueue = new ConcurrentQueue<OrdersUpdate>();
            MessageBroker.OrdersUpdateEvent += (e => { updatesQueue.Enqueue(e); });
            Exchange.LoadExchanges();
            foreach (var exchange in Config.EnabledExchanges)
            {
                var instance = Exchange.GetInstance(exchange);
                var roundTrip = TimeSpan.FromMilliseconds(instance.GetRoundTripMsec());
                var orderbook = new SortedDictionary<double, double>();
                List<double> latencyMeasurementsMsec = new List<double>();

                for (int i = 0; i < numTests; i++)
                {
                    var t1 = new Task(() =>
                    {
                        var startTime = DateTime.Now;
                        var gatherTime = i == 0 ? TimeSpan.FromSeconds(20.0) : TimeSpan.FromSeconds(5.0);
                        var maxTestTime = gatherTime.Add(TimeSpan.FromSeconds(10.0));
                        var orderSent = false;
                        var gotOurUpdate = false;
                        var sendTime = DateTime.MinValue;
                        var ourPrice = 0.0;
                        var ourSize = 0.0;
                        var originalSize = 0.0;
                        while (DateTime.Now - startTime < maxTestTime)
                        {
                            if (!orderSent && (DateTime.Now - startTime) > gatherTime)
                            {
                                var bestPrice = orderbook.ToList()[orderbook.Count - 1].Key;
                                ourPrice = bestPrice - 2 * instance.ProductInfo[product].PriceStep;
                                ourSize = UsdSize / MarketCapAPI.MarketCap[Exchange.GetBase(product)].PriceUsd;
                                var baseStepSize = instance.ProductInfo[product].BaseStep;
                                ourSize = Math.Floor(ourSize / baseStepSize) * baseStepSize;
                                if (!orderbook.TryGetValue(ourPrice, out originalSize))
                                    originalSize = 0.0;

                                Console.WriteLine("{0}: {1}: CalculateWebsocketLatency: best price: {2}, our price: {3}, our size: {4} (original size: {5})",
                                    DateTime.Now.ToString("hh:mm:ss.fff"), exchange, bestPrice, ourPrice, ourSize, originalSize);

                                instance.PlaceLimitOrder(product, true, ourPrice, ourSize, autoCancel: true);
                                sendTime = DateTime.UtcNow;
                                orderSent = true;
                            }

                            if (updatesQueue.TryDequeue(out var update))
                            {
                                foreach (var item in update.Orders)
                                {
                                    var edgeKey = item.Item1;
                                    var candles = item.Item2;
                                    if (exchange != edgeKey.Exchange)
                                        continue;

                                    var currentProduct = edgeKey.BaseCurrency + "-" + edgeKey.QuoteCurrency;
                                    if (!product.Equals(currentProduct) || !edgeKey.IsBidBook)
                                        continue;

                                    if (update.OverrideOrderbooks)
                                        orderbook.Clear();

                                    // got update for our product
                                    foreach (var candle in candles)
                                    {
                                        Console.WriteLine("{0}: {1}: CalculateWebsocketLatency: got update for price {2}. new size: {3}",
                                            DateTime.Now.ToString("hh:mm:ss.fff"), exchange, candle.Key, candle.Value);
                                        if (candle.Value < Config.NumericEpsilon && orderbook.ContainsKey(candle.Key))
                                            orderbook.Remove(candle.Key);
                                        else
                                            orderbook[candle.Key] = candle.Value;
                                    }
                                }


                                if (!orderSent || sendTime == DateTime.MinValue)
                                    continue;
                               
                                if (!orderbook.ContainsKey(ourPrice))
                                    continue;

                                if (Math.Abs(orderbook[ourPrice] - originalSize) < Config.NumericEpsilon)
                                    continue;

                                // our candle has been modified
                                var newSize = orderbook[ourPrice];

                                if (Math.Abs(newSize - (originalSize + ourSize)) > Config.NumericEpsilon)
                                {
                                    // not by our order
                                    Console.WriteLine("{0}: {1}: CalculateWebsocketLatency: failure, candle updated with other order",
                                        DateTime.Now.ToString("hh:mm:ss.fff"), exchange);
                                    Task.Yield();
                                    break;
                                }

                                // got update with our order
                                var updateLatency = (update.Measurement.ArrivalTime - sendTime) - roundTrip / 2;
                                Console.WriteLine("{0}: {1}: CalculateWebsocketLatency: order process latency + websocket latency = {2}[msec]",
                                    DateTime.Now.ToString("hh:mm:ss.fff"), exchange, updateLatency.TotalMilliseconds);
                                latencyMeasurementsMsec.Add(updateLatency.TotalMilliseconds);
                                gotOurUpdate = true;
                                Task.Yield();
                                break;
                            }
                            else
                                Thread.Sleep(1);
                        }
                        if (!gotOurUpdate)
                            Console.WriteLine("{0}: {1}: CalculateWebsocketLatency: didn't get our order update in 30 seconds", 
                                DateTime.Now.ToString("hh:mm:ss.fff"), exchange);
                    });
                    t1.Start();
                    t1.Wait();
                }
                var average = latencyMeasurementsMsec.Average();
                var std = Math.Sqrt(
                    latencyMeasurementsMsec.Sum(val => (val - average) * (val - average)) / latencyMeasurementsMsec.Count);
                Console.WriteLine("{0}: CalculateWebsocketLatency: latency measurement results:", exchange);
                latencyMeasurementsMsec.Sort();
                foreach (var item in latencyMeasurementsMsec)
                    Console.Write("{0} {1}", item, latencyMeasurementsMsec.IndexOf(item) % 10 == 0 ? "\r\n" : "");
                Console.WriteLine("");
                result[exchange] = (average, std);
            }
            return result;
        }
    }
}
