﻿using Logic;
using Logic.Types;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common.Types;

namespace Cryptage
{
    class LogPlayer
    {
		private static List<ExchangeName> exchaneNames = new List<ExchangeName> { ExchangeName.Binance, ExchangeName.Bitfinex, ExchangeName.Poloniex };
        private static Dictionary<string, ExchangeName> exchaneNamesConverter = new Dictionary<string, ExchangeName> {
            { "binance", ExchangeName.Binance }, { "bitfinex", ExchangeName.Bitfinex }, { "poloniex", ExchangeName.Poloniex } };

        public static void UpdateFees()
        {
            Dictionary<EdgeKey, EdgeInfo> fees = new Dictionary<EdgeKey, EdgeInfo>();


			string feesLogPath;
			if (System.Environment.OSVersion.Platform == PlatformID.Win32NT)
                feesLogPath = "C:\\logs\\crypto\\fees.json";
			else
				feesLogPath = "/Users/Itay/data/crypto/fees.json";
			
            using (var reader = new StreamReader(feesLogPath))
            {
                //dynamic fessJson = JsonConvert.DeserializeObject(reader.ReadToEnd());
                dynamic fessJson = JToken.Parse(reader.ReadToEnd());
                foreach (var exchName in exchaneNames)
                {
                    foreach (var x in fessJson[exchName.ToString()].Children())
                    {
                        string name = x.Name;
                        if (name.Contains('-'))
                        {
                            var symbolSplit = name.Split('-');
                            var takerFee = x.Value["taker_fee"].Value;
                            var makerFee = x.Value["maker_fee"].Value;
                            // TODO: add baseminsize
                            fees.Add(new EdgeKey(exchName, symbolSplit[0], symbolSplit[1], false ), new EdgeInfo(0, 0));
                            fees.Add(new EdgeKey(exchName, symbolSplit[0], symbolSplit[1], true), new EdgeInfo(0, 0));
                        }
                        
                    }
                }
            }

            MarketMonitor.GetInstance().Update(new EdgeInfoUpdate(fees));
        }

        public static void PlayLogs(bool realTimePlay = true)
        {
			string logPathTemplate;
            if (System.Environment.OSVersion.Platform == PlatformID.Win32NT)
				logPathTemplate = "C:\\logs\\crypto\\large\\{0}.json";
            else
				logPathTemplate = "/Users/Itay/data/crypto/large/{0}.json";

            var readers = exchaneNames.Select(n => new StreamReader(String.Format(logPathTemplate, n))).ToList();

            var marketMon = MarketMonitor.GetInstance();
            

            var topMessages = readers.Select(r => ParseOrdersUpdate(r.ReadLine())).ToList();

            var firstMessageTime = topMessages.Min(m => m.Measurement.ArrivalTime);
            var playStartTime = DateTime.UtcNow;

            Tuple<int, OrdersUpdate> minTimeMsg = null;
            int updatesSent = 0;
            while (true)
            {
                minTimeMsg = MessageTimeArgMin(topMessages);
                if (minTimeMsg.Item1 < 0)
                    break;

                if (((!realTimePlay || (updatesSent < 30000)) && marketMon.MessageQueueCount < 50) || 
                    (minTimeMsg.Item2.Measurement.ArrivalTime < (firstMessageTime + (DateTime.UtcNow - playStartTime))))
                {
                    marketMon.Update(minTimeMsg.Item2);
                    updatesSent++;
                    var r = readers[minTimeMsg.Item1];
                    topMessages[minTimeMsg.Item1] = r.EndOfStream ? null : ParseOrdersUpdate(r.ReadLine());
                }
                else
                {
                    Thread.Sleep(10);
                }
                
               
                minTimeMsg = null;
            }

            Console.WriteLine("Done...");
            Console.ReadKey();
        }


        public static Tuple<int, OrdersUpdate> MessageTimeArgMin(List<OrdersUpdate> messages)
        {
            OrdersUpdate minTimeMsg = null;
            int minIndex = -1;
            for (int i = 0; i < messages.Count; i++)
            {
                if ((messages[i] != null) && ((minTimeMsg == null) || 
                    (messages[i].Measurement.ArrivalTime < minTimeMsg.Measurement.ArrivalTime)))
                {
                    minTimeMsg = messages[i];
                    minIndex = i;
                }
            }

            return new Tuple<int, OrdersUpdate>(minIndex, minTimeMsg);
        }


        static OrdersUpdate ParseOrdersUpdate(string jsonLine)
        {
            dynamic item = JsonConvert.DeserializeObject(jsonLine);

            ExchangeName exchange = exchaneNamesConverter[item.exchange.Value];
            string product = item.product.Value;
            var bidsOrderbook = ParseOrderbook(item.orderbook.bids);
            var asksOrderbook = ParseOrderbook(item.orderbook.asks);
            var time = DateTime.Parse(item.time.Value);
            var type = item.type.Value;
            var productPairs = product.Split('-');

            var bidsEdgeKey = new EdgeKey(exchange, productPairs[0], productPairs[1], true);
            var asksEdgeKey = new EdgeKey(exchange, productPairs[0], productPairs[1], false);

            OrdersUpdate msg = null;
            if ("orders_snapshot".Equals(item.type.Value))
            {
                msg = new OrdersUpdate(true, time, TimeSpan.FromSeconds(0.0));
                msg.Add(bidsEdgeKey, bidsOrderbook);
                msg.Add(asksEdgeKey, asksOrderbook);
            }
            else if ("orders_update".Equals(item.type.Value))
            {
                msg = new OrdersUpdate(false, time, TimeSpan.FromSeconds(0.0));
                msg.Add(bidsEdgeKey, bidsOrderbook);
                msg.Add(asksEdgeKey, asksOrderbook);
            }

            return msg;

        }

        static Dictionary<double, double> ParseOrderbook(dynamic ob)
        {
            var orderbook = new Dictionary<double, double>();
            foreach (var item in ob)
            {
                orderbook.Add(item["price"].Value, item["size"].Value);
            }
            return orderbook;
        }
    }
}
