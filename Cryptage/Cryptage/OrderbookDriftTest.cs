﻿using Common.Types;
using ExchangeAPI;
using Logic;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Threading;
using Common;

namespace Cryptage
{
    class OrderbookDriftTest
    {
        private List<EdgeKey> edgeKeys;
        private Random rnd;
        private bool waitingForOrderbookSample;

        private ConcurrentQueue<EdgeInfoUpdate> _edgeInfosUpdateQueue;
        private ConcurrentQueue<Tuple<EdgeKey, double, double>> _orderbookSampleQueue;
        private Thread _loopThread;

        private bool SampleSequentially;
        private int EdgeSeqIdx = 0;
        private EdgeKey SingleEdgeToSample;
        private EdgeKey EdgeToRevisit;
        private DateTime DriftDetectionTime;
        private int RevisitCount;
        private TimeSpan MaxRevisitDuration = TimeSpan.FromMinutes(5);

        private MarketMonitor marketMonitorInstance;

        public OrderbookDriftTest(bool sampleSequentially, EdgeKey singleEdgeToSample=null)
        {
            SampleSequentially = sampleSequentially;
            SingleEdgeToSample = singleEdgeToSample;
            edgeKeys = new List<EdgeKey>();
            rnd = new Random();
            waitingForOrderbookSample = false;
            EdgeToRevisit = null;
            DriftDetectionTime = DateTime.MinValue;
            RevisitCount = 0;

            MessageBroker.EdgeInfoUpdateEvent += HandleEdgeInfoUpdates;
            marketMonitorInstance = MarketMonitor.GetInstance();

            _edgeInfosUpdateQueue = new ConcurrentQueue<EdgeInfoUpdate>();
            _orderbookSampleQueue = new ConcurrentQueue<Tuple<EdgeKey, double, double>>();
            _loopThread = new Thread(SampleLoop);
            _loopThread.Start();
        }

        private void SampleLoop()
        {
            while (true)
            {
                if (_edgeInfosUpdateQueue.TryDequeue(out EdgeInfoUpdate edgeInfoMsg))
                    HandleEdgeInfoUpdates(edgeInfoMsg);
                else if(_orderbookSampleQueue.TryDequeue(out Tuple<EdgeKey, double, double> orderbookSampleMsg))
                    HandleOrderbookSample(orderbookSampleMsg.Item1, orderbookSampleMsg.Item2, orderbookSampleMsg.Item3);
                else if (!waitingForOrderbookSample && (0 < edgeKeys.Count))
                {
                    EdgeKey edgeToSample = null;
                    if ((EdgeToRevisit == null) || (RevisitCount <= 0))
                    {
                        edgeToSample = SingleEdgeToSample ?? (SampleSequentially ? edgeKeys[EdgeSeqIdx++ % edgeKeys.Count] : edgeKeys[rnd.Next(edgeKeys.Count)]);
                        //Console.WriteLine("OrderbookDriftTest: now sampling: {0}", edgeToSample);
                        EdgeToRevisit = null;
                        DriftDetectionTime = DateTime.MinValue;
                        RevisitCount = 0;
                    }
                    else
                    {
                        edgeToSample = EdgeToRevisit;
                        RevisitCount--;
                    }

                    marketMonitorInstance.Update(edgeToSample, HandleOrderbookSample);
                    waitingForOrderbookSample = true;
                }
                else
                    Thread.Sleep(50);
            }
        }

        private void HandleEdgeInfoUpdates(EdgeInfoUpdate edgeInfosUpdate)
        {
            if (!Thread.CurrentThread.Equals(_loopThread))
            {
                _edgeInfosUpdateQueue.Enqueue(edgeInfosUpdate);
                return;
            }

            edgeKeys.AddRange(edgeInfosUpdate.EdgeInfos.Keys);
            edgeKeys = edgeKeys.Distinct().ToList();
        }

        private void HandleOrderbookSample(EdgeKey edgeKey, double price, double size)
        {
            if (!Thread.CurrentThread.Equals(_loopThread))
            {
                _orderbookSampleQueue.Enqueue(new Tuple<EdgeKey, double, double>(edgeKey, price, size));
                return;
            }

            waitingForOrderbookSample = false;

            if (price < Config.NumericEpsilon)
                return;

            var product = edgeKey.BaseCurrency + "-" + edgeKey.QuoteCurrency;

            double bestBid, bestAsk;
            try
            {
                (bestBid, bestAsk) = PriceQuery.GetBestPrice(edgeKey.Exchange, product, out var sizes);
            }
            catch (Exception)
            {
                return;
            }
            var bestPriceQuery = edgeKey.IsBidBook ? bestBid : bestAsk;

            var relDiff = Math.Abs(price - bestPriceQuery) / (price + bestPriceQuery);
            var isRelDiffLarge = 0.001 < relDiff && (edgeKey.IsBidBook ? (price > bestPriceQuery) : (price < bestPriceQuery));
            if ((isRelDiffLarge) || (EdgeToRevisit != null))
            {
                if (EdgeToRevisit == null)
                    DriftDetectionTime = DateTime.UtcNow;

                Console.WriteLine("{0} - Orderbook drift test for {1}, orderbook {2}, rest {3}, rel diff {4:F4}, Drift detection time {5}", 
                    DateTime.Now, edgeKey, price, bestPriceQuery, relDiff, DriftDetectionTime);

                if (isRelDiffLarge && ((DateTime.UtcNow - DriftDetectionTime) < MaxRevisitDuration))
                {
                    EdgeToRevisit = edgeKey;
                    RevisitCount = 5;
                }
                else if (isRelDiffLarge)
                {
                    EdgeSeqIdx = rnd.Next(edgeKeys.Count);
                    EdgeToRevisit = null;
                    DriftDetectionTime = DateTime.MinValue;
                    RevisitCount = 0;
                }
            }
        }

    }
}
