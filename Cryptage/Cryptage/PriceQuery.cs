﻿using Common;
using Common.Types;
using ExchangeAPI;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;

namespace Cryptage
{
    class PriceQuery
    {
        public static (double bestBid, double bestAsk) GetBestPrice(ExchangeName exchangeName, string product, 
            out (double bidSize, double askSize) sizes)
        {
            string uri;
            JToken token;
            var symbol = ExchangeAPI.Exchange.GetInstance(exchangeName).ProductToSymbol[product];
            (int count, TimeSpan period) = ExchangeAPI.Exchange.GetInstance(exchangeName).RequestLimit;
            int delay = (int)Math.Min(period.TotalMilliseconds / count / 1000.0, 1.0);
            switch (exchangeName)
            {
                case ExchangeName.HitBTC:
                    uri = String.Format(@"https://api.hitbtc.com/api/2/public/orderbook/{0}?limit=1", symbol);
                    token = JToken.Parse(Utils.HttpGet(uri, delay));
                    sizes = (token["bid"][0]["size"].ToObject<double>(), token["ask"][0]["size"].ToObject<double>());
                    return (token["bid"][0]["price"].ToObject<double>(), token["ask"][0]["price"].ToObject<double>());
                case ExchangeName.Binance:
                    uri = String.Format(@"https://api.binance.com/api/v1/depth?symbol={0}&limit=5", symbol);
                    token = JToken.Parse(Utils.HttpGet(uri, delay));
                    sizes = (token["bids"][0][1].ToObject<double>(), token["asks"][0][1].ToObject<double>());
                    return (token["bids"][0][0].ToObject<double>(), token["asks"][0][0].ToObject<double>());
                case ExchangeName.Huobi:
                    string format = "https://api.huobi.pro/market/depth?symbol={0}&type=step0&pick=[\"bids.1\",\"asks.1\"]";
                    uri = String.Format(format, symbol);
                    token = JToken.Parse(Utils.HttpGet(uri, delay));
                    sizes = (token["tick"]["bids"][0][1].ToObject<double>(), token["tick"]["asks"][0][1].ToObject<double>());
                    return (token["tick"]["bids"][0][0].ToObject<double>(), token["tick"]["asks"][0][0].ToObject<double>());
                case ExchangeName.OKEX:
                    uri = String.Format(@"https://www.okex.com/api/v1/depth.do?symbol={0}&size=1", symbol);
                    token = JToken.Parse(Utils.HttpGet(uri, delay));
                    sizes = (token["bids"][0][1].ToObject<double>(), token["asks"][0][1].ToObject<double>());
                    return (token["bids"][0][0].ToObject<double>(), token["asks"][0][0].ToObject<double>());
                case ExchangeName.Bittrex:
                    uri = String.Format("https://bittrex.com/api/v2.0/pub/Market/GetMarketOrderBook?marketname={0}", symbol);
                    token = JToken.Parse(Utils.HttpGet(uri, delay))["result"];
                    sizes = (token["buy"][0]["Quantity"].ToObject<double>(), token["sell"][0]["Quantity"].ToObject<double>());
                    return (token["buy"][0]["Rate"].ToObject<double>(), token["sell"][0]["Quantity"].ToObject<double>());
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
