using Logic;
using Logic.Types;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExchangeAPI;
using ExchangeAPI.Exchanges;
using ExchangeAPI.MarketCapProviders;
using System.Threading;
using Newtonsoft.Json.Linq;
using Common.Types;
using Common;
using System.Collections.Concurrent;

namespace Cryptage
{
    class Program
    {
        static void Main(string[] args)
        {
            //var orderbookDriftTester = new OrderbookDriftTest(sampleSequentially: true);
            RunMainFlow();

            //RateAdjustmentTest.RunRateAdjustmentTest();
            //FixBalanceTest();
            //TestExecutionCommands();
            //PlayLogs();
            //WebsocketLatencyTest();
            //PushDummyRates();
            //TestThreadContectSwitch();
        }

        private static void TestThreadContectSwitch()
        {
            var queue = new ConcurrentQueue<System.Diagnostics.Stopwatch>();
            void sleepAndEnqueue()
            {
                Thread.Sleep(5);
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                queue.Enqueue(sw);
            }

            double numberCrunching()
            {
                var prod = 1.0;
                foreach (var i in Enumerable.Range(1, 50000))
                {
                    prod *= (double)i;
                }
                return prod;
            }

            new Thread(() =>
            {
                var maxDiff = 10.0;
                var useless = 1.0;
                while (true)
                {
                    if (queue.TryDequeue(out var sw))
                    {
                        var diff = sw.ElapsedMilliseconds;
                        sw.Stop();
                        if (diff > maxDiff)
                            Console.WriteLine("{0}: diff = {1}[msec]", DateTime.Now, diff);
                        useless *= numberCrunching();
                    }
                    else
                        Thread.Sleep(1);
                }
            }).Start();

            
            new Thread(() =>
            {
                var useless = 1.0;
                while (true)
                {
                    useless *= numberCrunching();
                    sleepAndEnqueue();
                }
            }).Start();
            
        }

        private static void FixBalanceTest()
        {
            Config.WebsocketsEnabled = false;
            Dictionary<ExchangeName, HashSet<string>> desiredFundsNodes = new Dictionary<ExchangeName, HashSet<string>>();
            desiredFundsNodes[ExchangeName.Binance] = new HashSet<string> {
                "USDT", "BTC", "ETH", "GTO", "SALT", "OAX", "EOS", "NAS", "LTC"
            };

            desiredFundsNodes[ExchangeName.HitBTC] = new HashSet<string> {
                "USDT", "BTC", "ETH", "GTO", "SALT", "OAX", "EOS", "NAS", "LTC"
            };

            desiredFundsNodes[ExchangeName.Huobi] = new HashSet<string> {
                "USDT", "BTC", "ETH", "GTO", "SALT", "OAX", "EOS", "NAS", "LTC"
            };

            desiredFundsNodes[ExchangeName.OKEX] = new HashSet<string> {
                "USDT", "BTC", "ETH", "GTO", "SALT", "OAX", "EOS", "NAS", "LTC"
            };

            BalanceFixer.FixBalance(desiredFundsNodes);
        }

        private static void WebsocketLatencyTest()
        {
            Config.EnabledExchanges = new List<ExchangeName> {
                ExchangeName.Binance
                //ExchangeName.Huobi, 
                //ExchangeName.HitBTC,
                //ExchangeName.OKEX
            };
            var numTests = 20;
            var product = "DNT-BTC";
            var UsdSize = 10.0;
            var results = LatencyCalculator.CalculateWebsocketLatency(product, numTests, UsdSize);
            foreach (var result in results)
            {
                Console.WriteLine("{0}: latency avg: {1}[msec], latency std: {2} ({3} tests performed)",
                    result.Key, result.Value.updateLatencyMeanMsec, result.Value.updateLatencyStdMsec, numTests);
            }
        }

        private static void RunMainFlow()
        {
            if (Config.SendExecutionRequests)
            {
                Console.WriteLine("Running with SendExecutionRequests=True, press any key to continue...");
                Console.ReadKey();
                Console.WriteLine("Starting");
            }

            var marketMon = MarketMonitor.GetInstance();
            var merketCap = MarketCapAPI.UpdateMarketCap();

            Exchange.LoadExchanges(autoLoadWebsockets: false);

            Thread.Sleep(40000);
            while (true)
            {
                Thread.Sleep(20000);
                Console.WriteLine("MarketMonitor MessageQueueCount={0}, EdgeCount={1}, AvailableCycleCount={2}, OrderMessageDrop={3}",
                                  marketMon.MessageQueueCount, 
                                  marketMon.EdgeCount, 
                                  marketMon.AvailableCyclesCount,
                                  Interlocked.Read(ref MarketMonitor.OrderMessageDropCounter));
                Console.WriteLine("MinSizeFilter-BeforeMF={0}  MinSizeFilter-Funds={1}  InternalCycleFilter={2}  MarketCurrenciesFilter={3}  QuantizationFilter={4}  MinProfitFilter={5}",
                                  Interlocked.Read(ref Cycle.BaseMinSizeFilterCountBeforeMF),
                                  Interlocked.Read(ref Cycle.BaseMinSizeFilterCountFunds),
                                  Interlocked.Read(ref MarketMonitor.InternalCycleFilterCount),
                                  Interlocked.Read(ref MarketMonitor.MarketCurrenciesCycleFilterCount),
                                  Interlocked.Read(ref Cycle.QuantizationFilterCount),
                                  Interlocked.Read(ref MarketMonitor.MinProfitFilterCount));

                foreach (var counters in Exchange.MessageCounters)
                {
                    Console.WriteLine(
                        "{0}: count: {1}, latency: {2}[msec] (high: {3}), queues: ws: {4}, parser: {5}, disconnects: ws: {6}, rest: {7}",
                        counters.Key, counters.Value.msgCount, counters.Value.latency, counters.Value.highLatencyCount,
                        counters.Value.wsQueueCount, counters.Value.parserQueueCount, counters.Value.wsDisconnects, 
                        counters.Value.restDisconnects);
                }
            }
        }

        private static void TestExecutionCommands()
        {
            var name = ExchangeName.HitBTC;
            Config.EnabledExchanges = new List<ExchangeName> { name };
            Exchange.LoadExchanges();

            // wait for exchanges to get products info and balances
            Thread.Sleep(12000);

            Exchange exchange = Exchange.GetInstance(name);
            MarketMonitor marketMonitor = MarketMonitor.GetInstance();
            string product = "BTC-USDT";
            string baseCurrency = Exchange.GetBase(product);
            string quoteCurrency = Exchange.GetQuote(product);
            Product productInfo = exchange.ProductInfo[product];

            bool isBidCommand = true;
            double baseSize = marketMonitor.GetFund(new NodeKey(name, baseCurrency));
            double quoteSize = marketMonitor.GetFund(new NodeKey(name, quoteCurrency));
            double srcSize = isBidCommand ? 0.6547 : 0.0001; // baseSize;
            double price = 6347.02;
            double rate = isBidCommand ? 1 / price : price;
            var rateItem = new RateItem(srcSize, rate * srcSize);
            var edgeKey = new EdgeKey(name, baseCurrency, quoteCurrency, !isBidCommand);
            Exchange.AdjustRateItem(edgeKey, rateItem, out RateItem adjustedRateItem);

            ExecutionRequest executionRequest = new ExecutionRequest(new Guid(), name);
            executionRequest.Commands.Add(new ExecutionCommand(edgeKey, adjustedRateItem, rateItem, true));

            exchange.Execute(executionRequest);
        }

        private static void PlayLogs()
        {
            new Thread(() =>
            {
                while (true)
                {
                    Thread.Sleep(5000);
                    var marketMon = MarketMonitor.GetInstance();
                    Console.WriteLine("MarketMonitor MessageQueueCount={0}, EdgeCount={1}, AvailableCycleCount={2}",
                        marketMon.MessageQueueCount, marketMon.EdgeCount, marketMon.AvailableCyclesCount);
                }
            }).Start();

            LogPlayer.UpdateFees();
            var merketCap = MarketCapAPI.UpdateMarketCap();
            UpdateDummyFunds(merketCap);
            LogPlayer.PlayLogs(false);
        }

        private static void UpdateDummyFunds(Dictionary<string, MarketCapItem>  marketCap)
        {
            var fundedExchanges = new List<ExchangeName> { ExchangeName.Binance, ExchangeName.HitBTC, ExchangeName.Huobi };
            var fundedCurrencies = new List<string> { "BTC", "ETH", "XRP", "BCH", "EOS", "LTC", "USDT", "STEEM", "XLM", "USD", "EUR", "GBP", "JPY" };

            if (false)
            {
                var initialFundsUsd = 1000;
                foreach (var exchange in fundedExchanges)
                {
                    Dictionary<string, double> currencyFunds = new Dictionary<string, double>();
                    foreach (var curr in fundedCurrencies)
                    {
                        currencyFunds.Add(curr, initialFundsUsd / marketCap[curr].PriceUsd);
                    }
                    MarketMonitor.GetInstance().Update(new ExchangeFundsSnapshot(exchange, currencyFunds));
                }
            }
            else
            {
                foreach (var exchange in fundedExchanges)
                {
                    var initialFundsUsd = 1000;
                    Dictionary<string, double> currencyFunds = new Dictionary<string, double>();
                    foreach (var mc in marketCap.Where(m => m.Value.Rank < 10))
                    {
                        currencyFunds.Add(mc.Key, initialFundsUsd / mc.Value.PriceUsd);
                    }
                    MarketMonitor.GetInstance().Update(new ExchangeFundsSnapshot(exchange, currencyFunds));
                }
            }
        }

        private static void PushDummyRates()
        {
            Config.WebsocketsEnabled = false;

            var marketMon = MarketMonitor.GetInstance();
            var merketCap = MarketCapAPI.UpdateMarketCap();

            Exchange.LoadExchanges();

            Console.ReadKey();
            Console.WriteLine("Pushing dummy rates");

            var efs1 = new ExchangeFundsSnapshot(ExchangeName.Binance, new Dictionary<string, double> { { "ENJ", 137.762 }, { "ETH", 0.0615 }, { "BTC", 0.000135 } });
            MessageBroker.PublishMessage(efs1);
            var efs2 = new ExchangeFundsSnapshot(ExchangeName.OKEX, new Dictionary<string, double> { { "ENJ", 91.04 }, { "ETH", 0.027 }, { "BTC", 0.00297 } });
            MessageBroker.PublishMessage(efs2);

            var ou1 = new OrdersUpdate(false, new DelayMeasurement(""), TimeSpan.FromSeconds(0));
            ou1.Add(new EdgeKey(ExchangeName.OKEX, "ENJ", "ETH", false), new Dictionary<double, double> { { 0.00026376, 202.41 }, { 0.00026377, 1637.4 } });
            ou1.Add(new EdgeKey(ExchangeName.OKEX, "ENJ", "BTC", false), new Dictionary<double, double> { { 9.26E-6, 16864.79 }, { 9.28E-6, 1522.44 } });
            ou1.Add(new EdgeKey(ExchangeName.Binance, "ENJ", "BTC", true), new Dictionary<double, double> { { 9.3E-6, 46037 }, { 9.26E-6, 2159 } });
            ou1.Add(new EdgeKey(ExchangeName.Binance, "ENJ", "ETH", true), new Dictionary<double, double> { { 0.00026475, 530 }, { 0.00026375, 3872 } });
            MessageBroker.PublishMessage(ou1);

            Console.ReadKey();
            Console.WriteLine("Pushing dummy rates");
            MessageBroker.PublishMessage(ou1);


            Console.ReadKey();
            Console.WriteLine("Pushing dummy rates");
            MessageBroker.PublishMessage(ou1);

            Console.ReadKey();
        }
    }
}
