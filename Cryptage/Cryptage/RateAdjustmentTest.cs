﻿using Common.Types;
using ExchangeAPI;
using System.Linq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Common;
using System.Collections.Concurrent;
using ExchangeAPI.MarketCapProviders;

namespace Cryptage
{
    class RateAdjustmentTest
    {
        private static List<(EdgeKey testEdge, RateItem desiredRateItem, RateItem adjustedRateItem)> items;
        private static ConcurrentQueue<OrdersUpdate> updatesQueue = new ConcurrentQueue<OrdersUpdate>();
        private static Dictionary<EdgeKey, SortedDictionary<double, double>> orderbooks =
            new Dictionary<EdgeKey, SortedDictionary<double, double>>();

        public static void RunRateAdjustmentTest()
        {
             
            var UpdatesFromRest = false;
            Config.WebsocketsEnabled = !UpdatesFromRest;

            double desiredBaseSize;
            string product;
            var testExchange = ExchangeName.Huobi;
            Config.EnabledExchanges = new List<ExchangeName> { testExchange };
            product = "ETC-USDT";
            desiredBaseSize = 0.105;
            switch (testExchange)
            {
                case ExchangeName.Bittrex:
                    desiredBaseSize = 0.003;
                    product = "BCH-ETH";
                    break;
                case ExchangeName.HitBTC:
                    product = "BCH-BTC";
                    desiredBaseSize = 0.002;
                    break; ;
                case ExchangeName.Binance:
                    break;
                case ExchangeName.Huobi:
                case ExchangeName.OKEX:
                    desiredBaseSize = 0.0015;
                    product = "ETH-BTC";
                    break;
                default:
                    throw new Exception("unknown exchange");
            }

            MarketCapAPI.UpdateMarketCap();
            MarketCapAPI.marketCap = new Dictionary<string, MarketCapItem>
            {
                { Exchange.GetBase(product), MarketCapAPI.marketCap[Exchange.GetBase(product)] },
                { Exchange.GetQuote(product), MarketCapAPI.marketCap[Exchange.GetQuote(product)] }
            };

            Exchange.LoadExchanges();

            if (!UpdatesFromRest)
            {
                bool[] sides = { true, false };
                foreach (var isBidBook in sides)
                {
                    var comparer = isBidBook ? Comparer<double>.Create((x, y) => -Comparer<double>.Default.Compare(x, y)) : 
                        Comparer<double>.Default;
                    orderbooks[new EdgeKey(testExchange, Exchange.GetBase(product), Exchange.GetQuote(product), isBidBook)] =
                        new SortedDictionary<double, double>(comparer);
                }
                MessageBroker.OrdersUpdateEvent += (e => { updatesQueue.Enqueue(e); });
                UpdateOrderbook(new TimeSpan(0, 0, 20), out var useless);
            }
                
       

            // wait for exchanges to get products info and balances
            var delay = 8000;
            Thread.Sleep(delay);

            int numOfTests = 10;
            int numOfSuccesses = 0;
            MessageBroker.ExecutionReportEvent += HandleExecutionReport;

            for (int i = 0; i < numOfTests; i++)
            {
                ExecutionRequest executionRequest = new ExecutionRequest(Guid.NewGuid(), testExchange);
                items = new List<(EdgeKey testEdge, RateItem desiredRateItem, RateItem adjustedRateItem)>();
                var baseSize = desiredBaseSize;

                (var bestBidPrice, var bestAskPrice) = (0.0, 0.0);
                if (UpdatesFromRest)
                {
                    (bestBidPrice, bestAskPrice) = PriceQuery.GetBestPrice(testExchange, product, out var sizes);
                    if (sizes.bidSize < baseSize || sizes.askSize < baseSize)
                    {
                        Console.WriteLine("too small candle, retrying...");
                        Thread.Sleep(1000 * 5);
                        continue;
                    }
                }
                else
                {
                    // have update orderbook from last run
                    Console.WriteLine("Updating orderbook...");
                    UpdateOrderbook(new TimeSpan(0, 0, 5), out var currentBestPrices);
                    Console.WriteLine("{0}: {1}: best bid: {2}, best ask: {3}",
                        DateTime.Now, testExchange, currentBestPrices.bestBid, currentBestPrices.bestAsk);

                    while (true)
                    {
                        if (UpdateOrderbook(TimeSpan.Zero, out var newBestPrices))
                        {
                            if (currentBestPrices.bestBid > Config.NumericEpsilon &&
                                currentBestPrices.bestAsk > Config.NumericEpsilon)
                            {
                                if (newBestPrices.bestBid != currentBestPrices.bestBid ||
                                    newBestPrices.bestAsk != currentBestPrices.bestAsk)
                                {
                                    var (bestBidSize, bestAskSize) = GetBestOrdersSizes();
                                    if (bestBidSize >= baseSize && bestAskSize >= baseSize)
                                    {
                                        (bestBidPrice, bestAskPrice) = (newBestPrices.bestBid, newBestPrices.bestAsk);
                                        Console.WriteLine("{0}: {1}: best bid: {2}, best ask: {3}",
                                            DateTime.Now, testExchange, bestBidPrice, bestAskPrice);
                                        break;
                                    }
                                    Console.WriteLine("{0}: to small candle, retrying...");
                                }
                            }
                            currentBestPrices = newBestPrices;
                        }
                        else
                            Thread.Sleep(1);
                    }
                }

                foreach (var IsBidBook in new List<bool> { false, true }) // ask-reply is first
                {
                    var bestRate = IsBidBook ? bestBidPrice : 1 / bestAskPrice;
                    var desiredSrcSize = IsBidBook ? baseSize : baseSize / bestRate;
                    var desiredRateItem = new RateItem(desiredSrcSize, bestRate * desiredSrcSize);
                    var edgeKey = new EdgeKey(testExchange, Exchange.GetBase(product), Exchange.GetQuote(product), IsBidBook);

                    Exchange.AdjustRateItem(edgeKey, desiredRateItem, out RateItem adjustedRateItem);
                    if (!IsBidBook)
                        baseSize = Math.Min(baseSize, adjustedRateItem.DstSize);

                    items.Add((edgeKey, desiredRateItem, adjustedRateItem));
                    executionRequest.Commands.Add(new ExecutionCommand(edgeKey, adjustedRateItem, desiredRateItem, false));
                }
                
                Exchange.GetInstance(testExchange).Execute(executionRequest);

                ExecutionReport executionReport;
                while (!replyQueue.TryDequeue(out executionReport))
                {
                    Thread.Sleep(100);
                }

                foreach (var result in executionReport.ExecutionResult)
                {
                    var edgeKey = result.Item1;
                    if (edgeKey.IsBidBook)
                        continue;

                    var acctualRateItem = result.Item2;
                    var (testEdge, desiredRateItem, adjustedRateItem) = items.Find(ii => ii.testEdge.IsBidBook == edgeKey.IsBidBook);

                    Utils.PrintRateItem("Desired rate", edgeKey, desiredRateItem);
                    Utils.PrintRateItem("Adjusted rate", edgeKey, adjustedRateItem);
                    Utils.PrintRateItem("Actual rate", edgeKey, acctualRateItem);
                    if (Math.Abs(adjustedRateItem.Rate - acctualRateItem.Rate) < Config.NumericEpsilon ||
                        (acctualRateItem.Rate > adjustedRateItem.Rate))
                        numOfSuccesses++;
                }
            }

            Console.WriteLine("{0}: {1} successes out of {2} ({3}%)", testExchange, numOfSuccesses, numOfTests,
                100.0 * numOfSuccesses / numOfTests);

            Config.WebsocketsEnabled = true;
        }

        private static ConcurrentQueue<ExecutionReport> replyQueue = new ConcurrentQueue<ExecutionReport>();
        private static void HandleExecutionReport(ExecutionReport executionReport)
        {
            replyQueue.Enqueue(executionReport);
        }

        private static (double bestBidSize, double bestAskSize) GetBestOrdersSizes()
        {
            (double bestBidSize, double bestAskSize) = (0.0, 0.0);
            foreach (var orderbook in orderbooks)
            {
                if (orderbook.Key.IsBidBook)
                    bestBidSize = orderbook.Value.First().Value;
                else
                    bestAskSize = orderbook.Value.First().Value;

            }
            return (bestBidSize, bestAskSize);
        }

        private static bool UpdateOrderbook(TimeSpan delay, out (double bestBid, double bestAsk) bestPrices)
        {
            var startTime = DateTime.Now;
            var currentTime = startTime;
            var gotUpdates = false;
            while (currentTime <= startTime + delay)
            {
                if (updatesQueue.TryDequeue(out var update))
                {
                    gotUpdates = true;
                    var OverrideOrderbooks = update.OverrideOrderbooks;

                    foreach (var order in update.Orders)
                    {
                        var edgeKey = order.Item1;
                        if (OverrideOrderbooks)
                            orderbooks[edgeKey].Clear();

                        foreach (var item in order.Item2)
                        {
                            var price = item.Key;
                            var size = item.Value;
                            //Console.WriteLine("{0}: {1}: got update, price {2}: size: {3}", DateTime.Now ,edgeKey, price, size);
                            if (size < Config.NumericEpsilon)
                                orderbooks[edgeKey].Remove(price);
                            else
                                orderbooks[edgeKey][price] = size;
                        }
                    }
                }
                else
                    Thread.Sleep(1);

                currentTime = DateTime.Now;
            }

            bestPrices = (0.0, 0.0);
            foreach (var orderbook in orderbooks)
            {
                if (orderbook.Key.IsBidBook)
                    bestPrices.bestBid = orderbook.Value.Count > 0 ? orderbook.Value.First().Key: 0.0;
                else
                    bestPrices.bestAsk = orderbook.Value.Count > 0 ? orderbook.Value.First().Key : 0.0;
            }

            return gotUpdates;
        }
    }
}
