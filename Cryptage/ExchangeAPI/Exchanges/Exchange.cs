﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Collections.Concurrent;
using System.Threading;
using ExchangeAPI.Exchanges;
using ExchangeAPI.MarketCapProviders;
using Common.Types;
using System.Net.NetworkInformation;
using System.Web;
using System.Text;
using System.Net.Security;
using System.Net.Sockets;
using System.IO;
using Common;
using System.Text.Json;

namespace ExchangeAPI
{
    public class Product
    {
        public string Symbol;

        public double BaseMinSize;
        public double BaseStep;
        public int BasePrecision;

        public double PriceMin;
        public double PriceStep;
        public int PricePrecision;

        public double QuoteMinSize;
        public double QuoteStep;
        public int QuotePrecision;

        public double TakerFee;
        public double MakerFee;

        public bool isFeeAlwaysFromQuote;
        private double SetStepSize(double stepSize, int precision, double minSize)
        {
            return stepSize > Config.NumericEpsilon ? stepSize : 
                (precision >= 0 ? Math.Pow(10, -1 * precision) : minSize);
        }

        private double SetMinSize(double minSize, double stepSize, int precision)
        {
            return minSize > Config.NumericEpsilon ? minSize : 
                (stepSize > Config.NumericEpsilon ? stepSize : Math.Pow(10, -1 * precision));
        }

        private int SetPrecision(int precision, double stepSize, double minSize)
        {
            return precision >= 0 ? precision : 
                (stepSize > Config.NumericEpsilon ? Math.Max(0, (int)Math.Log10(1 / stepSize)) :
                Math.Max(0, (int)Math.Log10(1 / minSize)));
        }

        public Product(string Symbol, double TakerFee, double MakerFee, bool isFeeAlwaysFromQuote,
            double BaseStep = 0.0, int BasePrecision = -1, double BaseMinSize = 0.0,
            double QuoteStep = 0.0, int QuotePrecision = -1, double QuoteMinSize = 0.0,
            double PriceStep = 0.0, int PricePrecision = -1, double PriceMin = 0.0)
        {
            if (QuotePrecision < 0 && QuoteStep < Config.NumericEpsilon && QuoteMinSize < Config.NumericEpsilon)
                QuotePrecision = 8;

            this.Symbol = Symbol;
            this.BaseStep = SetStepSize(BaseStep, BasePrecision, BaseMinSize);
            this.PriceStep = SetStepSize(PriceStep, PricePrecision, PriceMin);
            this.QuoteStep = SetStepSize(QuoteStep, QuotePrecision, QuoteMinSize);

            this.BaseMinSize = SetMinSize(BaseMinSize, BaseStep, BasePrecision);
            this.PriceMin = SetMinSize(PriceMin, PriceStep, PricePrecision);
            this.QuoteMinSize = SetMinSize(QuoteMinSize, QuoteStep, QuotePrecision);

            this.BasePrecision = SetPrecision(BasePrecision, BaseStep, BaseMinSize);
            this.PricePrecision = SetPrecision(PricePrecision, PriceStep, PriceMin);
            this.QuotePrecision = SetPrecision(QuotePrecision, QuoteStep, QuoteMinSize);

            this.TakerFee = TakerFee;
            this.MakerFee = MakerFee;
            this.isFeeAlwaysFromQuote = isFeeAlwaysFromQuote;
        }
    }

    public abstract class Exchange
    {
        private static Dictionary<ExchangeName, Exchange> _instances = new Dictionary<ExchangeName, Exchange>();

        public static void LoadExchanges(bool autoLoadWebsockets = true)
        {
            JObject ApiKeys;
            var ApiKeysPath = Environment.OSVersion.Platform == PlatformID.Unix ?
                                    "ApiKeys.json" : "..\\..\\..\\ApiKeys.json";
            using (StreamReader r = new StreamReader(ApiKeysPath))
            {
                ApiKeys = JObject.Parse(r.ReadToEnd());
            }

            foreach (var item in ApiKeys)
            {
                JObject pair = (JObject)item.Value;
                ExchangeName exchangeName = ExchangeNameTranslator.StringToExchangeName(item.Key);
                if (Config.EnabledExchanges.Contains(exchangeName))
                    AddInstance(exchangeName, pair["ApiKey"].ToString(), pair["ApiSecret"].ToString());
            }

            if (autoLoadWebsockets)
                LoadWebsockets();
        }

        public static void LoadWebsockets()
        {
            foreach (var instance in _instances.Values)
                instance.CreateWebsocket();
        }

        public static Exchange GetInstance(ExchangeName name)
        {
            if (!_instances.ContainsKey(name))
                return null;
            return _instances[name];
        }

        public static void AddInstance(ExchangeName exchange, string ApiKey = null, string ApiSecret = null)
        {
            if (_instances.ContainsKey(exchange))
                return;

            Exchange instance = null;
            switch (exchange)
            {
                case ExchangeName.HitBTC:
                    instance = new ExchangeHitBTC(ApiKey, ApiSecret);
                    break;
                case ExchangeName.Poloniex:
                    instance = new ExchangePoloniex(ApiKey, ApiSecret);
                    break;
                case ExchangeName.Bitfinex:
                    instance = new ExchangeBitfinex(ApiKey, ApiSecret);
                    break;
                case ExchangeName.Binance:
                    instance = new ExchangeBinance(ApiKey, ApiSecret);
                    break;
                case ExchangeName.Bittrex:
                    instance = new ExchangeBittrex(ApiKey, ApiSecret);
                    break;
                case ExchangeName.Huobi:
                    instance = new ExchangeHuobi(ApiKey, ApiSecret);
                    break;
                case ExchangeName.Bibox:
                    instance = new ExchangeBibox(ApiKey, ApiSecret);
                    break;
                case ExchangeName.CoinTiger:
                    instance = new ExchangeCoinTiger(ApiKey, ApiSecret);
                    break;
                case ExchangeName.OKEX:
                    instance = new ExchangeOKEX(ApiKey, ApiSecret);
                    break;
                case ExchangeName.BitBay:
                    instance = new ExchangeBitbay(ApiKey, ApiSecret);
                    break;
                default:
                    throw new NotImplementedException();
            }

            _instances[instance.Name] = instance;
        }

        public string ApiKey { get; }
        public string ApiSecret { get; }

        public enum UrlType {
            PrivateUrl, ProductsUrl, TickerUrl, WebsocketUrl, SignalRUrl, OrdersUrl, PingUrl, DepthUrl, CurrenciesUrl
        };

        public enum PrivateRequestType { BalanceRequest, OrderInfoRequest };
        protected virtual int NumWebsockets { get; set; } = 1;
        protected List<WebSocketWrapper> _websockets = new List<WebSocketWrapper>();
        protected List<JsonParser> _jsonParser = new List<JsonParser>();

        private Thread _restThread;
        private ConcurrentQueue<ExecutionRequest> _executionRequestQueue;
        private ConcurrentQueue<(string product, bool isBidCommand, double price, double size, bool autoCancel)> _limitRequestQueue;
        private ConcurrentQueue<UrlType> _urlRequestQueue;
        private ConcurrentQueue<(string product, long orderId)> _orderInfoRequestQueue;
        private ConcurrentQueue<(string product, long orderId)> _orderCancelRequestQueue;

        private Thread _executionThread;
        private ConcurrentQueue<(Guid guid, List<ExecutionCommand> commands, int count, int numDuplicates)> _pendingReportQueue;
        protected ConcurrentQueue<(Guid executionCycleId, EdgeKey edgeKey, RateItem acctual)> _marketOrderResponseQueue;


        protected (Thread thread, ConcurrentQueue<(string, DelayMeasurement, WebSocketWrapper)> queue) _parserThreadQueue;
        protected Thread _pingerThread;
        public Dictionary<string, Product> ProductInfo;
        public Dictionary<string, string> ProductToSymbol;
        public Dictionary<string, string> SymbolToProduct;
        private Dictionary<string, (IPAddress ip, long roundTrip)> hostnameToIP =
            new Dictionary<string, (IPAddress ip, long roundTrip)>();
        Dictionary<IPAddress, string> ipToHostname = new Dictionary<IPAddress, string>();
        public long GetRoundTripMsec()
        {
            return hostnameToIP[new Uri(Urls[UrlType.PrivateUrl]).Host].roundTrip;
        }
        public abstract Dictionary<UrlType, string> Urls { get; set; }
        protected virtual int ApiPingIntervalMsec { get; set; } = 0;
        public abstract ExchangeName Name { get; }
        public abstract (int count, TimeSpan period) RequestLimit { get; set; }
        public Queue<DateTime> RequestLog { get; set; }

        private long WebsocketMsgCounter = 0;
        private long WebsocketMsgLatencyMsec = 0;
        private long WebsocketHighLatencyCounter = 0;
        private long WebsocketDisconnectCounter = 0;
        private long RestDisconnectCounter = 0;
        public static Dictionary<ExchangeName, 
            (int msgCount, int latency, int highLatencyCount,
            int wsQueueCount, int parserQueueCount, int wsDisconnects, int restDisconnects)> MessageCounters {
            get { return _instances.ToDictionary(e => e.Key, e => 
            ((int)Interlocked.Read(ref e.Value.WebsocketMsgCounter),
            (int)Interlocked.Read(ref e.Value.WebsocketMsgLatencyMsec),
            (int)Interlocked.Read(ref e.Value.WebsocketHighLatencyCounter),
            e.Value._websockets.Count > 0 ? e.Value._websockets.Max(ws => ws.MessageQueueCount) : 0,
            e.Value._parserThreadQueue.queue.Count,
            (int)Interlocked.Read(ref e.Value.WebsocketDisconnectCounter),
            (int)Interlocked.Read(ref e.Value.RestDisconnectCounter))
            ); }
        }
        public abstract OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws = null);
        public abstract void _websocketOnConnect(WebSocketWrapper ws);

        public TimeSpan RequestTimeout { get; set; } = TimeSpan.FromSeconds(10.0);

        public static string GetBase(string product) { return product.Split('-')[0]; }
        public static string GetQuote(string product) { return product.Split('-')[1]; }

        protected DateTime UnixStartTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

        protected abstract RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item);
        public static bool AdjustRateItem(EdgeKey edge, RateItem rateItem, out RateItem adjustedRateItem)
        {
            var product = edge.BaseCurrency + "-" + edge.QuoteCurrency;
            var isBidCommand = !edge.IsBidBook;
            adjustedRateItem = _instances[edge.Exchange]._adjustRateItem(product, isBidCommand, rateItem);

            // DstSize > 0 means we have enough SrcSize
            bool isOverMinSize = adjustedRateItem.DstSize > Config.NumericEpsilon;
            return isOverMinSize;
        }

        public Exchange(string ApiKey = null, string ApiSecret = null)
        {
            RequestLog = new Queue<DateTime>();
            this.ApiKey = ApiKey;
            this.ApiSecret = ApiSecret;

            _executionRequestQueue = new ConcurrentQueue<ExecutionRequest>();
            _limitRequestQueue = 
                new ConcurrentQueue<(string product, bool isBidCommand, double price, double size, bool autoCancel)>();
            _urlRequestQueue = new ConcurrentQueue<UrlType>();
            _orderInfoRequestQueue = new ConcurrentQueue<(string product, long orderId)>();
            _orderCancelRequestQueue = new ConcurrentQueue<(string product, long orderId)>();
            _restThread = new Thread(RestMessageLoop);
            _restThread.Start();

            _marketOrderResponseQueue = new ConcurrentQueue<(Guid executionCycleId, EdgeKey edgeKey, RateItem acctual)>();
            _pendingReportQueue = new ConcurrentQueue<(Guid guid, List<ExecutionCommand> commands, int count, int numDuplicates)>();
            _executionThread = new Thread(ExecutionMessageLoop);
            _executionThread.Start();

            var parserQueue = new ConcurrentQueue<(string, DelayMeasurement, WebSocketWrapper)>();
            _parserThreadQueue = (thread: new Thread(() => ParserMessageLoop(parserQueue)),
                queue: parserQueue);
            _parserThreadQueue.thread.Start();

            // API pinger to maintain HTTPS session alive
            if (ApiPingIntervalMsec > 0)
            {
                _pingerThread = new Thread(() =>
                {
                    while (true)
                    {
                        Thread.Sleep(ApiPingIntervalMsec);
                        _urlRequestQueue.Enqueue(UrlType.PingUrl);
                    }
                });
                _pingerThread.Start();
            }
        }

        private void GetCurrencies()
        {
            if (!Thread.CurrentThread.Equals(_restThread))
            {
                throw new APIException("must run from _restThread");
            }

            var result = MakeRequest(UrlType.CurrenciesUrl);
            var badCurrencies = (List<string>)result["bad"];
            FilterBadCurrencies(badCurrencies, "non withdrawable/depositable");
        }

        private void ResolveHosnames(bool updateUrls=false)
        {
            Ping pingSender = new Ping();
            foreach (var item in Urls)
            {
                var minRoundTrip = long.MaxValue;
                IPAddress bestIP = null;
                string hostname = new Uri(item.Value).Host;
                if (hostnameToIP.ContainsKey(hostname))
                    continue;

                IPHostEntry host;
                host = Dns.GetHostEntry(hostname);
                foreach (IPAddress ip in host.AddressList)
                {
                    PingReply reply = pingSender.Send(ip, timeout: 500);
                    if (reply.Status == IPStatus.Success && reply.RoundtripTime < minRoundTrip)
                    {
                        minRoundTrip = reply.RoundtripTime;
                        bestIP = ip;
                    }
                }
                bestIP = bestIP ?? host.AddressList[0];
                ConsoleWriter.WriteLine(string.Format("{0}: resolving {1} to {2}", Name, hostname, bestIP));
                hostnameToIP[hostname] = (bestIP, minRoundTrip == long.MaxValue ? 0 : minRoundTrip);
                ipToHostname[bestIP] = hostname;
            }

            if (!updateUrls)
                return;

            foreach (var item in Urls.ToList())
            {
                if (item.Key == UrlType.WebsocketUrl)
                    continue;

                Uri uri = new Uri(item.Value);
                UriBuilder uriBuilder = new UriBuilder(uri.Scheme, hostnameToIP[uri.Host].ip.ToString(), uri.Port, uri.PathAndQuery);
                Urls[item.Key] = HttpUtility.UrlDecode(uriBuilder.ToString());
            }
        }

        protected virtual string CreateWebsocketURL(string url, int index) => url;

        private void CreateWebsocket()
        {
            if (!(Urls.ContainsKey(UrlType.WebsocketUrl) && (Config.WebsocketsEnabled || WebsocketsAlwaysOn)))
                return;

            if (!Thread.CurrentThread.Equals(_restThread))
            {
                _urlRequestQueue.Enqueue(UrlType.WebsocketUrl);
                return;
            }

            var subscribe = Config.WebsocketsEnabled;

            if (NumWebsockets > SymbolToProduct.Keys.Count)
                NumWebsockets = 1;

            for (int i = 0; i < NumWebsockets; i++)
            {
                _jsonParser.Add(new JsonParser());
                _websockets.Add(new WebSocketWrapper(CreateWebsocketURL(Urls[UrlType.WebsocketUrl], i), 
                    ipToHostname.Keys.ToList(), WebsocketOnMessage, onConnect: WebsocketOnConnect,
                    onDisconnect: WebsocketOnDisconnect, compressionMethod: WebsocketCompression, subscribe: subscribe));
            }
        }

        private static readonly Guid defaultGuid = Guid.NewGuid();
        private void ExecutionMessageLoop()
        {
            while (true)
            {
                var guid = defaultGuid;
                if (_marketOrderResponseQueue.TryDequeue(out var response))
                {
                    guid = response.executionCycleId;
                    int i = 0, sameKeyIndex = -1;
                    for (; i < PendingExecutionReports[guid].items.Count; i++)
                    {
                        if (!PendingExecutionReports[guid].items[i].key.Equals(response.edgeKey))
                            continue;

                        sameKeyIndex = i;
                        if (PendingExecutionReports[guid].items[i].acctual != null)
                            continue;

                        PendingExecutionReports[guid].items[i] = (key: PendingExecutionReports[guid].items[i].key, 
                            requested: PendingExecutionReports[guid].items[i].requested, acctual: response.acctual);
                        break;
                    }

                    if (i == PendingExecutionReports[guid].items.Count)
                        PendingExecutionReports[guid].items.Add((key: response.edgeKey, 
                            requested: PendingExecutionReports[guid].items[sameKeyIndex].requested, acctual: response.acctual));
                }
                else if (_pendingReportQueue.TryDequeue(out var reportInfo))
                {
                    guid = reportInfo.guid;
                    if (PendingExecutionReports.ContainsKey(guid))
                        PendingExecutionReports[guid] =
                            (items: PendingExecutionReports[guid].items, numItems: reportInfo.count, 
                            numDuplicates: reportInfo.numDuplicates);
                    else
                    {
                        PendingExecutionReports[guid] =
                            (new List<(EdgeKey key, RateItem requested, RateItem acctual)>(), 
                            reportInfo.count, reportInfo.numDuplicates);
                        foreach (var command in reportInfo.commands)
                        {
                            PendingExecutionReports[guid].items.Add((key: command.Edge, requested: command.AdjustedRateItem, 
                                acctual: null));
                        }
                    }
                }

                else
                    Thread.Sleep(1);
                
                if (!guid.Equals(defaultGuid))
                {
                    var (items, count, numDuplicates) = PendingExecutionReports[guid];
                    if (items.Where(ii => ii.acctual != null).Count() == count + numDuplicates)
                    {
                        var isSuccessful = items.Where(ii => ii.acctual.Rate > Config.NumericEpsilon).Count() >= count;
                        PrintExecution(items);
                        var acctuals = items.Select(ii => new Tuple<EdgeKey, RateItem>(ii.key, ii.acctual)).ToList();
                        MessageBroker.PublishMessage(new ExecutionReport(guid, Name, acctuals, isSuccessful));
                        PendingExecutionReports.Remove(guid);
                    }
                }
            }
        }

        private void RestMessageLoop()
        {
            ResolveHosnames(updateUrls: false);
            apiHostname = new Uri(Urls[UrlType.ProductsUrl]).Host;
            GetProducts();
            if (Urls.ContainsKey(UrlType.CurrenciesUrl))
                GetCurrencies();
            GetTicker();
            UpdateBalances();
            CreateEdgeInfo();

            while (true)
            {
                if (_executionRequestQueue.TryDequeue(out var request))
                    Execute(request);
                else if (_limitRequestQueue.TryDequeue(out var limitRequest))
                    PlaceLimitOrder(limitRequest.product, limitRequest.isBidCommand, 
                        limitRequest.price, limitRequest.size, limitRequest.autoCancel);
                else if (_orderInfoRequestQueue.TryDequeue(out var orderInfoRequest))
                    GetOrderInfo(orderInfoRequest.orderId, orderInfoRequest.product);
                else if (_orderCancelRequestQueue.TryDequeue(out var orderCancelRequest))
                {
                    if (orderInfoRequest.orderId != 0)
                        CancelOrderById(orderInfoRequest.product, orderInfoRequest.orderId);
                    else
                        CancelOrdersByProduct(orderInfoRequest.product);
                }
                else if (_urlRequestQueue.TryDequeue(out var urlType))
                {
                    switch (urlType)
                    {
                        case UrlType.PingUrl:
                            MakeRequest(UrlType.PingUrl);
                            break;;
                        case UrlType.WebsocketUrl:
                            CreateWebsocket();
                            break;
                        default:
                            break;
                    }
                }
                else
                    Thread.Sleep(1);
            }
        }

        private void ParserMessageLoop(ConcurrentQueue<(string, DelayMeasurement, WebSocketWrapper)> queue)
        {
            while (true)
            {
                if (queue.TryDequeue(out (string, DelayMeasurement, WebSocketWrapper) message))
                {
                    message.Item2.Add("parse queue");
                    OrdersUpdate result = _websocketParseMessge(message.Item1, message.Item2, message.Item3);

                    if (result != null)
                    {
                        result.Measurement.Add("message parse");
                        MessageBroker.PublishMessage(result);
                        WebsocketMsgCounter++;

                        var alpha = 0.7;
                        var currenctLatency = result.Latency.TotalMilliseconds;
                        if (currenctLatency > Config.WebsocketHighLatencyThresholdMsec)
                            Interlocked.Increment(ref WebsocketHighLatencyCounter);

                        WebsocketMsgLatencyMsec = (long)(alpha * currenctLatency + (1 - alpha) * WebsocketMsgLatencyMsec);
                    }
                }
                else
                    Thread.Sleep(1);
            }
        }

        protected virtual Dictionary<string, string> NonStandardCurrency { get; set; } = new Dictionary<string, string>();
        protected virtual CompressionMethod WebsocketCompression { get; set; } = CompressionMethod.None;
        protected virtual bool WebsocketsAlwaysOn { get; set; } = false;

        protected virtual string StandartizeCurrency(string currency)
        {
            if (NonStandardCurrency.TryGetValue(currency, out string standartCurrency))
                return standartCurrency;
            else
                return currency;
        }

        protected abstract Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args);

        public Dictionary<string, object> ProcessRequestTest(UrlType requestType, Dictionary<string, string> args)
        {
            return ProcessRequest(requestType, args);
        }

        protected abstract Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response,
            Dictionary<string, string> args);

        public string ToBase64(string plainText)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
            return Convert.ToBase64String(plainTextBytes);
        }

        public string UrlEncode(Dictionary<string, string> data, bool sorted = false)
        {
            string encoded = "";
            if (sorted)
                data = data.OrderBy(key => EncodeHex(key.Key))
                    .ToDictionary((keyItem) => keyItem.Key, (valueItem) => valueItem.Value);

            foreach (var item in data)
                encoded += item.Key + "=" + Uri.EscapeDataString(item.Value) + "&";

            return encoded.TrimEnd('&');
        }

        private SslStream sslStream = null;
        private NetworkStream networkStream = null;
        private string apiHostname;

        public Dictionary<string, object> MakeRequest(UrlType requestType, Dictionary<string, string> args = null,
            bool urgent = false)
        {
            int numRetries = 0;
            while (numRetries < Config.HttpMaxRetries)
            {
                (var result, var statusCode) = 
                    MakeBatchRequest(requestType, new List<Dictionary<string, string>> { args }, count: 1, urgent: urgent)[0];
                if (statusCode == HttpStatusCode.OK || (statusCode == HttpStatusCode.NoContent && requestType == UrlType.PingUrl))
                    return result;
                numRetries++;
                ConsoleWriter.WriteLine("{0}: {1}: MakeRequest: {2}: Got {3}, retry {4} out of {5}",
                    DateTime.Now.ToString("hh:mm:ss.fff"), Name, requestType, statusCode, numRetries, Config.HttpMaxRetries);
            }
            var errorMsg = string.Format("{0}: {1}: MakeRequest num retries exceeded", DateTime.Now.ToString("hh:mm:ss.fff"), Name);
            if (requestType != UrlType.PingUrl)
                throw new APIException(errorMsg);
            else
                ConsoleWriter.WriteLine(errorMsg);
            return null;
        }

        private byte[] combinedHttpBuffer = new byte[Config.MaxHttpBufferLen * Config.MaxOrdersInExecution];

        public List<(Dictionary<string, object> result, HttpStatusCode statusCode)> MakeBatchRequest(UrlType requestType,
            List<Dictionary<string, string>> argsList = null, int count = 1, 
            bool urgent = false, DelayMeasurement triggerMeasurement = null)
        {
            // time critical section start
            DateTime now = DateTime.Now;
            if (urgent)
            {
                triggerMeasurement?.Add("rest queue");
                ConsoleWriter.WriteLine("{0}: {1}: MakeBatchRequest: starting {2} requests", 
                    now.ToString("hh:mm:ss.fff"), Name, count);
            }

            var results = new List<(Dictionary<string, object> result, HttpStatusCode statusCode)>();
            void AddEmptyResults(int resNum)
            {
                foreach (var i in Enumerable.Range(start: 0, count: resNum))
                    results.Add((null, HttpStatusCode.NoContent));
            }
            
            while (RequestLog.Count > 0 && now > RequestLog.Peek() + RequestLimit.period)
                RequestLog.Dequeue();

            if (RequestLog.Count >= RequestLimit.count)
            {
                if (urgent)
                {
                    ConsoleWriter.WriteLine("{0}: {1}: MakeBatchRequest failed: can't sleep on urgent request",
                        DateTime.Now.ToString("hh:mm:ss.fff"), Name);
                    AddEmptyResults(argsList.Count);
                    return results;
                }
                TimeSpan time = now - RequestLog.Peek();
                if (time.TotalMilliseconds < 0.0)
                    time = -time; // can be caused by time update from NTP
                ConsoleWriter.WriteLine("{0}: {1}: MakeBatchRequest sleeping for {2} because of request rate limit",
                   DateTime.Now.ToString("hh:mm:ss.fff"),  Name, time);
                Thread.Sleep(time);
            }
            else if (!urgent)
            {
                TimeSpan time = TimeSpan.FromSeconds(RequestLimit.period.TotalSeconds / RequestLimit.count);
                Thread.Sleep(time);
            }

            if (requestType == UrlType.PingUrl && _executionRequestQueue.Count() > 0)
            {
                AddEmptyResults(argsList.Count);
                return results;
            }
          
            try
            {
                if (sslStream == null)
                    (sslStream, networkStream) = Utils.SslConnectApi(apiHostname, ipToHostname.Keys.ToList());

                var writeOffset = 0;
                var totalNumDuplicates = 0;
                var currentNumDuplicates = 0;
                var duplicateIndex = new List<int>();
                for (int i = 0; i < count; i++)
                {
                    var requestParams = ProcessRequest(requestType, argsList[i]);
                    string url = Urls[requestType];

                    if (requestParams.ContainsKey("url"))
                        url = (string)requestParams["url"];
                    else if (url.Contains("{0}"))
                        url = string.Format(url, requestParams["command"]);
                    
                    if (requestParams.ContainsKey("query"))
                        url += "?" + requestParams["query"];

                    currentNumDuplicates = 0;
                    if (requestParams.TryGetValue("duplicate", out var doDuplication) && (bool)doDuplication)
                    {
                        currentNumDuplicates = Config.NumOfDuplicatesAfterAskReply;
                        duplicateIndex.Add(totalNumDuplicates + i);
                        //for (int j = 0; j < currentNumDuplicates; j++)
                        //    argsList[count + totalNumDuplicates + j] = argsList[i]; // required for ProcessResponse
                    }

                    writeOffset = Utils.CreateHttpRequest(url, requestParams, combinedHttpBuffer, writeOffset, currentNumDuplicates);
                    totalNumDuplicates += currentNumDuplicates;
                }

                count += totalNumDuplicates;
                foreach (var i in Enumerable.Range(0, count))
                    RequestLog.Enqueue(DateTime.Now);

                triggerMeasurement?.Add("rest requests creation");
                Utils.WriteSslStream(sslStream, combinedHttpBuffer, writeOffset);
                triggerMeasurement?.Add("rest requests sent");
                // time critical section end

                // required for ProcessResponse
                foreach (var index in duplicateIndex)
                    argsList.InsertRange(index, Enumerable.Repeat(argsList[index], Config.NumOfDuplicatesAfterAskReply));  

                if (urgent)
                {
                    ConsoleWriter.WriteLine("{0}: {1}: MakeBatchRequest: {2} requests sent",
                        DateTime.Now.ToString("hh:mm:ss.fff"), Name, count);
                }

                for (int i = 0; i < count; i++)
                {
                    (HttpStatusCode statusCode, var resonseHeaders, var responseString) = Utils.ReceiveHttpResponse(sslStream);
                    Dictionary<string, object> result = null;
                    switch (statusCode)
                    {
                        case HttpStatusCode.OK:
                            try
                            {
                                JToken response = JToken.Parse(responseString);
                                result = ProcessResponse(requestType, response, argsList[i]);
                            }
                            catch
                            {
                                ConsoleWriter.WriteLine("{0}: failed to process response to {1}", Name, requestType);
                            }
                            break;
                        default:
                            ConsoleWriter.WriteLine("{0}: {1}: ReceiveHttpResponse failed with status code {2}:", 
                                DateTime.Now.ToString("hh:mm:ss.fff"), Name, statusCode);
                            ConsoleWriter.Write(responseString + "\r\n");
                            break;
                    }
                    results.Add((result, statusCode));
                }
            }
            catch (Exception e)
            {
                if (!(e is EndOfStreamException || e is IOException))
                    throw e;
                ConsoleWriter.WriteLine(string.Format("{0}: {1}: MakeBatchRequest failed: {2}", 
                    DateTime.Now.ToString("hh:mm:ss.fff"), Name, e.Message));
                Interlocked.Increment(ref RestDisconnectCounter);
                Utils.SslDispose(sslStream, networkStream);
                (sslStream, networkStream) = Utils.SslConnectApi(apiHostname, ipToHostname.Keys.ToList());

                // enqueue ping request
                RequestLog.Clear();
                _urlRequestQueue.Enqueue(UrlType.PingUrl);

                // fill remaining results
                AddEmptyResults(argsList.Count - results.Count);
            }
            return results;
        }

        public static void HandleExecutionRequests(List<ExecutionRequest> requests)
        {
            foreach (var request in requests)
                GetInstance(request.Exchange).Execute(request);
        }

        private void PrintExecution(List<(EdgeKey key, RateItem requested, RateItem acctual)> items)
        {
            foreach (var (key, requested, acctual) in items)
            {
                string status;
                if (acctual.DstSize < Config.NumericEpsilon)
                    status = "failed";
                else if (acctual.DstSize - requested.DstSize >= -1 * Config.NumericEpsilon)
                    status = "won";
                else
                    status = "lost";

                ConsoleWriter
                    .WriteLine("{0}: execute values: {1} ({2}) {3} -> {4} ({5}) {6} (price: {7} ({8}), size: {9} ({10}) - {11}", 
                    Name, 
                    requested.SrcSize, acctual.SrcSize, key.SrcNode.Currency,
                    requested.DstSize, acctual.DstSize, key.DstNode.Currency,
                    requested.ExecutionPrice, acctual.ExecutionPrice,
                    requested.ExecutionSize, acctual.ExecutionSize,
                    status);
            }
        }

        public static Tuple<EdgeKey, RateItem> CreateFailedExecutionItem(EdgeKey key)
        {
            return new Tuple<EdgeKey, RateItem>(key, new RateItem(0.0, 0.0, "0.0", "0.0"));
        }

        private static List<Dictionary<string, string>> InitArgList(int count)
        {
            var list = new List<Dictionary<string, string>>();
            for (int i = 0; i < count; i++)
                list.Add(new Dictionary<string, string> { { "size", "" }, { "product", "" }, { "command", "" } });
            return list;
        }

        private List<Dictionary<string, string>> orderArgList = InitArgList(count: Config.MaxOrdersInExecution);
        protected Dictionary<long, Guid> ExecutionCycleIdByOrderId = new Dictionary<long, Guid>();
        public void Execute(ExecutionRequest request)
        {
            if (!Thread.CurrentThread.Equals(_restThread))
            {
                _executionRequestQueue.Enqueue(request);
                return;
            }

            // time critical section start
            _pendingReportQueue.Enqueue((request.ExecutionCycleId, request.Commands, int.MaxValue, 0));

            for (int i = 0; i < request.Commands.Count; i++)
            {
                var item = request.Commands[i];
                var product = item.Edge.BaseCurrency + "-" + item.Edge.QuoteCurrency;

                orderArgList[i]["product"] = product;
                orderArgList[i]["size"] = item.AdjustedRateItem.ExecutionSize;
                orderArgList[i]["price"] = item.AdjustedRateItem.ExecutionPrice;
                orderArgList[i]["command"] = !item.Edge.IsBidBook ? "bid" : "ask";
                var doDuplicate = i > 0 && !request.Commands[i - 1].Edge.IsBidBook && !item.IsStandaloneFundedSrc;
                orderArgList[i]["duplicate"] = doDuplicate ? "1" : "0";
            }

            var resultsList = MakeBatchRequest(UrlType.PrivateUrl, orderArgList, 
                count: request.Commands.Count, urgent: true, triggerMeasurement: request.TriggerUpdateMeasurement);
            // time critical section end

            orderArgList = orderArgList.Take(Config.MaxOrdersInExecution).ToList(); // can grow because of duplicates
            if (request.TriggerUpdateMeasurement != null)
                ConsoleWriter.WriteLine("{0}", request.TriggerUpdateMeasurement.ToString());

            var numDuplicates = resultsList.Count - request.Commands.Count;
            _pendingReportQueue.Enqueue((request.ExecutionCycleId, request.Commands, request.Commands.Count, numDuplicates));
            if (numDuplicates > 0)
            {
                // has duplicates
                foreach (var i in Enumerable.Range(0, numDuplicates))
                    request.Commands.Add(request.Commands[request.Commands.Count - 1]);
            }
            
            for (int i = 0; i < request.Commands.Count; i++)
            {
                var result = resultsList[i].result;
                var statusCode = resultsList[i].statusCode;
                double srcSize = 0.0, dstSize = 0.0;
                string size = "0.0", price = "0.0";
                bool isSuccessful = statusCode == HttpStatusCode.OK && (bool)result["success"];
                if (isSuccessful)
                {
                    srcSize = (double)result["srcSize"];
                    dstSize = (double)result["dstSize"];
                    size = result["size"].ToString();
                    price = result["price"].ToString();
                }

                var resultItem = new Tuple<EdgeKey, RateItem> (request.Commands[i].Edge, 
                    new RateItem(srcSize, dstSize, size, price));
                if (isSuccessful)
                {
                    if (srcSize > Config.NumericEpsilon)
                    {
                        _marketOrderResponseQueue.Enqueue((request.ExecutionCycleId, resultItem.Item1, resultItem.Item2));
                    }
                    else
                    {
                        var orderId = (long)result["orderId"];
                        string product = null;
                        if (result.TryGetValue("product", out object obj))
                            product = obj.ToString();
                        ExecutionCycleIdByOrderId[orderId] = request.ExecutionCycleId;
                        GetOrderInfo(orderId, product);
                    }
                }
                else
                {
                    ConsoleWriter.WriteLine("{0}: Execute: {1}: {2} market order {3} failed", Name, 
                        orderArgList[i]["product"], !request.Commands[i].Edge.IsBidBook ? "bid" : "ask", i + 1);
                    _marketOrderResponseQueue.Enqueue((request.ExecutionCycleId, resultItem.Item1, resultItem.Item2));
                }
            }
        }

        public void PlaceLimitOrder(string product, bool isBidCommand, double price, double size, bool autoCancel = false)
        {
            if (!Thread.CurrentThread.Equals(_restThread))
            {
                _limitRequestQueue.Enqueue((product, isBidCommand, price, size, autoCancel));
                return;
            }

            var orderArgs = new Dictionary<string, string> {
                                    { "size", size.ToString(string.Format("F{0}", ProductInfo[product].BasePrecision)) },
                                    { "price", price.ToString(string.Format("F{0}", ProductInfo[product].PricePrecision)) },
                                    { "product", product },
                                    { "command", isBidCommand ? "bid" : "ask" },
                                    { "type", "limit" }
                                };

            var orderResult = MakeRequest(UrlType.PrivateUrl, orderArgs, true);
            var isSuccessful = (bool)orderResult["success"];
            if (!isSuccessful)
            {
                ConsoleWriter.WriteLine("{0}: {1}: PlaceLimitOrder: request with size {2} at price {3} failed",
                    DateTime.Now.ToString("hh:mm:ss.fff"), Name, size, price);
            }
            else
            {
                ConsoleWriter.WriteLine("{0}: {1}: PlaceLimitOrder: request with size {2} at price {3} succeeded",
                    DateTime.Now.ToString("hh:mm:ss.fff"), Name, size, price);
                if (autoCancel)
                {
                    CancelOrderById(product, (long)orderResult["orderId"]);
                }
            }

        }

        public void CancelOrderById(string product, long orderId)
        {
            if (!Thread.CurrentThread.Equals(_restThread))
            {
                _orderCancelRequestQueue.Enqueue((product, orderId));
                return;
            }

            var args = new Dictionary<string, string> {
                { "command", "cancel" }, { "orderId", orderId.ToString() }, { "product", product }
            };
            var result = MakeRequest(UrlType.PrivateUrl, args);
            ConsoleWriter.WriteLine("{0}: {1}: CancelOrderById got reply", DateTime.Now.ToString("hh:mm:ss.fff"), Name);
        }

        public void CancelOrdersByProduct(string product)
        {
            if (!Thread.CurrentThread.Equals(_restThread))
            {
                _orderCancelRequestQueue.Enqueue((product, 0));
                return;
            }

            var args = new Dictionary<string, string> { { "command", "cancel" }, { "product", product } };
            var result = MakeRequest(UrlType.PrivateUrl, args);
            ConsoleWriter.WriteLine("{0}: {1}: CancelOrdersByProduct got reply", DateTime.Now.ToString("hh:mm:ss.fff"), Name);
        }

        protected virtual void GetOrderInfo(long orderId, string product = null)
        {
            if (!Thread.CurrentThread.Equals(_restThread))
            {
                _orderInfoRequestQueue.Enqueue((product, orderId));
                return;
            }

            var args = new Dictionary<string, string>
            {
                { "orderId", orderId.ToString() },
                { "command", "order" }
            };
            if (product != null)
                args["product"] = product;
            var result = MakeRequest(UrlType.PrivateUrl, args);
            if (result != null) // ToDo: handle MakeRequest failure
            {
                var executionCycleId = ExecutionCycleIdByOrderId[orderId];
                ExecutionCycleIdByOrderId.Remove(orderId);

                var resultProduct = (string)result["product"];
                var isBidCommand = result["side"].Equals("bid");
                _marketOrderResponseQueue.Enqueue((
                    executionCycleId, new EdgeKey(Name, GetBase(resultProduct), GetQuote(resultProduct), !isBidCommand),
                        new RateItem(
                            (double)result["srcSize"], (double)result["dstSize"], 
                            result["size"].ToString(), result["price"].ToString()
                            )
                    )
                );
            }
        }

        private Dictionary<Guid, (List<(EdgeKey key, RateItem requested, RateItem acctual)> items, int numItems, int numDuplicates)>
            PendingExecutionReports = new Dictionary<Guid, (List<(EdgeKey key, RateItem requested, RateItem acctual)> items, 
                int numItems, int numDuplicates)>();

        private System.Diagnostics.Stopwatch WebsockStopWatch = new System.Diagnostics.Stopwatch();
        public void WebsocketOnMessage(string message, DelayMeasurement measurement, WebSocketWrapper ws)
        {
            _parserThreadQueue.queue.Enqueue((message, measurement, ws));
        }

        public void WebsocketOnDisconnect(WebSocketWrapper ws)
        {
            Interlocked.Increment(ref WebsocketDisconnectCounter);
        }

        protected int maxSubscriptions = 0;
        protected void SetWebsockNumSubscribeSymbols(WebSocketWrapper ws)
        {
            var index = _websockets.IndexOf(ws);
            var symbolCount = SymbolToProduct.Keys.Count;
            if (symbolCount % NumWebsockets == 0)
                maxSubscriptions = symbolCount / NumWebsockets;
            else
                maxSubscriptions = (symbolCount + (NumWebsockets - symbolCount % NumWebsockets)) / NumWebsockets;

            Interlocked.Exchange(ref ws.SubscribeCounter, 0);
            Interlocked.Exchange(ref ws.NumSubscribeProducts, Math.Min(maxSubscriptions, symbolCount - index * maxSubscriptions));
        }

        protected List<string> GetWebsocketSubscribeSymbols(WebSocketWrapper ws)
        {
            var startIndex = _websockets.IndexOf(ws) * maxSubscriptions;
            return SymbolToProduct.Keys.Skip(startIndex).Take((int)Interlocked.Read(ref ws.NumSubscribeProducts)).ToList();
        }

        public void WebsocketOnConnect(WebSocketWrapper ws)
        {
            var updates = new OrdersUpdate(overrideOrderbooks: true, 
                measurement: new DelayMeasurement(Name.ToString()), latency: new TimeSpan(0));
            var emptyOrderbook = new Dictionary<double, double>();
            SetWebsockNumSubscribeSymbols(ws);
            foreach (var symbol in GetWebsocketSubscribeSymbols(ws))
            {
                var product = SymbolToProduct[symbol];
                updates.Add(new EdgeKey(Name, GetBase(product), GetQuote(product), isBidBook: true), emptyOrderbook);
                updates.Add(new EdgeKey(Name, GetBase(product), GetQuote(product), isBidBook: false), emptyOrderbook);
            }
            MessageBroker.PublishMessage(updates);
            _websocketOnConnect(ws);
        }

        private void GetProducts()
        {
            if (!Thread.CurrentThread.Equals(_restThread))
            {
                throw new APIException("must run from _restThread");
            }

            ProductInfo = new Dictionary<string, Product>();
            MakeRequest(requestType: UrlType.ProductsUrl);
            var numProducts = ProductInfo.Keys.Count;
            ProductInfo = ProductInfo.Where(p =>
            {
                var quoteC = GetQuote(p.Key);
                var baseC = GetBase(p.Key);
                var res = MarketCapAPI.MarketCap.ContainsKey(quoteC) && MarketCapAPI.MarketCap.ContainsKey(baseC);
                if (!res)
                {
                    ConsoleWriter.WriteLine(string.Format("{0}: Removing product {1} (no market cap)", Name, p.Key));
                    return res;
                }
                if (Config.ExcludedNodes.TryGetValue(Name, out HashSet<string> excluded))
                {
                    res = !(excluded.Contains(quoteC) || excluded.Contains(baseC));
                    if (!res)
                        ConsoleWriter.WriteLine(string.Format("{0}: Removing product {1} (excluded)", Name, p.Key));
                }
                
                return res;
                
            }).ToDictionary(p => p.Key, p => p.Value);

            ConsoleWriter.WriteLine(string.Format("{0}: Received info on {1} products ({2} after filter)", 
                Name, numProducts, ProductInfo.Keys.Count));

            ProductToSymbol = new Dictionary<string, string>();
            SymbolToProduct = new Dictionary<string, string>();
            foreach (string product in ProductInfo.Keys)
            {
                string symbol = ProductInfo[product].Symbol;
                ProductToSymbol[product] = symbol;
                SymbolToProduct[symbol] = product;
            }
        }

        private void GetTicker()
        {
            if (!Thread.CurrentThread.Equals(_restThread))
            {
                throw new APIException("must run from _restThread");
            }

            if (!Urls.ContainsKey(UrlType.TickerUrl))
            {
                throw new APIException(string.Format("{0}: WARNING: no ticker url", Name));
            }

            var result = MakeRequest(requestType: UrlType.TickerUrl);
            var maxPriceDeviation = 30.0 / 100; // 30 precent
            var badCurrencies = new HashSet<string>();
            foreach (var item in result)
            {
                var product = item.Key;
                if (!GetQuote(product).Equals("BTC"))
                    continue;
                var tickerPriceBTC = (double)item.Value;
                var priceBTC = MarketCapAPI.MarketCap[GetBase(product)].PriceBTC;
                var priceDeviation = Math.Abs(tickerPriceBTC - priceBTC) / priceBTC;
                if (priceDeviation > maxPriceDeviation)
                    badCurrencies.Add(GetBase(product));
            }
            FilterBadCurrencies(badCurrencies.ToList(), "price deviation from MarketCapApi");
        }

        private void FilterBadCurrencies(List<string> badCurrencies, string filterReason)
        {
            foreach (var currency in badCurrencies)
            {
                var badProducts = ProductInfo.Keys.Where(p => GetBase(p).Equals(currency) || GetQuote(p).Equals(currency)).ToList();
                foreach (var product in badProducts)
                {
                    ConsoleWriter.WriteLine("{0}: Removing {1} ({2})", Name, product, filterReason);
                    ProductInfo.Remove(product);
                    if (ProductToSymbol.TryGetValue(product, out var symbol))
                    {
                        ProductToSymbol.Remove(product);
                        SymbolToProduct.Remove(symbol);
                    }
                }
            }
        }

        private void CreateEdgeInfo()
        {
            if (!Thread.CurrentThread.Equals(_restThread))
            {
                throw new APIException("must run from _restThread");
            }

            Dictionary<EdgeKey, EdgeInfo> edgeInfos = new Dictionary<EdgeKey, EdgeInfo>();
            foreach (string product in ProductInfo.Keys)
            {
                string baseCurrency = product.Split('-')[0];
                string quoteCurrency = product.Split('-')[1];
                double TakerFee = ProductInfo[product].TakerFee;
                double MakerFee = ProductInfo[product].MakerFee;
                double baseMinSize = ProductInfo[product].BaseMinSize;
                bool[] sides = { true, false };
                foreach (var isBidCommand in sides)
                {
                    double feeFactor;
                    if (ProductInfo[product].isFeeAlwaysFromQuote)
                        feeFactor = isBidCommand ? 1.0 / (1 + TakerFee) : (1 - TakerFee);
                    else
                        feeFactor = 1 - TakerFee;
                    edgeInfos[new EdgeKey(Name, baseCurrency, quoteCurrency, !isBidCommand)] = 
                        new EdgeInfo(baseMinSize, feeFactor);
                }
            }
            MessageBroker.PublishMessage(new EdgeInfoUpdate(edgeInfos));
        }

        public void UpdateBalances()
        {
            if (!Thread.CurrentThread.Equals(_restThread))
            {
                throw new APIException("must run from _restThread");
            }

            Dictionary<string, string> args = new Dictionary<string, string> { { "command", "balance" } };
            Dictionary<string, object> result = MakeRequest(requestType: UrlType.PrivateUrl, args: args);
            var numCurrencies = result.Count;
            result = result.Where(p =>
            {
                return MarketCapAPI.MarketCap.ContainsKey(p.Key) && ((double)p.Value > Config.NumericEpsilon);
            }).ToDictionary(p => p.Key, p => p.Value);

            ConsoleWriter.WriteLine(string.Format("{0}: Balance updated for {1} currencies ({2} after filter)", 
                Name, numCurrencies, result.Count));

            ExchangeFundsSnapshot snapshot = new ExchangeFundsSnapshot(Name, result.ToDictionary(r => r.Key, r => (double)r.Value));
            MessageBroker.PublishMessage(snapshot);
        }

        protected string ByteToString(byte[] buff, bool uppercase=true)
        {
            string sbinary = "";
            string format = uppercase ? "X2" : "x2";
            for (int i = 0; i < buff.Length; i++)
                sbinary += buff[i].ToString(format); /* hex format */
            return sbinary;
        }

        protected string EncodeHex(string input)
        {
            string hexEncoded = "";
            foreach (var c in input)
            {
                hexEncoded += string.Format("{0:X}", Convert.ToInt32(c));
            }
            return hexEncoded;
        }

        public static byte[] HexStringToBytes(string hexString)
        {
            if (hexString == null)
                throw new ArgumentNullException("hexString");
            if (hexString.Length % 2 != 0)
                throw new ArgumentException("hexString must have an even length", "hexString");
            var bytes = new byte[hexString.Length / 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                string currentHex = hexString.Substring(i * 2, 2);
                bytes[i] = Convert.ToByte(currentHex, 16);
            }
            return bytes;
        }
    }


}
