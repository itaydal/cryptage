﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Common;
using Common.Types;
using Newtonsoft.Json.Linq;

namespace ExchangeAPI.Exchanges
{
    public class ExchangeBibox : Exchange
    {
        public ExchangeBibox(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
        }

        public override ExchangeName Name => ExchangeName.Bibox;
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string>
        {
            { UrlType.ProductsUrl, "https://api.bibox.com/v1/mdata?cmd=pairList" },
            { UrlType.PrivateUrl, "https://api.bibox.com/v1/{0}"},
            { UrlType.WebsocketUrl, "wss://push.bibox.com" }
            //{ UrlType.WebsocketUrl, "wss://mopush.bibox360.com" }
        };
        public override (int count, TimeSpan period) RequestLimit { get; set; } = (6, new TimeSpan(0, 0, 1));

        private int subscribeIndex;
        private List<KeyValuePair<string, string>> SymbolsList;
        void SubscribeToProduct(WebSocketWrapper ws)
        {
            var item = SymbolsList[subscribeIndex];
            ConsoleWriter.WriteLine(string.Format("{0}: Subscribing to {1} ({2} out of {3})",
                Name, item.Value, subscribeIndex + 1, SymbolsList.Count));
            var message = string.Format(
                "{{\"event\":\"addChannel\",\"channel\":\"bibox_sub_spot_{0}_depth\",\"binary\":1, \"top\":10}}", item.Key);
            ws.SendMessage(message);
            subscribeIndex++;
        }
        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            subscribeIndex = 0;
            SymbolsList = SymbolToProduct.ToList();
            SubscribeToProduct(ws);
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws = null)
        {
            if (subscribeIndex < SymbolsList.Count)
                SubscribeToProduct(ws); 

            JArray token = JArray.Parse(message);
            JToken data;
            if (token[0]["binary"].ToString().Equals("1"))
            {
                var stream = new MemoryStream(Convert.FromBase64String(token[0]["data"].ToString()));
                var decompressionStream = new GZipStream(stream, CompressionMode.Decompress);
                data = JToken.Parse(new StreamReader(decompressionStream).ReadToEnd());
            }
            else
                data = token[0]["data"];

            var product = SymbolToProduct[data["pair"].ToString()];
            var latency = NtpAgent.GetAccurateCurrentUnixTime() - 
                UnixStartTime.AddMilliseconds(data["update_time"].ToObject<long>());
            OrdersUpdate ordersUpdate = new OrdersUpdate(true, measurement, latency, isDiffUpdate: false);
            bool hasUpdates = false;
            bool[] orderSides = { true, false };
            foreach (bool isBid in orderSides)
            {
                int numUpdates = 0;
                Dictionary<double, double> update = new Dictionary<double, double>();
                foreach (var order in (JArray)data[isBid ? "bids" : "asks"])
                {
                    if (numUpdates++ == Config.MaxNumOrderUpdates)
                        break;
                    var price = order["price"].ToObject<double>();
                    update[price] = order["volume"].ToObject<double>();
                }
                if (update.Count > 0)
                {
                    ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), isBid), update);
                    hasUpdates = true;
                }
            }
            return hasUpdates ? ordersUpdate : null;
        }

        protected override Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args)
        {
            Dictionary<string, object> requestParams = new Dictionary<string, object>();
            switch (requestType)
            {
                case UrlType.ProductsUrl:
                    requestParams["method"] = "GET";
                    break;
                case UrlType.PrivateUrl:
                    string cmds = null;
                    requestParams["method"] = "POST";
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        throw new NotImplementedException();
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        cmds = "[{\"cmd\":\"transfer/assets\",\"body\":{\"select\":1}}]";
                        requestParams["command"] = "transfer";
                    }

                    string signature;
                    using (var hmac = new HMACMD5(Encoding.UTF8.GetBytes(ApiSecret)))
                    {
                        var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(cmds));
                        signature = ByteToString(hash, uppercase: false);
                    }

                    requestParams["content"] = UrlEncode(new Dictionary<string, string>
                    {
                        { "cmds", cmds }, { "apikey", ApiKey }, { "sign", signature }
                    });
                    break;

                default:
                    throw new NotImplementedException();
            }
            return requestParams;
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, 
            Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            switch (requestType)
            {
                case UrlType.ProductsUrl:
                    ProductInfo = new Dictionary<string, Product>();
                    foreach (var item in (JArray)response["result"])
                    {
                        var symbol = item["pair"].ToString();
                        var currencies = symbol.Split('_');
                        string product = StandartizeCurrency(currencies[0]) + "-" + StandartizeCurrency(currencies[1]);
                        int basePrecision = 4;
                        int pricePrecision = (GetQuote(product).Equals("BTC") || GetQuote(product).Equals("ETH")) ? 8 : 4;
                        ProductInfo[product] = new Product(Symbol: symbol, TakerFee: 0.1 / 100, MakerFee: 0.1 / 100,
                            isFeeAlwaysFromQuote: false, BasePrecision: basePrecision,
                            PricePrecision: pricePrecision); // TODO: validate all
                    }
                    break;
                case UrlType.PrivateUrl:
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        throw new NotImplementedException();
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        result = new Dictionary<string, object>();
                        foreach (var item in (JArray)response["result"][0]["result"]["assets_list"])
                        {
                            var currency = StandartizeCurrency(item["coin_symbol"].ToString());
                            result[currency] = item["balance"].ToObject<double>() - item["freeze"].ToObject<double>();
                        }
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            throw new NotImplementedException();
        }
    }
}
