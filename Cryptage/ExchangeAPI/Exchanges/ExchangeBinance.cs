﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Common.Types;
using Common;
using System.Collections;

namespace ExchangeAPI.Exchanges
{
    public class ExchangeBinance: Exchange
    {
        public ExchangeBinance(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
        }

        public override ExchangeName Name => ExchangeName.Binance;
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string>
        {
            { UrlType.ProductsUrl, "https://api.binance.com/api/v1/exchangeInfo" },
            { UrlType.TickerUrl, "https://api.binance.com/api/v1/ticker/24hr"},
            { UrlType.WebsocketUrl, "wss://stream.binance.com:9443/stream?streams={0}" },
            //{ UrlType.WebsocketUrl, "wss://stream2.binance.cloud:443/stream?streams={0}" },
            { UrlType.PrivateUrl, "https://api.binance.com/api/v3/{0}"},
            { UrlType.PingUrl, "https://api.binance.com/api/v1/time" },
            { UrlType.DepthUrl, "https://api.binance.com/api/v1/depth?symbol={0}&limit={1}" }
        };
        protected override int ApiPingIntervalMsec { get; set; } = 60 * 1000; // disconnects after 200 seconds
        public override (int count, TimeSpan period) RequestLimit { get; set; } = (60, new TimeSpan(0, 0, 60));
      
        protected override Dictionary<string, string> NonStandardCurrency { get; set; } = new Dictionary<string, string>
        {
            { "BCC", "BCH" }, { "IOTA", "MIOTA" }, { "YOYO", "YOYOW" }, { "BQX", "ETHOS" }
        };

        protected override string CreateWebsocketURL(string url, int index)
        {
            string sep = "@depth";
            var symbolCount = SymbolToProduct.Keys.Count;
            int maxSubscriptions;
            if (symbolCount % NumWebsockets == 0)
                maxSubscriptions = symbolCount / NumWebsockets;
            else
                maxSubscriptions = (symbolCount + (NumWebsockets - symbolCount % NumWebsockets)) / NumWebsockets;

            var count = Math.Min(maxSubscriptions, symbolCount - index * maxSubscriptions);
            var symbols = SymbolToProduct.Keys.ToList().Skip(index * maxSubscriptions).Take(count);
            var streams = symbols.Select(s => s.ToLower());
            return string.Format(url, string.Join(sep + "/", streams)) + sep;
        }

        private Dictionary<string, int> LastUpdateSeqNum = new Dictionary<string, int>();
        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            foreach (var symbol in GetWebsocketSubscribeSymbols(ws))
            {
                var product = SymbolToProduct[symbol];
                LastUpdateSeqNum[product] = 0;
            }
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws=null)
        {
            var parsedMessage = (Dictionary<string, object>)_jsonParser[_websockets.IndexOf(ws)].Parse(message);

            if (parsedMessage["data"] == null)
                return null;
            var parsedMessageData = (Dictionary<string, object>)parsedMessage["data"];

            if (!parsedMessageData["e"].ToString().Equals("depthUpdate"))
                return null;

            var product = SymbolToProduct[parsedMessageData["s"].ToString()];
            var firstUpdateSeqNum = int.Parse(parsedMessageData["U"].ToString());
            var lastUpdateSeqNum = int.Parse(parsedMessageData["u"].ToString());
            if (LastUpdateSeqNum[product] != 0 && LastUpdateSeqNum[product] != firstUpdateSeqNum - 1)
                ConsoleWriter.WriteLine(string.Format("Binance: updates for {0} were lost (from {1} to {2})",
                    product, LastUpdateSeqNum[product], firstUpdateSeqNum));

            LastUpdateSeqNum[product] = lastUpdateSeqNum;

            var latency = measurement.ArrivalTime - UnixStartTime.AddMilliseconds(long.Parse(parsedMessageData["E"].ToString()));
            OrdersUpdate ordersUpdate = new OrdersUpdate(false, measurement, latency, isDiffUpdate: false);
            var bids = (List<object>)parsedMessageData["b"];
            if (bids.Count > 0)
                ordersUpdate.Add(new EdgeKey(Name, GetBase(product), GetQuote(product), true), new BinanceOrderbookReader(bids));
            var asks = (List<object>)parsedMessageData["a"];
            if (asks.Count > 0)
                ordersUpdate.Add(new EdgeKey(Name, GetBase(product), GetQuote(product), false), new BinanceOrderbookReader(asks));

            return ordersUpdate;
        }

        protected override Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args)
        {
            Dictionary<string, object> requestParams = new Dictionary<string, object>();
            switch (requestType)
            {
                case UrlType.DepthUrl:
                    requestParams["url"] = string.Format(Urls[UrlType.DepthUrl], ProductToSymbol[args["product"]], args["size"]);
                    break;
                case UrlType.ProductsUrl:
                case UrlType.PingUrl:
                case UrlType.TickerUrl:
                        break;
                case UrlType.PrivateUrl:
                    DateTimeOffset timestamp = NtpAgent.GetAccurateCurrentUnixTime() + TimeSpan.FromSeconds(-1.0);
                    var query = new Dictionary<string, string>
                    {
                            { "timestamp", timestamp.ToUnixTimeMilliseconds().ToString()}
                    };

                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        requestParams["command"] = "order";
                        requestParams["method"] = "POST";
                        query["symbol"] = ProductToSymbol[args["product"]];
                        query["side"] = args["command"].Equals("bid") ? "BUY" : "SELL";
                        query["newOrderRespType"] = "FULL";
                        if (args.TryGetValue("type", out string orderType) && orderType.Equals("limit"))
                        {
                            query["type"] = "LIMIT";
                            query["price"] = args["price"];
                            query["timeInForce"] = "GTC";
                        }
                        else
                            query["type"] = "MARKET";
                        query["quantity"] = args["size"];
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        requestParams["command"] = "account";
                        requestParams["method"] = "GET";
                    }
                    else if (args["command"].Equals("cancel"))
                    {
                        requestParams["command"] = "order";
                        requestParams["method"] = "DELETE";
                        if (args.TryGetValue("product", out string product))
                        {
                            query["symbol"] = ProductToSymbol[args["product"]];
                        }
                        if (args.TryGetValue("orderId", out string orderId))
                        {
                            query["orderId"] = orderId;
                        }
                    }

                    string signedData = UrlEncode(query);
                    using (var hmac = new HMACSHA256(Encoding.UTF8.GetBytes(ApiSecret)))
                    {
                        var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(signedData));
                        var signature = ByteToString(hash);
                        requestParams["query"] = signedData + "&" + 
                            UrlEncode(new Dictionary<string, string> { { "signature", signature } });
                    }
                    
                    requestParams["headers"] = new Dictionary<string, string> {
                        { "X-MBX-APIKEY", ApiKey },
                        { "Content-Type", "application/x-www-form-urlencoded" }
                    };
                    break;
                default:
                    throw new NotImplementedException();
            }
            return requestParams;
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, 
            Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            switch (requestType)
            {
                case UrlType.DepthUrl:
                    result = new Dictionary<string, object>();
                    List<string> sides;
                    if (args.TryGetValue("side", out var s))
                        sides = new List<string>() { s };
                    else
                        sides = new List<string> { "bids", "asks" };

                    foreach (var side in sides)
                    {
                        var orderbook = new SortedDictionary<double, double>(side.Equals("bids") ?
                            Comparer<double>.Create((x, y) => -Comparer<double>.Default.Compare(x, y)) : Comparer<double>.Default);
                        foreach (var candle in (JArray)response[side])
                            orderbook[candle[0].ToObject<double>()] = candle[1].ToObject<double>();
                        result[side] = orderbook;
                    }
                    break;
                case UrlType.PingUrl:
                    break;
                case UrlType.ProductsUrl:
                    var allAssets = JArray.Parse(Utils.HttpGet("https://www.binance.com/assetWithdraw/getAllAsset.html"));
                    var badCurrencies = new List<string>();
                    foreach (var asset in allAssets)
                    {
                        if (!asset["enableCharge"].ToObject<bool>() || !asset["enableWithdraw"].ToObject<bool>())
                        {
                            var assetName = StandartizeCurrency(asset["assetCode"].ToString());
                            badCurrencies.Add(assetName);
                            ConsoleWriter.WriteLine("Binance: removing {0} (non withdrawable/depositable)", assetName);
                        }
                    }

                    foreach (var item in (JArray)response["symbols"])
                    {
                        var baseCurrency = StandartizeCurrency(item["baseAsset"].ToString());
                        if (baseCurrency.Equals("123") || badCurrencies.Contains(baseCurrency))
                            continue;
                        var quoteCurrency = StandartizeCurrency(item["quoteAsset"].ToString());
                        if (badCurrencies.Contains(quoteCurrency))
                            continue;

                        var product = baseCurrency + "-" + quoteCurrency;
                        double BaseMinSize = 0.0, BaseStep = 0.0, PriceMin = 0.0, PriceStep = 0.0;
                        int QuotePrecision = item["quotePrecision"].ToObject<int>(); 
                        int BasePrecision = item["baseAssetPrecision"].ToObject<int>();
                        foreach (var filter in (JArray)item["filters"])
                        {
                            if (filter["filterType"].ToString().Equals("LOT_SIZE"))
                            {
                                BaseMinSize = filter["minQty"].ToObject<double>();
                                BaseStep = filter["stepSize"].ToObject<double>();
                            }
                            if (filter["filterType"].ToString().Equals("PRICE_FILTER"))
                            {
                                PriceMin = filter["minPrice"].ToObject<double>();
                                PriceStep = filter["tickSize"].ToObject<double>();
                            }
                        }
                        ProductInfo[product] = new Product(
                            Symbol: item["symbol"].ToString(),
                            BaseStep: BaseStep,
                            BaseMinSize: BaseMinSize,
                            QuotePrecision: QuotePrecision,
                            PriceMin: PriceMin,
                            PriceStep: PriceStep,
                            BasePrecision: BasePrecision,
                            TakerFee: 0.1 / 100,
                            MakerFee: 0.1 / 100,
                            isFeeAlwaysFromQuote: false);
                    }
                    break;
                case UrlType.TickerUrl:
                    result = new Dictionary<string, object>();
                    foreach (var item in (JArray)response)
                    {
                        var symbol = item["symbol"].ToString();
                        if (!SymbolToProduct.ContainsKey(symbol))
                            continue;
                        result[SymbolToProduct[symbol]] = item["lastPrice"].ToObject<double>();
                    }
                    break;
                case UrlType.PrivateUrl:
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        if (response["code"] != null)
                            throw new APIException(response["msg"].ToString());
                        var isBidCommand = args["command"].Equals("bid");
                        var baseCurrency = GetBase(args["product"]);
                        var quoteCurrency = GetQuote(args["product"]);
                        double dstSize = 0.0, srcSize = 0.0;
                        double totalSize = 0.0, averagePrice = 0.0;
                        string status = response["status"].ToString();
                        if (status.Equals("FILLED"))
                        {
                            foreach (var fill in (JArray)response["fills"])
                            {
                                var feeCurrency = StandartizeCurrency(fill["commissionAsset"].ToString());
                                if (!feeCurrency.Equals(isBidCommand ? baseCurrency : quoteCurrency))
                                    throw new APIException("feeCurrency != DstCurrency");
                                var fee = fill["commission"].ToObject<double>();
                                var price = fill["price"].ToObject<double>();
                                var quantity = fill["qty"].ToObject<double>();
                                averagePrice = (averagePrice * totalSize + price * quantity) / (totalSize + quantity);
                                totalSize += quantity;
                                if (isBidCommand)
                                {
                                    dstSize += quantity - fee;
                                    srcSize += (quantity * price);
                                }
                                else
                                {
                                    dstSize += (quantity * price) - fee;
                                    srcSize += quantity;
                                }
                            }
                        }
                        double rate = dstSize < Config.NumericEpsilon ? 0.0 : dstSize / srcSize;
                        result = new Dictionary<string, object> {
                            { "dstSize", dstSize }, { "srcSize", srcSize }, { "rate", rate},
                            { "price", averagePrice }, { "size", totalSize }, { "orderId", response["orderId"].ToObject<long>() },
                            { "success", dstSize > Config.NumericEpsilon || status.Equals("NEW") } };
                        break;
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        result = new Dictionary<string, object>();
                        foreach (var item in (JArray)response["balances"])
                            result[StandartizeCurrency(item["asset"].ToString())] =
                                item["free"].ToObject<double>();
                    }
                    else if (args["command"].Equals("cancel"))
                    {
                        result = new Dictionary<string, object> { {"orderId", response["orderId"].ToObject<long>()} };
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            var info = ProductInfo[product];

            (double SrcSize, double rate) = (item.SrcSize, item.Rate);
            var price = isBidCommand ? 1 / rate : rate;
            var executionPrice = price.ToString(string.Format("F{0}", info.PricePrecision));

            string executionSize;
            double DstSize;
            if (!isBidCommand)
            {
                SrcSize = Math.Floor(SrcSize / info.BaseStep) * info.BaseStep;
                if (info.BaseMinSize - SrcSize > Config.NumericEpsilon)
                    return new RateItem(0.0, 0.0);
                DstSize = SrcSize * rate;
                executionSize = SrcSize.ToString(string.Format("F{0}", info.BasePrecision));
            }
            else
            {
                DstSize = Math.Floor(SrcSize * rate / info.BaseStep) * info.BaseStep;
                if (info.BaseMinSize - DstSize > Config.NumericEpsilon)
                    return new RateItem(0.0, 0.0);
                SrcSize = DstSize / rate;
                executionSize = DstSize.ToString(string.Format("F{0}", info.BasePrecision));
            }

            var fee = DstSize * info.TakerFee;
            var precisionFactor = Math.Pow(10, isBidCommand ? info.BasePrecision : info.QuotePrecision);
            fee = Math.Round(fee * precisionFactor) / precisionFactor;
            DstSize -= fee;

            return new RateItem(SrcSize, DstSize, executionSize, executionPrice);
        }
    }

    class BinanceOrderbookReader : IDictionary<double, double>
    {
        List<object> TicksData;

        public BinanceOrderbookReader(List<object> ticksData)
        {
            TicksData = ticksData;
        }

        public int Count => TicksData.Count;

        public IEnumerator<KeyValuePair<double, double>> GetEnumerator()
        {
            foreach (var item in TicksData)
            {
                var l = (List<object>)item;
                yield return new KeyValuePair<double, double>(
                    double.Parse((string)l[0]),
                    double.Parse((string)l[1]));
            }
        }

        public double this[double key] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ICollection<double> Keys => throw new NotImplementedException();

        public ICollection<double> Values => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(double key, double value)
        {
            throw new NotImplementedException();
        }

        public void Add(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(double key)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<double, double>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(double key)
        {
            throw new NotImplementedException();
        }

        public bool Remove(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(double key, out double value)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
