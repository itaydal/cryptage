﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Common;
using Common.Types;
using Newtonsoft.Json.Linq;
namespace ExchangeAPI.Exchanges
{
    public class ExchangeBitbay : Exchange
    {
        private enum StompCommand { STOMP, SUBSCRIBE, CONNECTED, MESSAGE };
        private Dictionary<StompCommand, string> SerializeStompCommand = new Dictionary<StompCommand, string>
        {
            { StompCommand.STOMP, "STOMP" }, { StompCommand.SUBSCRIBE, "SUBSCRIBE" },
            { StompCommand.CONNECTED, "CONNECTED" }, { StompCommand.MESSAGE, "MESSAGE" },
        };
        private Dictionary<string, StompCommand> DeserializeStompCommand;

        public ExchangeBitbay(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
            DeserializeStompCommand = SerializeStompCommand.ToList().ToDictionary(obj => obj.Value, obj => obj.Key);
        }

        public override ExchangeName Name => ExchangeName.BitBay;
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string>
        {
            { UrlType.TickerUrl, "https://api.bitbay.net/rest/trading/stats" },
            { UrlType.ProductsUrl, "https://api.bitbay.net/rest/trading/ticker" },
            //{ UrlType.PrivateUrl, "https://bitbay.net/API/Trading/tradingApi.php"},
            { UrlType.PrivateUrl, "https://api.bitbay.net/rest/{0}" },
            { UrlType.WebsocketUrl, "wss://api.bitbay.net/bitbay-websocket/websocket" }
        };

        protected override Dictionary<string, string> NonStandardCurrency { get; set; } = new Dictionary<string, string>
        {
            { "BCC", "BCH" }
        };

        public override (int count, TimeSpan period) RequestLimit { get; set; } = (50, new TimeSpan(0, 1, 0));

        private string CreateStompFrame(StompCommand command, Dictionary<string, string> headers, string payload = "")
        {
            var stompHeaders = SerializeStompCommand[command] + "\n";
            foreach (var header in headers)
                stompHeaders += header.Key + ":" + header.Value + "\n";
            
            return stompHeaders + "\n" + payload + "\x00";
        }

        private (StompCommand command, Dictionary<string, string> headers, string payload) ParseStompFrame(string stompFrame)
        {
            string[] payloadSep = { "\n\n" };
            var HeadersAndPayload = stompFrame.Replace("\x00", "").Split(payloadSep, StringSplitOptions.None);
            var lines = HeadersAndPayload[0].Split('\n');
            var command = DeserializeStompCommand[lines[0]];

            var payload = HeadersAndPayload[1];
            var headers = new Dictionary<string, string>();
            foreach (var line in lines.Skip(1))
            {
                var kv = line.Split(':');
                headers[kv[0]] = kv[1];
            }
            return (command , headers, payload);
        }

        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            ws.SendMessage(CreateStompFrame(StompCommand.STOMP, new Dictionary<string, string> { { "accept-version", "1.1" } }));
            var headers = new Dictionary<string, string> { { "ack", "auto" } };
            foreach (var symbol in SymbolToProduct.Values)
            {
                // get snapshot
                /*
                headers["destination"] = string.Format("/rest/trading/orderbook/{0}", symbol);
                headers["id"] = Guid.NewGuid().ToString();
                ws.SendMessage(CreateStompFrame(StompCommand.SUBSCRIBE, headers));
                */

                // subscribe to updates
                headers["destination"] = string.Format("/ws/topic/trading/orderbook/{0}", symbol);
                headers["id"] = symbol;
                ws.SendMessage(CreateStompFrame(StompCommand.SUBSCRIBE, headers));
            }
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws = null)
        {
            OrdersUpdate ordersUpdate = null;
            (var command, var args, var payload) = ParseStompFrame(message);
            switch (command)
            {
                case StompCommand.MESSAGE:
                    var product = args["subscription"];
                    var token = JObject.Parse(payload);
                    var timestamp = token["timestamp"].ToObject<long>();
                    var latency = measurement.ArrivalTime - UnixStartTime.AddMilliseconds(timestamp);
                    ordersUpdate = new OrdersUpdate(overrideOrderbooks: false, measurement: measurement, latency: latency);
                    (var bidUpdates, var askUpdates) = (new Dictionary<double, double>(), new Dictionary<double, double>());
                    foreach (var item in (JArray)token["changes"])
                    {
                        var price = item["rate"].ToObject<double>();
                        var action = item["action"].ToString();
                        var isBidBook = item["entryType"].ToString().Equals("Buy");
                        double size = 0.0;
                        if (action.Equals("update"))
                            size = item["state"]["ca"].ToObject<double>();
                        if (isBidBook)
                            bidUpdates[price] = size;
                        else
                            askUpdates[price] = size;
                    }
                    if (bidUpdates.Count > 0)
                        ordersUpdate.Add(new EdgeKey(Name, GetBase(product), GetQuote(product), true), bidUpdates);
                    if (askUpdates.Count > 0)
                        ordersUpdate.Add(new EdgeKey(Name, GetBase(product), GetQuote(product), false), askUpdates);
                    break;
                case StompCommand.CONNECTED:
                    break;
                default:
                    break;
            }
            return ordersUpdate;
        }

        protected override Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args)
        {
            Dictionary<string, object> requestParams = new Dictionary<string, object>();
            switch (requestType)
            {
                case UrlType.TickerUrl:
                case UrlType.ProductsUrl:
                    break;
                case UrlType.PrivateUrl:
                    var content = new Dictionary<string, string>();
                    DateTimeOffset timestamp = NtpAgent.GetAccurateCurrentUnixTime();
                    content["moment"] = timestamp.ToUnixTimeSeconds().ToString();
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        requestParams["command"] = "trading/offer/" + args["product"];
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        requestParams["command"] = "balances/BITBAY/balance";
                    }
                    else
                        throw new APIException(args["command"]);

                    requestParams["content"] = content.ToString();
                    requestParams["content_type"] = "application/json";
                    requestParams["method"] = "POST";
                    using (var hmac = new HMACSHA512(Encoding.UTF8.GetBytes(ApiSecret)))
                    {
                        requestParams["headers"] = new Dictionary<string, string> {
                            { "API-Hash",
                                ByteToString(hmac.ComputeHash(Encoding.UTF8.GetBytes((string)requestParams["content"])), false) },
                            { "API-Key", ApiKey },
                            { "operation-id", Guid.NewGuid().ToString() },
                            { "Request-Timestamp", timestamp.ToUnixTimeSeconds().ToString() }
                        };
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            return requestParams;
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, 
            Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            switch (requestType)
            {
                case UrlType.TickerUrl:
                    result = new Dictionary<string, object>();
                    foreach (var item in ((JObject)response["items"]).Values())
                    {
                        var symbol = item["m"].ToString();
                        if (!SymbolToProduct.ContainsKey(symbol))
                            continue;

                        result[SymbolToProduct[symbol]] = (item["l"].ToObject<double>() + item["h"].ToObject<double>()) / 2;
                    }
                    break;
                case UrlType.ProductsUrl:
                    result = new Dictionary<string, object>();
                    (double TakerFee, double MakerFee) fees;
                    var fiats = MarketCapProviders.MarketCapAPI.GetFiatMarketCup().Keys;
                    foreach (var ii in ((JObject)response["items"]).Values())
                    {
                        var item = ii["market"];
                        var symbol = item["code"].ToString();
                        var product = StandartizeCurrency(GetBase(symbol)) + "-" + StandartizeCurrency(GetQuote(symbol));
                        if (fiats.Contains(GetQuote(product)))
                            fees = (0.1 / 100, 0.1 / 100);
                        else
                            fees = (0.43 / 100, 0.30 / 100);

                        var baseMinSize = item["first"]["minOffer"].ToObject<double>();
                        var basePrecision = item["first"]["scale"].ToObject<int>();
                        var quoteMinSize = item["second"]["minOffer"].ToObject<double>();
                        var quotePrecision = item["second"]["scale"].ToObject<int>();
                        ProductInfo[product] = new Product(symbol, TakerFee: fees.TakerFee, MakerFee: fees.MakerFee,
                            isFeeAlwaysFromQuote: false, BasePrecision: basePrecision, BaseMinSize: baseMinSize,
                            QuotePrecision: quotePrecision, QuoteMinSize: quoteMinSize);
                    }
                    break;
                case UrlType.PrivateUrl:
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        throw new NotImplementedException();
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        result = new Dictionary<string, object>();
                        foreach (var item in (JObject)response["balances"])
                            result[StandartizeCurrency(item.Key)] =
                                item.Value["available"].ToObject<double>();
                    }
                    else
                        throw new NotImplementedException(args["command"]);
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            throw new NotImplementedException();
        }
    }
}
