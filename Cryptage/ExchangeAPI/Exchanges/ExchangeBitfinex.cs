﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Common.Types;

namespace ExchangeAPI.Exchanges
{
    public class ExchangeBitfinex : Exchange
    {
        public override ExchangeName Name => ExchangeName.Bitfinex;
        private int WebsocketVersion = 2;
        private Dictionary<int, string> ChannelIdToProduct;
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string>
            {
                { UrlType.ProductsUrl, "https://api.bitfinex.com/v1/symbols_details" },
                { UrlType.WebsocketUrl, "wss://api.bitfinex.com/ws/2" },
            };

        public override (int count, TimeSpan period) RequestLimit { get; set; } = (count: 30, period: new TimeSpan(0, 1, 0));
   
        public ExchangeBitfinex(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
            ChannelIdToProduct = new Dictionary<int, string>();
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws=null)
        {
            JToken token = JToken.Parse(message);

            if (token.Type == JTokenType.Object)
            {
                if (token["event"].Equals(JValue.FromObject("info")))
                {
                    if (token["version"].ToObject<int>() != WebsocketVersion)
                        throw new NotImplementedException();
                }
                else if (token["event"].Equals(JValue.FromObject("subscribed")))
                {
                    ChannelIdToProduct[token["chanId"].ToObject<int>()] = SymbolToProduct[token["pair"].ToString()];
                }
            }
            else if (token.Type == JTokenType.Array)
            {
                int channelId = token[0].ToObject<int>();
                if (token[1].Type == JTokenType.Array)
                {
                    string product = ChannelIdToProduct[channelId];
                    bool overrideOrderbooks;
                    JArray tripletArray;
                    if (token[1][0].Type != JTokenType.Array)
                    {
                        overrideOrderbooks = false;
                        tripletArray = new JArray { token[1] };
                    }
                    else
                    {
                        overrideOrderbooks = true;
                        tripletArray = (JArray)token[1];
                    }
          
                    OrdersUpdate ordersUpdate = new OrdersUpdate(overrideOrderbooks, measurement, TimeSpan.FromSeconds(0.0));
                    Dictionary<double, double> bidsUpdate = new Dictionary<double, double>();
                    Dictionary<double, double> asksUpdate = new Dictionary<double, double>();
                    foreach (JArray triplet in tripletArray)
                    {
                        double price = triplet[0].ToObject<double>();
                        double count = triplet[1].ToObject<double>();
                        double amount = triplet[2].ToObject<double>();
                        double realAmount;
                        if (count > 0)
                            realAmount = Math.Abs(amount);
                        else
                            realAmount = 0.0;

                        if (amount > 0)
                            bidsUpdate[price] = realAmount;
                        else
                            asksUpdate[price] = realAmount;
                    }
                    if (bidsUpdate.Count > 0)
                        ordersUpdate.Add( new EdgeKey(this.Name, GetBase(product), GetQuote(product), true ), bidsUpdate);
                    if (asksUpdate.Count > 0)
                        ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), false), asksUpdate);
                    return ordersUpdate;
                }
            }
            return null;
        }

        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            JObject o = 
                new JObject { { "event", "subscribe" }, {"channel", "book"}, { "prec", "P0" }, { "freq", "F0" } };
            foreach (string symbol in SymbolToProduct.Keys)
            {
                o["symbol"] = "t" + symbol;
                ws.SendMessage(o.ToString(Formatting.None));
            }
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, 
            Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            if (requestType == UrlType.ProductsUrl)
            {
                foreach (JToken item in response.ToArray())
                {
                    string symbol = item["pair"].ToString().ToUpper();
                    string product = symbol.Substring(0, 3) + "-" + symbol.Substring(3, 3);

                    // reformat bad currency names to standard names
                    product =
                        product.Replace("DSH", "DASH").Replace("QSH", "QASH").Replace("QTM", "QTUM").Replace("DAT", "DATA");

                    /*
                    ProductInfo[product] =
                        new Product(Symbol: symbol, BaseMinSize: item["minimum_order_size"].ToObject<double>(),
                        QuotePrecision: item["price_precision"].ToObject<int>(),
                        TakerFee: 0.2 / 100, MakerFee: 0.1 / 100);
                        */
                }
            }
            return result;
        }

        protected override Dictionary<string, object> ProcessRequest(UrlType requestTime, Dictionary<string, string> args)
        {
            throw new NotImplementedException();
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            throw new NotImplementedException();
        }
    }
}
