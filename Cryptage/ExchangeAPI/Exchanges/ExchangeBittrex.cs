﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json.Linq;
using Microsoft.AspNet.SignalR.Client;
using Bittrex.Net.Sockets;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Threading;
using Microsoft.AspNet.SignalR.Client.Http;
using System.Collections.Concurrent;
using Common.Types;
using Common;

namespace ExchangeAPI.Exchanges
{
    public class ExchangeBittrex: Exchange
    {
        private const string HubName = "c2"; //"coreHub"; // "c2"

        private const string MarketEvent = "uE";
        private const string SummaryLiteEvent = "uL";
        private const string SummaryEvent = "uS";
        private const string OrderEvent = "uO";

        private HubConnection _hubConnection { get; set; }
        private IHubProxy _hubProxy { get; set; }
        private string InvokeParam { get; set; }
        //private ConcurrentQueue<Action> _websocketMessageQueue;
        //private Thread _websocketMessageThread;
    
        private Dictionary<string, List<JToken>> updatesBuffer = new Dictionary<string, List<JToken>>();

        protected override Dictionary<string, string> NonStandardCurrency { get; set; } = new Dictionary<string, string>
        {
            { "BCC", "BCH" }
        };

        public ExchangeBittrex(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
            if (!Config.WebsocketsEnabled)
                return;

            /*
            CheckIfInMessageLoop(() =>
            {
                updatesBuffer = new Dictionary<string, List<JToken>>();
                foreach (var product in ProductToSymbol.Keys)
                    updatesBuffer[product] = new List<JToken>();

                _websocketMessageQueue = new ConcurrentQueue<Action>();
                _hubConnection = new HubConnection(Urls[UrlType.SignalRUrl]);
                _hubProxy = _hubConnection.CreateHubProxy(HubName);

                _websocketMessageThread = new Thread(() =>
                {
                    _hubProxy.On<string>(MarketEvent, e =>
                    {
                        _websocketMessageQueue.Enqueue(() => { WebsocketOnMessage(Decode(e), NtpAgent.GetAccurateCurrentUnixTime(), null); });
                    });
                    _hubConnection.Start(new WebsocketCustomTransport(new CryptoExchange.Net.Logging.Log(),
                        new DefaultHttpClient())).Wait();
                    Task.Run(async () =>
                    {
                        var items = SymbolToProduct.ToList();
                        int index = 0;
                        while (true)
                        {
                            if (_websocketMessageQueue.TryDequeue(out Action message))
                                message();
                            else if (index < items.Count)
                            {
                                var symbol = items[index].Key;
                                var product = items[index++].Value;
                                ConsoleWriter.WriteLine(string.Format("{0}: SubscribeToExchangeDeltas to {1}", Name, product));
                                await _hubProxy.Invoke<bool>("SubscribeToExchangeDeltas", symbol);
                                ConsoleWriter.WriteLine(string.Format("{0}: QueryExchangeState of {1}", Name, product));
                                string response = await _hubProxy.Invoke<string>("QueryExchangeState", symbol);
                                InvokeParam = symbol;
                                WebsocketOnMessage(Decode(response), NtpAgent.GetAccurateCurrentUnixTime(), null);
                            }
                            else
                                Thread.Sleep(1);
                        }
                    }).Wait();
                });
                _websocketMessageThread.Start();
            }, _restThreadQueue);
            */
        }

        private Dictionary<double, double> ParseUpdate(JArray items)
        {
            Dictionary<double, double> update = new Dictionary<double, double>();
            foreach (var item in items)
            {
                //var type = item["TY"].ToObject<int>();
                var price = item["R"].ToObject<double>();
                var size = item["Q"].ToObject<double>();
                update[price] = size;
            }
            return update;
        }

        private void AddOrderUpdates(OrdersUpdate ordersUpdate, JToken token, string product)
        {
            string[] sides = { "Z", "S" };
            foreach (var side in sides)
            {
                Dictionary<double, double> update = ParseUpdate((JArray)token[side]);
                if (update.Count > 0)
                    ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), side.Equals("Z")), update);
            }
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws=null)
        {
            JToken token = JToken.Parse(message);

            string symbol = token["M"].ToString();
            bool isOverride = false;
            if (symbol.Equals(""))
            {
                symbol = InvokeParam;
                isOverride = true;
            }

            var product = SymbolToProduct[symbol];
            if (!isOverride && updatesBuffer.ContainsKey(product))
            {
                updatesBuffer[product].Add(token);
                return null;
            }

            OrdersUpdate ordersUpdate = new OrdersUpdate(isOverride, measurement, TimeSpan.FromSeconds(0.0), isDiffUpdate: false);
            if (isOverride)
            {
                var nounce = token["N"].ToObject<int>();
                updatesBuffer[product] = updatesBuffer[product].Where(item => item["N"].ToObject<int>() > nounce).ToList();
                Dictionary<string, Dictionary<double, double>> updates = new Dictionary<string, Dictionary<double, double>>
                {
                    {"Z", ParseUpdate((JArray)token["Z"]) },
                    {"S", ParseUpdate((JArray)token["S"]) }
                };
                string[] sides = { "Z", "S" };
                foreach (var item in updatesBuffer[product])
                {
                    foreach (var side in sides)
                    {
                        Dictionary<double, double> update = ParseUpdate((JArray)token[side]);
                        foreach (var u in update)
                        {
                            if (u.Value < Config.NumericEpsilon && updates[side].ContainsKey(u.Key))
                                updates[side].Remove(u.Key);
                            else
                                updates[side][u.Key] = u.Value;
                        }
                    }
                }
                foreach (var side in sides)
                {
                    if (updates[side].Count > 0)
                        ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), side.Equals("Z")), updates[side]);
                }
                updatesBuffer.Remove(product);
            }
            else
                AddOrderUpdates(ordersUpdate, token, product);
            return ordersUpdate;
        }

        // Decode converts Bittrex CoreHub2 socket wire protocol data into JSON.
        // Data goes from base64 encoded to gzip (byte[]) to minifed JSON.
        public static string Decode(string wireData)
        {
            // Step 1: Base64 decode the wire data into a gzip blob
            byte[] gzipData = Convert.FromBase64String(wireData);

            // Step 2: Decompress gzip blob into minified JSON
            using (var decompressedStream = new MemoryStream())
            using (var compressedStream = new MemoryStream(gzipData))
            using (var deflateStream = new DeflateStream(compressedStream, CompressionMode.Decompress))
            {
                deflateStream.CopyTo(decompressedStream);
                decompressedStream.Position = 0;

                using (var streamReader = new StreamReader(decompressedStream))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        public override ExchangeName Name => ExchangeName.Bittrex;
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string> {
            {UrlType.ProductsUrl, "https://bittrex.com/api/v1.1/public/getmarkets" },
            {UrlType.PrivateUrl, "https://bittrex.com/api/{0}" },
            {UrlType.SignalRUrl, "https://socket.bittrex.com/signalr" },
            //{UrlType.TickerUrl, "https://bittrex.com/api/v2.0/pub/Markets/GetMarketSummaries" }
        };
        public override (int count, TimeSpan period) RequestLimit { get; set; } = (count: 60, period: new TimeSpan(0, 1, 0));

        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            throw new NotImplementedException();
        }

        protected override Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args)
        {
            Dictionary<string, object> requestParams = new Dictionary<string, object>();
            switch (requestType)
            {
                case UrlType.ProductsUrl:
                    break;
                case UrlType.SignalRUrl:
                    requestParams["url"] = Urls[UrlType.SignalRUrl] + "negotiate?" + UrlEncode(args);
                    break;
                case UrlType.PrivateUrl:
                    var query = new Dictionary<string, string> { { "apikey", ApiKey },
                            { "nonce", DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString()}};
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        requestParams["command"] = "v2.0/key/market/" + (args["command"].Equals("bid") ? "TradeBuy": "TradeSell");
                        query.Add("marketname", ProductToSymbol[args["product"]]);
                        query.Add("ordertype", "MARKET");
                        query.Add("quantity", args["size"]);
                        query.Add("rate", "0.001");
                        query.Add("conditiontype", "NONE");
                        query.Add("target", "0");
                        query.Add("timeInEffect", "GOOD_TIL_CANCELLED");
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        requestParams["command"] = "v2.0/key/balance/getbalances";
                    }
                    else
                        throw new NotImplementedException(args["command"]);

                    requestParams["url"] = 
                        string.Format(Urls[UrlType.PrivateUrl], requestParams["command"]) + "?" + UrlEncode(query);
                    byte[] uriBytes = Encoding.UTF8.GetBytes((string)requestParams["url"]);
                    using (var hmac = new HMACSHA512(Encoding.UTF8.GetBytes(ApiSecret)))
                    {
                        requestParams["headers"] = new Dictionary<string, string> { { "apisign",
                                ByteToString(hmac.ComputeHash(uriBytes)) } };
                    }
                    
                    break;
                default:
                    throw new NotImplementedException();
            }
            return requestParams;
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            switch (requestType)
            {
                case UrlType.ProductsUrl:
                    if (response["success"].ToObject<bool>() == true)
                    {
                        foreach (var item in (JArray)response["result"])
                        {
                            if (item["IsActive"].ToObject<bool>() == false)
                                continue;

                            string symbol = item["MarketName"].ToString();
                            string[] currencies = symbol.Split('-');
                            string product = string.Format("{0}-{1}", StandartizeCurrency(currencies[1]),
                                StandartizeCurrency(currencies[0]));
                            ProductInfo[product] = new Product(
                                Symbol: symbol,
                                isFeeAlwaysFromQuote: false, // TODO: check if this is correct
                                BaseMinSize: item["MinTradeSize"].ToObject<double>(),
                                BaseStep: item["MinTradeSize"].ToObject<double>(),
                                TakerFee: 0.25 / 100,
                                MakerFee: 0.25 / 100);
                        }
                    }
                    else
                        throw new APIException(response.ToString());
                    break;
                case UrlType.SignalRUrl:
                    /*
                    var urlParams = new Dictionary<string, string>
                    {
                        ["ConnectionId"] = response["ConnectionId"].ToString(),
                        ["ConnectionToken"] = response["ConnectionToken"].ToString(),
                        ["ClientProtocol"] = response["ProtocolVersion"].ToString()
                    };
                    foreach (var item in args)
                    {
                        urlParams[item.Key] = item.Value;
                    }
                    result = new Dictionary<string, object>
                    {
                        ["WebsocketUrl"] = Urls[UrlType.SignalRUrl].Replace("https", "wss") + "connect?" + UrlEncode(urlParams)
                    };
                    */
                    break;
                case UrlType.PrivateUrl:
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        if (response["success"].ToObject<bool>() == false)
                            ConsoleWriter.WriteLine(response["message"].ToString());
                        else
                        {
                        }
                        
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        result = new Dictionary<string, object>();
                        foreach (var item in (JArray)response["result"])
                            result[StandartizeCurrency(item["Balance"]["Currency"].ToString())] = 
                                item["Balance"]["Available"].ToObject<double>();
                    }
                    else
                        throw new APIException(args["command"]);
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            var executionSize = isBidCommand ? item.DstSize : item.SrcSize;
            return new RateItem(srcSize: item.SrcSize, dstSize: item.DstSize, executionSize: executionSize.ToString(),
                executionPrice: item.Rate.ToString());
        }
    }
}
