﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Common.Types;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Collections;
using Common;

namespace ExchangeAPI.Exchanges
{
    public class ExchangeCoinTiger : Exchange
    {
        public ExchangeCoinTiger(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
        }

        public override ExchangeName Name => ExchangeName.CoinTiger;
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string>
        {
            { UrlType.ProductsUrl, "https://api.cointiger.com/exchange/trading/api/v2/currencys" },
            { UrlType.PrivateUrl, "https://api.cointiger.com/exchange/trading/api/{0}"},
            { UrlType.TickerUrl, "https://api.cointiger.com/exchange/trading/api/market/detail?symbol=btcbitcny" },
            { UrlType.WebsocketUrl, "wss://api.cointiger.com/exchange-market/ws" },
            { UrlType.PingUrl, "https://api.cointiger.com/exchange/trading/api/v2/timestamp" }
        };
        protected override int ApiPingIntervalMsec { get; set; } = 100 * 1000;
        public override (int count, TimeSpan period) RequestLimit { get; set; } = (6, new TimeSpan(0, 0, 1));
        protected override CompressionMethod WebsocketCompression { get; set; } = CompressionMethod.Gzip;

        private int subscribeIndex;
        private List<KeyValuePair<string, string>> SymbolsList;
        void SubscribeToProduct(WebSocketWrapper ws)
        {
            var item = SymbolsList[subscribeIndex];
            ConsoleWriter.WriteLine(string.Format("{0}: Subscribing to {1} ({2} out of {3})",
                Name, item.Value, subscribeIndex + 1, SymbolsList.Count));
            ws.SendMessage(string.Format(
                "{{\"event\":\"sub\",\"params\":{{\"channel\":\"market_{0}_depth_step0\",\"cb_id\":{1},\"asks\":20,\"bids\":20}}}}",
                item.Key, subscribeIndex));
            subscribeIndex++;
        }
        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            subscribeIndex = 0;
            SymbolsList = SymbolToProduct.ToList();
            SubscribeToProduct(ws);
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws = null)
        {
            var parsedMessage = (Dictionary<string, object>)_jsonParser[_websockets.IndexOf(ws)].Parse(message);
            if (parsedMessage.ContainsKey("ping"))
            {
                ws.SendMessage(string.Format("{{\"pong\":{0}}}", long.Parse(parsedMessage["ping"].ToString())));
                return null;
            }
            
            if (parsedMessage.ContainsKey("event_rep"))
            {
                if (subscribeIndex < SymbolsList.Count)
                    SubscribeToProduct(ws);
                return null;
            }
  

            var symbol = parsedMessage["channel"].ToString().Split('_')[1];
            var product = SymbolToProduct[symbol];

            var latency = NtpAgent.GetAccurateCurrentUnixTime() - 
                UnixStartTime.AddMilliseconds(long.Parse(parsedMessage["ts"].ToString()));
            OrdersUpdate ordersUpdate = new OrdersUpdate(true, measurement, latency, isDiffUpdate: false);
            var parsedMessageData = (Dictionary<string, object>)parsedMessage["tick"];
            var bids = (List<object>)parsedMessageData["buys"];
            if (bids.Count > 0)
                ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), true), new CoinTigerOrderbookReader(bids));
            var asks = (List<object>)parsedMessageData["asks"];
            if (asks.Count > 0)
                ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), false), new CoinTigerOrderbookReader(asks));

            return ordersUpdate;
        }

        protected override Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args)
        {
            Dictionary<string, object> requestParams = new Dictionary<string, object>();
            switch (requestType)
            {
                case UrlType.ProductsUrl:
                case UrlType.TickerUrl:
                case UrlType.PingUrl:
                    break;
                case UrlType.PrivateUrl:
                    DateTimeOffset timestamp = NtpAgent.GetAccurateCurrentUnixTime();
                    var query = new Dictionary<string, string>
                    {
                        { "time", timestamp.ToUnixTimeMilliseconds().ToString() }
                    };
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        requestParams["method"] = "POST";
                        requestParams["command"] = "order";
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        requestParams["method"] = "GET";
                        requestParams["command"] = "user/balance";
                    }
                    else
                        throw new APIException(args["command"]);

                    string uncodedQuery = UrlEncode(query, sorted: true);
                    //string encodedSecret = Uri.EscapeDataString(ApiSecret);
                    using (var hmac = new HMACSHA512(Encoding.UTF8.GetBytes(ApiSecret)))
                    {
                        var signedData = uncodedQuery.Replace("&", "").Replace("=", "") + ApiSecret;
                        var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(signedData));
                        var signature = ByteToString(hash, uppercase: false);
                        requestParams["query"] =
                            UrlEncode(new Dictionary<string, string> { { "api_key", ApiKey } }) + "&" +
                            uncodedQuery + "&" +
                            UrlEncode(new Dictionary<string, string> { { "sign", signature } });
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            return requestParams;
        }

        private JObject GetAllTickers()
        {
            var webRequest = (HttpWebRequest)WebRequest.Create("https://www.cointiger.pro/exchange/api/public/market/detail");
            string response = new StreamReader(webRequest.GetResponse().GetResponseStream()).ReadToEnd();
            return JObject.Parse(response);
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, 
            Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            switch (requestType)
            {
                case UrlType.PingUrl:
                    break;
                case UrlType.TickerUrl:
                    result = new Dictionary<string, object>();
                    var allTickers = GetAllTickers();
                    foreach (var item in allTickers)
                    {
                        var symbol = item.Key.ToString().ToLower();
                        if (!SymbolToProduct.ContainsKey(symbol))
                            continue;
                        result[SymbolToProduct[symbol]] = item.Value["last"].ToObject<double>();
                    }
                    break;
                case UrlType.ProductsUrl:
                    string[] partitions = { "bitcny", "btc", "usdt", "eth" };
                    foreach (var partition in partitions)
                    {
                        foreach (var item in (JArray)response["data"][partition + "-partition"])
                        {
                            var baseCurrency = StandartizeCurrency(item["baseCurrency"].ToString().ToUpper());
                            var quoteCurrency = StandartizeCurrency(item["quoteCurrency"].ToString().ToUpper());
                            var product = baseCurrency + "-" + quoteCurrency;
                            var symbol = product.Replace("-", "").ToLower();
                            ProductInfo[product] = new Product(Symbol: symbol, 
                                TakerFee: 0.1 / 100, MakerFee: 0.1 / 100,
                                isFeeAlwaysFromQuote: false);
                        }
                    }
                    break;
                case UrlType.PrivateUrl:
                    result = new Dictionary<string, object>();
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        throw new NotImplementedException();
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        foreach (var item in (JArray)response["data"])
                            result[StandartizeCurrency(item["coin"].ToString().ToUpper())] = item["normal"].ToObject<double>();
                    }
                    else
                        throw new NotImplementedException(args["command"]);
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            throw new NotImplementedException();
        }
    }


    class CoinTigerOrderbookReader : IDictionary<double, double>
    {
        List<object> TicksData;

        public CoinTigerOrderbookReader(List<object> ticksData)
        {
            TicksData = ticksData;
        }

        public int Count => TicksData.Count;

        public IEnumerator<KeyValuePair<double, double>> GetEnumerator()
        {
            foreach (var item in TicksData)
            {
                var l = (List<object>)item;
                yield return new KeyValuePair<double, double>(double.Parse((string)l[0]), double.Parse((string)l[1]));
            }
        }

        public double this[double key] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ICollection<double> Keys => throw new NotImplementedException();

        public ICollection<double> Values => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(double key, double value)
        {
            throw new NotImplementedException();
        }

        public void Add(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(double key)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<double, double>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(double key)
        {
            throw new NotImplementedException();
        }

        public bool Remove(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(double key, out double value)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
