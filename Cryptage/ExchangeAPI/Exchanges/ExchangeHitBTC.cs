﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Common.Types;
using Common;
using System.Collections;
using SimpleJson;
using System.Threading;

namespace ExchangeAPI.Exchanges
{
    public class ExchangeHitBTC: Exchange
    {
        private static bool useWebsocketV2 = false;
        protected override int NumWebsockets { get; set; } = 10;
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string>
        {
            { UrlType.CurrenciesUrl, "https://api.hitbtc.com/api/2/public/currency" },
            { UrlType.ProductsUrl, "https://api.hitbtc.com/api/2/public/symbol" },
            { UrlType.TickerUrl, "https://api.hitbtc.com/api/2/public/ticker" },
            { UrlType.WebsocketUrl, useWebsocketV2 ? "wss://st.hitbtc.com/" : "wss://api.hitbtc.com/api/2/ws" },
            { UrlType.PrivateUrl, "https://api.hitbtc.com/api/2/{0}" },
            { UrlType.PingUrl, "https://api.hitbtc.com/api/2/public/currency/BTC" }
        };
        protected override int ApiPingIntervalMsec { get; set; } = 35 * 1000; // 06/10/18 - disconnects after 65 seconds
        public override (int count, TimeSpan period) RequestLimit { get; set; } = (count: 100, period: new TimeSpan(0, 0, 1));
       
        protected override Dictionary<string, string> NonStandardCurrency { get; set; } = new Dictionary<string, string>
        {
            { "BQX", "ETHOS" }, { "EMGO", "MGO" }, { "IOTA", "MIOTA" },
            { "USD", "USDT" }
        };

        public ExchangeHitBTC(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
        }

        public override ExchangeName Name => ExchangeName.HitBTC;

        private void SubscribeToSymbols(WebSocketWrapper ws)
        {
            string subscribeFormat;
            if (useWebsocketV2)
                subscribeFormat = "[1,\"orderbook\",\"{0}\"]";
            else
                subscribeFormat = "{{\"method\":\"subscribeOrderbook\",\"id\":\"{0}\",\"params\":{{\"symbol\":\"{0}\"}}}}";

            var symbol = GetWebsocketSubscribeSymbols(ws)[(int)Interlocked.Read(ref ws.SubscribeCounter)];
            LastUpdateSeqNum[SymbolToProduct[symbol]] = 0;
            ws.SendMessage(string.Format(subscribeFormat, symbol));
        }

        private Dictionary<string, int> LastUpdateSeqNum = new Dictionary<string, int>();
        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            SubscribeToSymbols(ws);
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws=null)
        {
            var wsIndex = _websockets.IndexOf(ws);
            var parser = _jsonParser[wsIndex];
            bool overrideOrderbook;
            Dictionary<string, object> parsedMessageParams;
            string product;

            if (useWebsocketV2)
            {
                var parsed = (List<object>)parser.Parse(message);
                overrideOrderbook = int.Parse(parsed[0].ToString()) == 0;
                product = SymbolToProduct[parsed[2].ToString()];
                parsedMessageParams = (Dictionary<string, object>)parsed[3];
                /*
                if (subscribeIndex[wsIndex] < SymbolToProduct.Count)
                    SubscribeToProduct(ws, wsIndex);
                */
            }
            else
            {
                var parsedMessage = (Dictionary<string, object>)parser.Parse(message);
                if (parsedMessage.ContainsKey("error"))
                    throw new NotImplementedException();
                if (parsedMessage.ContainsKey("result"))
                {
                    if (!bool.Parse(parsedMessage["result"].ToString()))
                        throw new APIException("HitBTC: subscription to product failed");

                    var subscribeIndex = Interlocked.Increment(ref ws.SubscribeCounter);
                    var numOfProducts = (int)Interlocked.Read(ref ws.NumSubscribeProducts);
                    ConsoleWriter.WriteLine("HitBTC: websocket {0} out of {1} subscribed to {2} ({3} out of {4})",
                        wsIndex + 1, NumWebsockets, SymbolToProduct[parsedMessage["id"].ToString()], subscribeIndex, numOfProducts);
                    
                    if (subscribeIndex < numOfProducts)
                        SubscribeToSymbols(ws);
                    
                    return null;
                }
                var method = parsedMessage["method"].ToString();
                if (method.Equals("snapshotOrderbook"))
                    overrideOrderbook = true;
                else if (method.Equals("updateOrderbook"))
                    overrideOrderbook = false;
                else
                    return null;

                parsedMessageParams = (Dictionary<string, object>)parsedMessage["params"];
                product = SymbolToProduct[parsedMessageParams["symbol"].ToString()];
                int seqNum = int.Parse(parsedMessageParams["sequence"].ToString());

                if (LastUpdateSeqNum[product] != 0 && LastUpdateSeqNum[product] != seqNum - 1)
                    ConsoleWriter.WriteLine(string.Format("HitBTC: updates for {0} were lost (from {1} to {2})",
                        product, LastUpdateSeqNum[product], seqNum));

                LastUpdateSeqNum[product] = seqNum;
            }

            OrdersUpdate ordersUpdate = new OrdersUpdate(overrideOrderbook, measurement, 
                TimeSpan.FromSeconds(0.0), isDiffUpdate: false);
            var bids = (List<object>)parsedMessageParams["bid"];
            if (bids.Count > 0)
                ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), true), 
                    new HitBTCOrderbookReader(bids, useWebsocketV2));
            var asks = (List<object>)parsedMessageParams["ask"];
            if (asks.Count > 0)
                ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), false), 
                    new HitBTCOrderbookReader(asks, useWebsocketV2));

            return ordersUpdate;
        }

        private Dictionary<string, string> orderContent = new Dictionary<string, string> {
            { "side", "" }, { "symbol", "" }, { "quantity", "" }, { "type", "" }
        };
        protected override Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args)
        {
            var requestParams = new Dictionary<string, object>();
            switch (requestType)
            {
                case UrlType.PingUrl:
                case UrlType.ProductsUrl:
                case UrlType.CurrenciesUrl:
                case UrlType.TickerUrl:
                    break;
                case UrlType.PrivateUrl:
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        requestParams["command"] = "order";
                        requestParams["method"] = "POST";

                        orderContent["side"] = args["command"].Equals("bid") ? "buy" : "sell";
                        orderContent["symbol"] = ProductToSymbol[args["product"]];
                        orderContent["quantity"] = args["size"];
                        if (args.TryGetValue("type", out string orderType) && orderType.Equals("limit"))
                        {
                            orderContent["type"] = "limit";
                            orderContent["price"] = args["price"];
                        }
                        else
                            orderContent["type"] = "market";
                        requestParams["content"] = UrlEncode(orderContent);
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        requestParams["command"] = "trading/balance";
                        requestParams["method"] = "GET";
                    }
                    else if (args["command"].Equals("cancel"))
                    {
                        requestParams["command"] = "order";
                        requestParams["method"] = "DELETE";
                    }

                    string auth = "Basic " + ToBase64(ApiKey + ":" + ApiSecret);
                    requestParams["headers"] = new Dictionary<string, string> { { "Authorization", auth } };
                    break;
                default:
                    throw new NotImplementedException();
            }
            return requestParams;
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, 
            Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            switch (requestType)
            {
                case UrlType.PingUrl:
                    break;
                case UrlType.CurrenciesUrl:
                    result = new Dictionary<string, object>();
                    var badCurrencies = new List<string>();
                    foreach (var info in (JArray)response)
                    {
                        if (!info["payinEnabled"].ToObject<bool>() || !info["payoutEnabled"].ToObject<bool>()
                            || !info["transferEnabled"].ToObject<bool>() || info["delisted"].ToObject<bool>())
                            badCurrencies.Add(StandartizeCurrency(info["id"].ToString()));
                    }
                    result["bad"] = badCurrencies;
                    break;
                case UrlType.ProductsUrl:
                    foreach (var item in (JArray)response)
                    {
                        string baseCurrency = StandartizeCurrency(item["baseCurrency"].ToString());
                        string quoteCurrency = StandartizeCurrency(item["quoteCurrency"].ToString());
                        string product = baseCurrency + "-" + quoteCurrency;
                        int quotePrecision = quoteCurrency.Equals("BTC") ? 9 : 8;
                        ProductInfo[product] = new Product(
                            Symbol: item["id"].ToString(),
                            isFeeAlwaysFromQuote: true,
                            BaseStep: item["quantityIncrement"].ToObject<double>(),
                            PriceStep: item["tickSize"].ToObject<double>(),
                            QuotePrecision: quotePrecision,
                            TakerFee: item["takeLiquidityRate"].ToObject<double>(),
                            MakerFee: item["provideLiquidityRate"].ToObject<double>());
                    }
                    break;
                case UrlType.TickerUrl:
                    result = new Dictionary<string, object>();
                    foreach (var item in (JArray)response)
                    {
                        var symbol = item["symbol"].ToString();
                        if (!SymbolToProduct.ContainsKey(symbol))
                            continue;

                        var price = item["last"].Type != JTokenType.Null ? item["last"] : item["bid"];
                        result[SymbolToProduct[symbol]] = price.ToObject<double>();
                    }
                    break;
                case UrlType.PrivateUrl:
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        if (response["error"] != null)
                            throw new APIException(response["error"]["description"].ToString());

                        double dstSize = 0.0, srcSize = 0.0;
                        double totalSize = 0.0, averagePrice = 0.0;
                        if (response["status"].ToString().Equals("filled"))
                        {
                            foreach (var trade in (JArray)response["tradesReport"])
                            {
                                double quantity = trade["quantity"].ToObject<double>();
                                double price = trade["price"].ToObject<double>();
                                averagePrice = (averagePrice * totalSize + price * quantity) / (totalSize + quantity);
                                totalSize += quantity;
                                double fee = trade["fee"].ToObject<double>();
                                if (args["command"].Equals("bid"))
                                {
                                    dstSize += quantity;
                                    srcSize += (quantity * price) + fee;
                                }
                                else
                                {
                                    dstSize += (quantity * price) - fee;
                                    srcSize += quantity;
                                }
                            }
                        }
                        double rate = dstSize < Config.NumericEpsilon ? 0.0 : dstSize / srcSize;
                        result = new Dictionary<string, object> {
                            { "dstSize", dstSize }, { "srcSize", srcSize }, { "rate", rate},
                            { "price", averagePrice }, { "size", totalSize }, { "orderId", response["id"].ToObject<long>() },
                            { "success", dstSize > Config.NumericEpsilon || response["type"].ToString().Equals("limit") } };
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        result = new Dictionary<string, object>();
                        foreach (var item in (JArray)response)
                            result[StandartizeCurrency(item["currency"].ToString())] = item["available"].ToObject<double>();
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            var info = ProductInfo[product];

            (double SrcSize, double rate) = (item.SrcSize, item.Rate);
            var price = isBidCommand ? 1 / rate : rate;
            var executionPrice = price.ToString(string.Format("F{0}", info.PricePrecision));

            string executionSize;
            double DstSize;
            double TruncateQuote(double q)
            {
                var factor = Math.Pow(10, info.QuotePrecision);
                return Math.Max(Math.Round(q * factor) / factor, Math.Pow(10, -1 * info.QuotePrecision));
            }

            double TruncateFee(double q)
            {
                var factor = Math.Pow(10, info.QuotePrecision);
                return Math.Max(Math.Ceiling(q * factor) / factor, Math.Pow(10, -1 * info.QuotePrecision));
            }

            if (!isBidCommand)
            {
                SrcSize = Math.Floor(SrcSize / info.BaseStep) * info.BaseStep;
                if (info.BaseMinSize - SrcSize > Config.NumericEpsilon)
                    return new RateItem(0.0, 0.0);
                DstSize = SrcSize * rate;
                var fee = TruncateFee(DstSize * info.TakerFee);
                DstSize = TruncateQuote(DstSize - fee);
                executionSize = SrcSize.ToString(string.Format("F{0}", info.BasePrecision));
            }
            else
            {
                var minFee = TruncateFee(0.0);
                if (SrcSize < minFee)
                    return new RateItem(0.0, 0.0);
                
                DstSize = Math.Floor(SrcSize * rate / (1 + info.TakerFee) / info.BaseStep) * info.BaseStep;
                var fee = TruncateFee(DstSize / rate * info.TakerFee);
                var NewSrcSize = TruncateQuote(DstSize / rate + fee);
                while (NewSrcSize - SrcSize > Config.NumericEpsilon)
                {
                    DstSize -= info.BaseStep;
                    fee = TruncateFee(DstSize / rate * info.TakerFee);
                    NewSrcSize = TruncateQuote(DstSize / rate + fee);
                }
                if (info.BaseMinSize - DstSize > Config.NumericEpsilon)
                    return new RateItem(0.0, 0.0);
                SrcSize = NewSrcSize;
                executionSize = DstSize.ToString(string.Format("F{0}", info.BasePrecision));
            }

            return new RateItem(SrcSize, DstSize, executionSize, executionPrice);
        }
    }

    class HitBTCOrderbookReader : IDictionary<double, double>
    {
        List<object> TicksData;
        bool useWebsocketV2;

        public HitBTCOrderbookReader(List<object> ticksData, bool useWebsocketV2 = false)
        {
            TicksData = ticksData;
            this.useWebsocketV2 = useWebsocketV2;
        }

        public int Count => TicksData.Count;

        public IEnumerator<KeyValuePair<double, double>> GetEnumerator()
        {
            foreach (var item in TicksData)
            {
                if (useWebsocketV2)
                {
                    var l = (List<object>)item;
                    yield return new KeyValuePair<double, double>(
                        double.Parse((string)l[0]),
                        double.Parse((string)l[1]));
                }
                else
                {
                    var l = (Dictionary<string, object>)item;
                    yield return new KeyValuePair<double, double>(
                        double.Parse((string)l["price"]),
                        double.Parse((string)l["size"]));
                }
            }
        }

        public double this[double key] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ICollection<double> Keys => throw new NotImplementedException();

        public ICollection<double> Values => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(double key, double value)
        {
            throw new NotImplementedException();
        }

        public void Add(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(double key)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<double, double>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(double key)
        {
            throw new NotImplementedException();
        }

        public bool Remove(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(double key, out double value)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
