﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Common.Types;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ExchangeAPI.Exchanges
{
    public class ExchangeHuobi : Exchange
    {
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string>
        {
            { UrlType.ProductsUrl, "https://api.huobi.pro/v1/common/symbols" },
            { UrlType.TickerUrl, "https://api.huobi.pro/market/tickers" },
            { UrlType.CurrenciesUrl, "https://api.huobi.pro/v2/beta/common/currencies" },
            { UrlType.WebsocketUrl, "wss://api.huobi.pro/ws" },
            { UrlType.PrivateUrl, "https://api.huobi.pro/v1/{0}" },
            { UrlType.PingUrl, "https://api.huobi.pro/v1/common/timestamp" },
            { UrlType.DepthUrl, "https://api.huobi.pro/market/depth?symbol={0}&type=step0&pick=[\"bids.{1}\",\"asks.{1}\"]" }
        };

        protected override int ApiPingIntervalMsec { get; set; } = 60 * 1000; // disconnects after 400 seconds
        protected override Dictionary<string, string> NonStandardCurrency { get; set; } = new Dictionary<string, string>
        {
            { "IOTA", "MIOTA" }
        };

        public override (int count, TimeSpan period) RequestLimit { get; set; } = (count: 100, period: new TimeSpan(0, 0, 10));
        public int AccountId = 0;
        public ExchangeHuobi(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
        }

        public override ExchangeName Name => ExchangeName.Huobi;
        protected override int NumWebsockets { get; set; } = 10;
        protected override CompressionMethod WebsocketCompression { get; set; } = CompressionMethod.Gzip;
        void SubscribeToSymbols(WebSocketWrapper ws)
        {
            var messageFormat = "{{\"id\":\"{0}\",\"sub\":\"market.{0}.depth.step0\",\"pick\":[\"bids.5\",\"asks.5\"]}}";
            foreach (var symbol in GetWebsocketSubscribeSymbols(ws))
            {
                ws.SendMessage(string.Format(messageFormat, symbol));
            }
        }

        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            SubscribeToSymbols(ws);
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws = null)
        {
            var index = _websockets.IndexOf(ws);
            var parsedMessage = (Dictionary<string, object>)_jsonParser[index].Parse(message);

            if (parsedMessage.TryGetValue("id", out var symbol))
            {
                if (!parsedMessage["status"].ToString().Equals("ok"))
                    throw new APIException("Huobi: subscription to product failed");

                var counter = Interlocked.Increment(ref ws.SubscribeCounter);
                ConsoleWriter.WriteLine("Huobi: websocket {0} out of {1} subscribed to {2} ({3} out of {4})", 
                    index + 1, NumWebsockets, SymbolToProduct[symbol.ToString()], 
                    counter, Interlocked.Read(ref ws.NumSubscribeProducts));
                return null;
            }

            if (parsedMessage.TryGetValue("ping", out object pingVal))
            {
                
                string pong = string.Format("{{\"pong\":{0}}}", long.Parse(pingVal.ToString()));
                ws.SendMessage(pong);
                return null;
            }

            var channel = parsedMessage["ch"].ToString();
            var product = SymbolToProduct[channel.Split('.')[1]];
            if (!channel.Contains("depth"))
                return null;

            var latency = measurement.ArrivalTime - UnixStartTime.AddMilliseconds(long.Parse(parsedMessage["ts"].ToString()));

            OrdersUpdate ordersUpdate = new OrdersUpdate(true, measurement, latency, isDiffUpdate: false);
            var ticks = (Dictionary<string, object>)parsedMessage["tick"];
            var bids = (List<object>)ticks["bids"];
            if (bids.Count > 0)
                ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), true), new HuobiOrderbookReader(bids));
            var asks = (List<object>)ticks["asks"];
            if (asks.Count > 0)
                ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), false), new HuobiOrderbookReader(asks));
            
            return ordersUpdate;
        }

        private static string CreateHttpOrderContent(string accountId, string symbol, string amount, 
            string type, string price = null)
        {
            const string fourParamFormat = "{{\"symbol\":\"{0}\",\"amount\":\"{1}\",\"type\":\"{2}\",\"account-id\":\"{3}\"}}";
            const string fiveParamFormat = "{{\"symbol\":\"{0}\",\"amount\":\"{1}\",\"type\":\"{2}\",\"account-id\":\"{3}\",\"price\":\"{4}\"}}";
            if (price != null)
                return string.Format(fiveParamFormat, symbol, amount, type, accountId, price);
            else
                return string.Format(fourParamFormat, symbol, amount, type, accountId);
        }
        protected override Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args)
        {
            Dictionary<string, object> requestParams = new Dictionary<string, object>();
            switch (requestType)
            {
                case UrlType.PingUrl:
                case UrlType.ProductsUrl:
                case UrlType.CurrenciesUrl:
                case UrlType.TickerUrl:
                    break;
                case UrlType.PrivateUrl:
                    DateTimeOffset timestamp = NtpAgent.GetAccurateCurrentUnixTime() + TimeSpan.FromSeconds(-1.0);
                    var query = new Dictionary<string, string>
                    {
                        { "Timestamp", timestamp.ToString("yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture)},
                        { "AccessKeyId", ApiKey },
                        { "SignatureMethod", "HmacSHA256" },
                        { "SignatureVersion", "2" }
                    };
                    requestParams["method"] = "GET";

                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        string price = null;
                        string type = "sell-market";
                        if (args.TryGetValue("type", out string orderType) && orderType.Equals("limit"))
                        {
                            type = args["command"].Equals("bid") ? "buy-limit" : "sell-limit";
                            price = args["price"];
                        }
                        else if (args["command"].Equals("bid"))
                        {
                            price = args["price"];
                            type = "buy-ioc";
                        }

                        if (args.TryGetValue("duplicate", out var dup))
                            requestParams["duplicate"] = dup.Equals("1");
                        requestParams["command"] = "order/orders/place";
                        requestParams["method"] = "POST";
                        requestParams["content"] = CreateHttpOrderContent(AccountId.ToString(),
                            ProductToSymbol[args["product"]], args["size"], type, price);
                        requestParams["content_type"] = "application/json";
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        if (AccountId == 0)
                        {
                            var res = MakeRequest(UrlType.PrivateUrl, new Dictionary<string, string> { { "command", "account" } });
                            AccountId = (int)res["account_id"];
                        }                            
                        
                        requestParams["command"] = string.Format("account/accounts/{0}/balance", AccountId);
                    }
                    else if(args["command"].Equals("account"))
                    {
                        requestParams["command"] = "account/accounts";
                    }
                    else if (args["command"].Equals("order"))
                    {
                        requestParams["command"] = "order/orders/" + args["orderId"];
                    }
                    else if (args["command"].Equals("cancel"))
                    {
                        requestParams["command"] = "order/orders/batchCancelOpenOrders";
                        requestParams["method"] = "POST";
                        requestParams["content"] = 
                            string.Format("{{\"account-id\":\"{0}\",\"symbol\":\"{1}\"}}", AccountId.ToString(),
                            ProductToSymbol[args["product"]]);
                        requestParams["content_type"] = "application/json";
                    }

                    string signHead = requestParams["method"] + "\napi.huobi.pro\n/v1/" + requestParams["command"] + "\n";
                    string signedData = UrlEncode(query, sorted: true);
                    using (var hmac = new HMACSHA256(Encoding.UTF8.GetBytes(ApiSecret)))
                    {
                        var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(signHead + signedData));
                        var signature = Convert.ToBase64String(hash);
                        requestParams["query"] = signedData + "&" +
                            UrlEncode(new Dictionary<string, string> { { "Signature", signature } });
                    }
                    break;
                case UrlType.DepthUrl:
                    requestParams["url"] = string.Format(Urls[UrlType.DepthUrl], ProductToSymbol[args["product"]], args["size"]);
                    break;
                default:
                    throw new NotImplementedException();
            }
            return requestParams;
        }

        private Dictionary<string, (double BaseMinSize, double QuoteMinSize)> HuobiGetTradeLimits()
        {
            var productTradeLimits = new Dictionary<string, (double BaseMinSize, double QuoteMinSize)>();

            var TradeLimitsFile = Environment.OSVersion.Platform == PlatformID.Unix ?
                                             "huobi_trade_limits.json" : "..\\..\\..\\huobi_trade_limits.json";

            if (!File.Exists(TradeLimitsFile))
                throw new APIException(string.Format("can't find {0} file", TradeLimitsFile));
            
            JObject TradeLimits;
            using (StreamReader r = new StreamReader(TradeLimitsFile))
            {
                TradeLimits = JObject.Parse(r.ReadToEnd());
            }

            foreach (var item in TradeLimits)
            {
                var baseAndQuote = item.Key.Split('-');
                var product = StandartizeCurrency(baseAndQuote[0].ToUpper()) + "-" +
                    StandartizeCurrency(baseAndQuote[1].ToUpper());
                productTradeLimits[product] = (BaseMinSize: item.Value["min_base_size"].ToObject<double>(), 
                    QuoteMinSize: item.Value["min_quote_size"].ToObject<double>());
            }

            return productTradeLimits;
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, 
            Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            switch (requestType)
            {
                case UrlType.PingUrl:
                    break;
                case UrlType.TickerUrl:
                    result = new Dictionary<string, object>();
                    foreach (var item in (JArray)response["data"])
                    {
                        var symbol = item["symbol"].ToString();
                        if (!SymbolToProduct.ContainsKey(symbol))
                            continue;

                        var price = 0.5 * (item["low"].ToObject<double>() + item["high"].ToObject<double>());
                        result[SymbolToProduct[symbol]] = price;
                    }
                    break;
                case UrlType.CurrenciesUrl:
                    result = new Dictionary<string, object>();
                    var badCurrencies = new List<string>();
                    foreach (var info in (JArray)response["data"])
                    {
                        if (!info["deposit_enabled"].ToObject<bool>() || !info["withdraw_enabled"].ToObject<bool>())
                            badCurrencies.Add(StandartizeCurrency(info["currency_code"].ToString().ToUpper()));
                    }
                    result["bad"] = badCurrencies;
                    break;
                case UrlType.ProductsUrl:
                    // https://api.huobi.pro/v2/beta/common/symbols
                    var productTradeLimits = HuobiGetTradeLimits();
                    foreach (var item in (JArray)response["data"])
                    {
                        string baseCurrency = item["base-currency"].ToString();
                        string quoteCurrency = item["quote-currency"].ToString();
                        var symbol = baseCurrency + quoteCurrency;
                        string product = StandartizeCurrency(baseCurrency.ToUpper()) + "-" + 
                            StandartizeCurrency(quoteCurrency.ToUpper());
                        int PricePrecision = Math.Min(8, item["price-precision"].ToObject<int>());
                        int BasePrecision = item["amount-precision"].ToObject<int>();
                        if (!productTradeLimits.TryGetValue(product, out (double BaseMinSize, double QuoteMinSize) tradeLimits))
                        {
                            ConsoleWriter.WriteLine("{0}: Removing {1} - failed to get trade limits", Name, product);
                            continue;
                        }
                        var BaseMinSize = Math.Max(tradeLimits.BaseMinSize, Math.Pow(10, -1 * BasePrecision));
                        ProductInfo[product] = new Product(
                            Symbol: symbol,
                            isFeeAlwaysFromQuote: false,
                            BaseMinSize: BaseMinSize,
                            QuoteMinSize: tradeLimits.QuoteMinSize,
                            QuotePrecision: PricePrecision,
                            PricePrecision: PricePrecision,
                            BasePrecision: BasePrecision,
                            TakerFee: 0.2 / 100,
                            MakerFee: 0.2 / 100);
                    }
                    break;
                case UrlType.PrivateUrl:
                    result = new Dictionary<string, object>();
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        result["dstSize"] = 0.0;
                        result["srcSize"] = 0.0;
                        result["rate"] = 0.0;
                        result["size"] = "0.0";
                        result["price"] = "0.0";
                        result["success"] = false;

                        if (response["status"].ToString().Equals("ok"))
                        {
                            result["success"] = true;
                            result["orderId"] = response["data"].ToObject<long>();
                        }
                        else
                        {
                            var errorMsg = response["err-msg"].ToString();
                            if (Config.ContinueExecutionOnFailedOrders)
                                ConsoleWriter.WriteLine("Huobi: {0}", errorMsg);
                            else
                                throw new APIException(errorMsg);
                        }   
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        foreach (var item in (JArray)response["data"]["list"])
                        {
                            if (!item["type"].ToString().Equals("trade"))
                                continue;
                            var currency = StandartizeCurrency(item["currency"].ToString().ToUpper());
                            result[currency] = item["balance"].ToObject<double>();
                        }
                    }
                    else if (args["command"].Equals("account"))
                    {
                        result["account_id"] = response["data"][0]["id"].ToObject<int>();
                    }
                    else if (args["command"].Equals("order"))
                    {
                        JObject data = (JObject)response["data"];
                        if (data["state"].ToString().Equals("submitted"))
                        {
                            // retry until order filled
                            GetOrderInfo(long.Parse(args["orderId"]));
                            result = null;
                        }
                        else
                        {
                            var isBid = data["type"].ToString().Contains("buy");
                            result["product"] = SymbolToProduct[data["symbol"].ToString()];
                            result["side"] = isBid ? "bid" : "ask";

                            var quoteSize = data["field-cash-amount"].ToObject<double>();
                            var baseSize = data["field-amount"].ToObject<double>();
                            var fee = data["field-fees"].ToObject<double>();
                            var price = quoteSize / baseSize;
                            result["price"] = price;
                            result["size"] = baseSize;

                            if (isBid)
                                baseSize -= fee;
                            else
                                quoteSize -= fee;

                            (double srcSize, double dstSize) = isBid ? (quoteSize, baseSize) : (baseSize, quoteSize);
                            result["srcSize"] = srcSize;
                            result["dstSize"] = dstSize;
                            result["rate"] = dstSize / srcSize;
                        }
                    }
                    else if (args["command"].Equals("cancel"))
                    {
                    }
                    break;
                case UrlType.DepthUrl:
                    result = new Dictionary<string, object>();
                    List<string> sides;
                    if (args.TryGetValue("side", out var s))
                        sides = new List<string>() { s };
                    else
                        sides = new List<string> { "bids", "asks" };

                    foreach (var side in sides)
                    {
                        var orderbook = new SortedDictionary<double, double>(side.Equals("bids") ?
                            Comparer<double>.Create((x, y) => -Comparer<double>.Default.Compare(x, y)) : Comparer<double>.Default);
                        foreach (var candle in (JArray)response["tick"][side])
                            orderbook[candle[0].ToObject<double>()] = candle[1].ToObject<double>();
                        result[side] = orderbook;
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            var info = ProductInfo[product];

            (double SrcSize, double rate) = (item.SrcSize, item.Rate);
            var price = isBidCommand ? 1 / rate * (1 + 0.5 * (1 - Config.AskReplySafetyFactor)) : rate;
            var executionPrice = price.ToString(string.Format("F{0}", info.PricePrecision));

            string executionSize;
            double DstSize;
            if (!isBidCommand)
            {
                SrcSize = Math.Floor(SrcSize / info.BaseStep) * info.BaseStep;
                if (info.BaseMinSize - SrcSize > Config.NumericEpsilon)
                    return new RateItem(0.0, 0.0);
                DstSize = SrcSize * rate;
                executionSize = SrcSize.ToString(string.Format("F{0}", info.BasePrecision));
            }
            else
            {
                DstSize = Math.Floor(SrcSize * rate / info.BaseStep) * info.BaseStep;
                if (info.BaseMinSize - DstSize > Config.NumericEpsilon)
                    return new RateItem(0.0, 0.0);
                SrcSize = DstSize / rate;
                executionSize = DstSize.ToString(string.Format("F{0}", info.BasePrecision));
            }

            var fee = DstSize * info.TakerFee;
            DstSize -= fee;

            return new RateItem(SrcSize, DstSize, executionSize, executionPrice);
        }
    }

    class HuobiOrderbookReader : IDictionary<double, double>
    {
        List<object> TicksData;

        public HuobiOrderbookReader(List<object> ticksData)
        {
            TicksData = ticksData;
        }

        public int Count => TicksData.Count;

        public IEnumerator<KeyValuePair<double, double>> GetEnumerator()
        {
            foreach (var item in TicksData)
            {
                var l = (List<object>)item;
                yield return new KeyValuePair<double, double>(
                    double.Parse((string)l[0]),
                    double.Parse((string)l[1]));
            }
        }

        public double this[double key] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ICollection<double> Keys => throw new NotImplementedException();

        public ICollection<double> Values => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(double key, double value)
        {
            throw new NotImplementedException();
        }

        public void Add(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(double key)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<double, double>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(double key)
        {
            throw new NotImplementedException();
        }

        public bool Remove(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(double key, out double value)
        {
            throw new NotImplementedException();
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
