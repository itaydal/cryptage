﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using Common;
using Common.Types;
using Newtonsoft.Json.Linq;

namespace ExchangeAPI.Exchanges
{
    public class ExchangeOKEX : Exchange
    {

        private Thread _orderInfoThread;
        private ConcurrentQueue<long> _orderIdQueue;
        private ConcurrentQueue<(long orderId, Tuple<EdgeKey, RateItem> result)> _orderInfoResultQueue;
        public ExchangeOKEX(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
            _orderIdQueue = new ConcurrentQueue<long>();
            _orderInfoResultQueue = new ConcurrentQueue<(long orderId, Tuple<EdgeKey, RateItem> result)>();
            _orderInfoThread = new Thread(OrderInfoMessageLoop);
            _orderInfoThread.Start();
        }

        public override ExchangeName Name => ExchangeName.OKEX;
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string>
        {
            { UrlType.ProductsUrl, "https://www.okex.com/v2/spot/markets/products" },
            { UrlType.CurrenciesUrl, "https://www.okex.com/v2/spot/markets/currencies" },
            { UrlType.PrivateUrl, "https://www.okex.com/api/v1/{0}"},
            { UrlType.TickerUrl, "https://www.okex.com/v2/spot/markets/tickers" },
            //{ UrlType.WebsocketUrl, "wss://real.okex.com:10441/websocket" },
            { UrlType.WebsocketUrl, "wss://real.okex.com:10440/websocket/okexapi?compress=true" },
            { UrlType.PingUrl, "https://www.okex.com/v2/spot/markets/tickers?symbol=btc_usdt" },
            { UrlType.DepthUrl, "https://www.okex.com/api/v1/depth.do?symbol={0}&size={1}" }
        };

        protected override int ApiPingIntervalMsec { get; set; } = 60 * 1000; // disconnects after 400 seconds
        public override (int count, TimeSpan period) RequestLimit { get; set; } = (1000, new TimeSpan(0, 0, 60));
        protected override Dictionary<string, string> NonStandardCurrency { get; set; } = new Dictionary<string, string>
        {
            { "IOTA", "MIOTA" }, { "YOYO", "YOYOW" }
        };

        protected override bool WebsocketsAlwaysOn { get; set; } = true;
        protected override int NumWebsockets { get; set; } = 10;
        protected override CompressionMethod WebsocketCompression { get; set; } = CompressionMethod.Zlib;

        private void PingWebsocket(WebSocketWrapper ws)
        {
            try
            {
                while (true)
                {
                    ws.SendMessage("{\"event\":\"ping\"}");
                    Thread.Sleep(30 * 1000);
                }
            }
            catch (Exception e)
            {
                ConsoleWriter.WriteLine("OKEX: PingWebsocket got exception {0}", e.Message);
            }
        }

        private static int NumBatchSubscribes = 10;
        private static string depth_suffix = "_depth"; // "_depth_10" for full orderbook updates
        private bool overrideOrderbook = !depth_suffix.EndsWith("depth");

        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            ws.SendMessage(new JObject { { "event", "login" },
                { "parameters",
                    new JObject {
                        { "api_key", ApiKey },
                        { "sign", Sign(UrlEncode(new Dictionary<string, string> { { "api_key", ApiKey } }, sorted: true)) }
                    }
                }
            }.ToString());

            var index = _websockets.IndexOf(ws);
            SubscribeToSymbols(ws);

            if (ws.ApplicationPinger == null)
            {
                ws.ApplicationPinger = new Thread(() => PingWebsocket(ws));
                ws.ApplicationPinger.Start();
            }
        }

        private void SubscribeToSymbols(WebSocketWrapper ws)
        {
            var startIndex = (int)Interlocked.Read(ref ws.SubscribeCounter);
            var count = Math.Min(NumBatchSubscribes, (int)Interlocked.Read(ref ws.NumSubscribeProducts) - startIndex);
            var depthMessageFormat = "{{\"event\":\"addChannel\",\"channel\":\"ok_sub_spot_{0}{1}\"}},";
            var orderMessageFormat =
                "{{\"event\":\"addChannel\",\"channel\":\"ok_sub_spot_{0}_order\",\"parameters\":{{\"api_key\":\"{1}\",\"symbol\":\"{0}\",\"sign\":\"{2}\"}}}},";

            var messageArray = "[";
            foreach (var symbol in GetWebsocketSubscribeSymbols(ws).Skip(startIndex).Take(count))
            {
                if (ws._subscribe)
                    messageArray += string.Format(depthMessageFormat, symbol, depth_suffix);

                var parameters = new Dictionary<string, string> { { "api_key", ApiKey }, { "symbol", symbol } };
                var signature = Sign(UrlEncode(parameters, sorted: true));
                messageArray += string.Format(orderMessageFormat, symbol, ApiKey, signature);
            }
            ws.SendMessage(messageArray.TrimEnd(',') + "]");
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws = null)
        {
            OrdersUpdate ordersUpdate = null;
            if (message.StartsWith("{\"event\":\"pong\"}"))
            {
                // ping reply (pong)
                return null;
            }

            var index = _websockets.IndexOf(ws);
            var parsedMessages = (List<object>)_jsonParser[index].Parse(message);
            foreach (var item in parsedMessages)
            {
                var parsedMessage = (Dictionary<string, object>)item;
                
                var channel = parsedMessage["channel"].ToString();
                if (channel.EndsWith(depth_suffix))
                {
                    var product = SymbolToProduct[channel.Substring(12).Replace(depth_suffix, "")];
                    var parsedMessageParams = (Dictionary<string, object>)parsedMessage["data"];
                    var timestamp = long.Parse(parsedMessageParams["timestamp"].ToString());

                    var latency = measurement.ArrivalTime - UnixStartTime.AddMilliseconds(timestamp);
                    if (ordersUpdate == null)
                        ordersUpdate = new OrdersUpdate(overrideOrderbook, measurement, latency, isDiffUpdate: false);

                    var bids = (List<object>)parsedMessageParams["bids"];
                    if (bids.Count > 0)
                        ordersUpdate.Add(new EdgeKey(Name, GetBase(product), GetQuote(product), true),
                            new OKEXOrderbookReader(bids));
                    var asks = (List<object>)parsedMessageParams["asks"];
                    if (asks.Count > 0)
                        ordersUpdate.Add(new EdgeKey(Name, GetBase(product), GetQuote(product), false),
                            new OKEXOrderbookReader(asks));
                    break;
                }

                if (channel.Equals("addChannel"))
                {
                    var data = (Dictionary<string, object>)parsedMessage["data"];
                    var subChannel = data["channel"].ToString();
                    if (!subChannel.EndsWith("_order"))
                        break;

                    var product = SymbolToProduct[subChannel.Replace("ok_sub_spot_", "").Replace("_order", "")];
                    var counter = Interlocked.Increment(ref ws.SubscribeCounter);
                    ConsoleWriter.WriteLine("OKEX: websocket {0} out of {1} subscribed to {2} ({3} out of {4})", 
                        index + 1, _websockets.Count, product, counter, Interlocked.Read(ref ws.NumSubscribeProducts));

                    if (counter > 1 && (counter % NumBatchSubscribes == 0))
                        SubscribeToSymbols(ws);
                    break;
                }

                if (channel.EndsWith("_order"))
                {
                    ParseTradeInfo((Dictionary<string, object>)parsedMessage["data"]);
                    break;
                }

                if (channel.Equals("login") || channel.EndsWith("_balance"))
                    break;

                throw new APIException("");
            }
            return ordersUpdate; // can be null
        }

        protected override Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args)
        {
            Dictionary<string, object> requestParams = new Dictionary<string, object>();
            switch (requestType)
            {
                case UrlType.PingUrl:
                case UrlType.ProductsUrl:
                case UrlType.CurrenciesUrl:
                case UrlType.TickerUrl:
                    break;
                case UrlType.PrivateUrl:
                    var content = new Dictionary<string, string> {
                        { "api_key", ApiKey }
                    };
                    requestParams["method"] = "POST";

                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        requestParams["command"] = "trade.do";
                        if (args.TryGetValue("duplicate", out var dup))
                            requestParams["duplicate"] = dup.Equals("1");
                        content["symbol"] = ProductToSymbol[args["product"]];
                        if (args["command"].Equals("bid") ||
                            (args.TryGetValue("type", out string orderType) && orderType.Equals("limit")))
                        {
                            content["type"] = args["command"].Equals("bid") ? "buy" : "sell";
                            content["price"] = args["price"];
                            content["amount"] = args["size"];
                        }
                        else
                        {
                            content["amount"] = args["size"];
                            content["type"] = "sell_market";
                        }
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        requestParams["command"] = "userinfo.do";
                    }
                    else if (args["command"].Equals("cancel"))
                    {
                        requestParams["command"] = "cancel_order.do";
                        content["symbol"] = ProductToSymbol[args["product"]];
                        content["order_id"] = args["orderId"];
                    }

                    requestParams["content"] = AddSignatureToContent(content);
                    break;
                case UrlType.DepthUrl:
                    requestParams["url"] = string.Format(Urls[UrlType.DepthUrl], ProductToSymbol[args["product"]], args["size"]);
                    break;
                default:
                    throw new NotImplementedException();
            }
            return requestParams;
        }

        private string Sign(string data)
        {
            var hash = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(data + "&secret_key=" + ApiSecret));
            return ByteToString(hash, uppercase: true);
        }

        private string AddSignatureToContent(Dictionary<string, string> parameters)
        {
            var signedData = UrlEncode(parameters, sorted: true);
            return signedData + "&sign=" + Sign(signedData);
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, 
            Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            switch (requestType)
            {
                case UrlType.PingUrl:
                    break;
                case UrlType.CurrenciesUrl:
                    result = new Dictionary<string, object>();
                    var badCurrencies = new List<string>();
                    foreach (var info in (JArray)response["data"])
                    {
                        if (!info["rechargeable"].ToObject<bool>() || !info["withdrawable"].ToObject<bool>() ||
                            !info["canTransfer"].ToObject<bool>())
                            badCurrencies.Add(StandartizeCurrency(info["symbol"].ToString()));
                    }
                    result["bad"] = badCurrencies;
                    break;
                case UrlType.ProductsUrl:
                    foreach (var item in (JArray)response["data"])
                    {
                        var symbol = item["symbol"].ToString();
                        var baseC = StandartizeCurrency(symbol.Split('_')[0].ToUpper());
                        var quoteC = StandartizeCurrency(symbol.Split('_')[1].ToUpper());
                        var product = baseC + "-" + quoteC;
                        var pricePrecision = item["maxPriceDigit"].ToObject<int>();
                        var basePrecision = item["maxSizeDigit"].ToObject<int>();
                        var baseMinSize = item["minTradeSize"].ToObject<double>();
                        var quoteStepSize = item["quoteIncrement"].ToObject<double>();
                        var quotePrecision = item["quotePrecision"].ToObject<int>();
                        //var quoteMinSize = Math.Pow(10, -1 * quotePrecision);
                        ProductInfo[product] = new Product(Symbol: symbol,
                                TakerFee: 0.2 / 100, MakerFee: 0.15 / 100,
                                isFeeAlwaysFromQuote: false, BasePrecision: basePrecision, BaseMinSize: baseMinSize,
                                QuoteStep: quoteStepSize, QuotePrecision: quotePrecision, //QuoteMinSize: quoteMinSize,
                                PricePrecision: pricePrecision);
                    }
                    break;
                case UrlType.TickerUrl:
                    result = new Dictionary<string, object>();
                    foreach (var item in (JArray)response["data"])
                    {
                        var symbol = item["symbol"].ToString();
                        if (!SymbolToProduct.ContainsKey(symbol))
                            continue;
                        result[SymbolToProduct[symbol]] = 0.5 * (item["buy"].ToObject<double>() + item["sell"].ToObject<double>());
                    }
                    break;
                case UrlType.PrivateUrl:
                    result = new Dictionary<string, object>();
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        result["dstSize"] = 0.0;
                        result["srcSize"] = 0.0;
                        result["rate"] = 0.0;
                        result["size"] = "0.0";
                        result["price"] = "0.0";
                        result["success"] = false;
                        result["orderId"] = (long)0;
                        if (((JObject)response).ContainsKey("error_code"))
                        {
                            var errorCode = response["error_code"].ToObject<int>();
                            var errorMsg = string.Format("OKEX: order failed with error code: {0}", errorCode);
                            if (Config.ContinueExecutionOnFailedOrders)
                                ConsoleWriter.WriteLine(errorMsg);
                            else
                                throw new APIException(errorMsg);
                        }
                        else
                        {
                            result["success"] = true;
                            result["orderId"] = response["order_id"].ToObject<long>();
                            result["product"] = args["product"];
                        }
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        result = new Dictionary<string, object>();
                        foreach (var item in (JObject)response["info"]["funds"]["free"])
                            result[StandartizeCurrency(item.Key.ToUpper())] = item.Value.ToObject<double>();
                    }
                    else if (args["command"].Equals("cancel"))
                    {
                    }
                    break;
                case UrlType.DepthUrl:
                    result = new Dictionary<string, object>();
                    List<string> sides;
                    if (args.TryGetValue("side", out var s))
                        sides = new List<string>() { s };
                    else
                        sides = new List<string> { "bids", "asks" };

                    foreach (var side in sides)
                    {

                        var orderbook = new SortedDictionary<double, double>(side.Equals("bids") ?
                            Comparer<double>.Create((x, y) => -Comparer<double>.Default.Compare(x, y)) : Comparer<double>.Default);
                        foreach (var candle in (JArray)response[side])
                            orderbook[candle[0].ToObject<double>()] = candle[1].ToObject<double>();
                        result[side] = orderbook;
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        private void OrderInfoMessageLoop()
        {
            while (true)
            {
                if (_orderIdQueue.TryDequeue(out var orderId))
                {
                    if (PendingOrderInfo.ContainsKey(orderId))
                    {
                        _marketOrderResponseQueue.Enqueue((ExecutionCycleIdByOrderId[orderId], PendingOrderInfo[orderId].Item1,
                            PendingOrderInfo[orderId].Item2));
                        PendingOrderInfo.Remove(orderId);
                        ExecutionCycleIdByOrderId.Remove(orderId);
                    }
                }
                else if (_orderInfoResultQueue.TryDequeue(out var orderInfo))
                {
                    if (ExecutionCycleIdByOrderId.ContainsKey(orderInfo.orderId))
                    {
                        _marketOrderResponseQueue.Enqueue((ExecutionCycleIdByOrderId[orderInfo.orderId], orderInfo.result.Item1,
                            orderInfo.result.Item2));
                        ExecutionCycleIdByOrderId.Remove(orderInfo.orderId);
                    }
                    else
                    {
                        PendingOrderInfo[orderInfo.orderId] = orderInfo.result;
                    }
                }
                else
                    Thread.Sleep(1);
            }
        }

        Dictionary<long, Tuple<EdgeKey, RateItem>> savedTrades = new Dictionary<long, Tuple<EdgeKey, RateItem>>();
        private void ParseTradeInfo(Dictionary<string, object> trade)
        {
            var orderId = long.Parse(trade["orderId"].ToString());
            var product = SymbolToProduct[trade["symbol"].ToString()];
            var type = trade["tradeType"].ToString();
            var status = int.Parse(trade["status"].ToString());
            var isBidCommand = type.Contains("buy");
            var averagePrice = trade["averagePrice"].ToString();
            var completedTradeAmount = double.Parse(trade["completedTradeAmount"].ToString());
            var tradePrice = double.Parse(trade["tradePrice"].ToString());
            var tradeUnitPrice = double.Parse(trade["tradeUnitPrice"].ToString());

            if (status != 2)
                return;

            (double srcDize, double dstSize, string price) = isBidCommand ?
                (tradePrice, completedTradeAmount * (1 - ProductInfo[product].TakerFee), averagePrice) :
                (completedTradeAmount, tradePrice * (1 - ProductInfo[product].TakerFee), 
                (tradePrice / completedTradeAmount).ToString());

            var resultItem = new Tuple<EdgeKey, RateItem>(
                new EdgeKey(Name, GetBase(product), GetQuote(product), !isBidCommand),
                new RateItem(srcDize, dstSize, completedTradeAmount.ToString(), price));
            _orderInfoResultQueue.Enqueue((orderId, resultItem));
        }

        private Dictionary<long, Tuple<EdgeKey, RateItem>> PendingOrderInfo = new Dictionary<long, Tuple<EdgeKey, RateItem>>();

        protected override void GetOrderInfo(long orderId, string product = null)
        {
            _orderIdQueue.Enqueue(orderId);
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            var info = ProductInfo[product];

            (double SrcSize, double rate) = (item.SrcSize, item.Rate);
            var price = isBidCommand ? 1 / rate * (1 + 0.5 * (1 - Config.AskReplySafetyFactor)) : rate;
            var executionPrice = price.ToString(string.Format("F{0}", info.PricePrecision));

            string executionSize;
            double DstSize;
            if (!isBidCommand)
            {
                SrcSize = Math.Floor(SrcSize / info.BaseStep) * info.BaseStep;
                if (info.BaseMinSize - SrcSize > Config.NumericEpsilon)
                    return new RateItem(0.0, 0.0);
                DstSize = SrcSize * rate;
                executionSize = SrcSize.ToString(string.Format("F{0}", info.BasePrecision));
            }
            else
            {
                /*
                SrcSize = Math.Floor(SrcSize / info.QuoteStep) * info.QuoteStep;
                if (info.QuoteMinSize - SrcSize > Config.NumericEpsilon)
                    return new RateItem(0.0, 0.0);

                DstSize = SrcSize * rate;
                if (info.BaseMinSize - DstSize > Config.NumericEpsilon)
                    return new RateItem(0.0, 0.0);

                executionSize = SrcSize.ToString(string.Format("F{0}", info.PricePrecision));
                */
                
                DstSize = Math.Floor(SrcSize * rate / info.BaseStep) * info.BaseStep;
                if (info.BaseMinSize - DstSize > Config.NumericEpsilon)
                    return new RateItem(0.0, 0.0);
                SrcSize = DstSize / rate;
                executionSize = DstSize.ToString(string.Format("F{0}", info.BasePrecision));
                
            }

            var fee = DstSize * info.TakerFee;
            //var precisionFactor = Math.Pow(10, isBidCommand ? info.BasePrecision : info.QuotePrecision);
            //fee = Math.Ceiling(fee * precisionFactor) / precisionFactor;
            DstSize -= fee;

            return new RateItem(SrcSize, DstSize, executionSize, executionPrice);
        }
    }

    class OKEXOrderbookReader : IDictionary<double, double>
    {
        List<object> TicksData;

        public OKEXOrderbookReader(List<object> ticksData)
        {
            TicksData = ticksData;
        }

        public int Count => TicksData.Count;

        public IEnumerator<KeyValuePair<double, double>> GetEnumerator()
        {
            foreach (var item in TicksData)
            {
                var l = (List<object>)item;
                yield return new KeyValuePair<double, double>(
                    double.Parse((string)l[0]),
                    double.Parse((string)l[1]));
            }
        }

        public double this[double key] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public ICollection<double> Keys => throw new NotImplementedException();

        public ICollection<double> Values => throw new NotImplementedException();

        public bool IsReadOnly => throw new NotImplementedException();

        public void Add(double key, double value)
        {
            throw new NotImplementedException();
        }

        public void Add(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public bool ContainsKey(double key)
        {
            throw new NotImplementedException();
        }

        public void CopyTo(KeyValuePair<double, double>[] array, int arrayIndex)
        {
            throw new NotImplementedException();
        }

        public bool Remove(double key)
        {
            throw new NotImplementedException();
        }

        public bool Remove(KeyValuePair<double, double> item)
        {
            throw new NotImplementedException();
        }

        public bool TryGetValue(double key, out double value)
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
