﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Common.Types;

namespace ExchangeAPI.Exchanges
{
    public class ExchangePoloniex : Exchange
    {
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string>
        {
            { UrlType.ProductsUrl, "https://poloniex.com/public?command=return24hVolume" },
            { UrlType.WebsocketUrl, "wss://api2.poloniex.com" },
            { UrlType.PrivateUrl, "https://poloniex.com/tradingApi" }
        };
        public override (int count, TimeSpan period) RequestLimit { get; set; } = (count: 6, period: new TimeSpan(0, 0, 1));

        private Dictionary<int, string> ChannelIdToProduct;

        public ExchangePoloniex(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
            ChannelIdToProduct = new Dictionary<int, string>
            {
                { 1001, "trollbox" },
                { 1002, "ticker" },
                { 1010, "heartbeat" }
            };
        }

        public override ExchangeName Name => ExchangeName.Poloniex;

        protected override Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args)
        {
            Dictionary<string, object> requestParams = new Dictionary<string, object>();

            if (args["command"].Equals("bid") || args["command"].Equals("ask"))
            {
                Dictionary<string, string> data = new Dictionary<string, string>
                {
                    ["command"] = args["command"].Equals("bid") ? "buy" : "sell",
                    ["rate"] = args["command"].Equals("bid") ? "0": this.ProductInfo[args["product"]].PriceMin.ToString(),
                    ["currencyPair"] = ProductToSymbol[args["product"]],
                    ["amount"] = args["size"], ["fillOrKill"] = "1"
                };
                requestParams["data"] = UrlEncode(data);
                requestParams["method"] = "POST";
            }
            return requestParams;
        }

        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            JObject o = new JObject { { "command", "subscribe" } };
            foreach (string symbol in SymbolToProduct.Keys)
            {
                o["channel"] = symbol;
                ws.SendMessage(o.ToString(Formatting.None));
            }
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws = null)
        {
            JToken token = JToken.Parse(message);

            token = (JArray)token;
            int channelId = token[0].ToObject<int>();
            if (this.ChannelIdToProduct.ContainsKey(channelId) && this.ChannelIdToProduct[channelId].Equals("heartbeat"))
                return null;
            else
            {
                Dictionary<double, double> bidsUpdate = new Dictionary<double, double>();
                Dictionary<double, double> asksUpdate = new Dictionary<double, double>();
                OrdersUpdate ordersUpdate = new OrdersUpdate(false, measurement, TimeSpan.FromSeconds(0.0));
                string product = null;
                foreach (var data in token[2])
                {
                    string updateType = data[0].ToString();
                    if (updateType.Equals("i"))
                    {
                        JObject info = (JObject)data[1];
                        product = SymbolToProduct[info["currencyPair"].ToString()];
                        this.ChannelIdToProduct[channelId] = product;
                        bool isBid = false;
                        ordersUpdate.OverrideOrderbooks = true;
                        foreach (var orders in (JArray)info["orderBook"])
                        {
                            foreach (var order in (JObject)orders)
                            {
                                Dictionary<double, double> update = isBid ? bidsUpdate : asksUpdate;
                                update.Add(Convert.ToDouble(order.Key), Convert.ToDouble(order.Value));
                            }
                            ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), isBid), isBid ? bidsUpdate : asksUpdate);
                            isBid = true;
                        }
                        return ordersUpdate;
                    }
                    else if (updateType.Equals("o"))
                    {
                        product = ChannelIdToProduct[channelId];
                        bool isBid = data[1].ToObject<int>() == 0 ? false : true;
                        Dictionary<double, double> update = isBid ? bidsUpdate : asksUpdate;
                        update.Add(Convert.ToDouble(data[2]), Convert.ToDouble(data[3]));
                    }
                }
                if (product != null)
                {
                    ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), true), bidsUpdate);
                    ordersUpdate.Add(new EdgeKey(this.Name, GetBase(product), GetQuote(product), false), asksUpdate);
                    return ordersUpdate;
                }
            }
            return null;
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, 
            Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            if (requestType == UrlType.ProductsUrl)
            {
                foreach (var pair in (JObject)response)
                {
                    string symbol = pair.Key;
                    if (symbol.Contains("total"))
                        continue;
                    string product = string.Join("-", symbol.Split('_').Reverse<string>());
                    //ProductInfo[product] = new Product(Symbol: symbol, TakerFee: 0.25 / 100, MakerFee: 0.15 / 100);
                }
            }
            return result;
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            throw new NotImplementedException();
        }
    }
}
