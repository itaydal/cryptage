﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Types;
using Newtonsoft.Json.Linq;

namespace ExchangeAPI.Exchanges
{
    public class ExchangeTemplate : Exchange
    {
        public ExchangeTemplate(string ApiKey = null, string ApiSecret = null) : base(ApiKey, ApiSecret)
        {
        }

        public override ExchangeName Name => throw new NotImplementedException();
        public override Dictionary<UrlType, string> Urls { get; set; } = new Dictionary<UrlType, string>
        {
            { UrlType.ProductsUrl, "" },
            { UrlType.PrivateUrl, ""},
            { UrlType.WebsocketUrl, "" }
        };
        public override (int count, TimeSpan period) RequestLimit { get; set; } = (1, new TimeSpan(0, 0, 10));

        public override void _websocketOnConnect(WebSocketWrapper ws)
        {
            throw new NotImplementedException();
        }

        public override OrdersUpdate _websocketParseMessge(string message, DelayMeasurement measurement, WebSocketWrapper ws = null)
        {
            throw new NotImplementedException();
        }

        protected override Dictionary<string, object> ProcessRequest(UrlType requestType, Dictionary<string, string> args)
        {
            Dictionary<string, object> requestParams = new Dictionary<string, object>();
            switch (requestType)
            {
                case UrlType.ProductsUrl:
                    throw new NotImplementedException();
                case UrlType.PrivateUrl:
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        throw new NotImplementedException();
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        throw new NotImplementedException();
                    }
                    else
                        throw new APIException(args["command"]);
                default:
                    throw new NotImplementedException();
            }
            return requestParams;
        }

        protected override Dictionary<string, object> ProcessResponse(UrlType requestType, JToken response, 
            Dictionary<string, string> args)
        {
            Dictionary<string, object> result = null;
            switch (requestType)
            {
                case UrlType.ProductsUrl:
                    throw new NotImplementedException();
                case UrlType.PrivateUrl:
                    if (args["command"].Equals("bid") || args["command"].Equals("ask"))
                    {
                        throw new NotImplementedException();
                    }
                    else if (args["command"].Equals("balance"))
                    {
                        throw new NotImplementedException();
                    }
                    else
                        throw new NotImplementedException(args["command"]);
                default:
                    throw new NotImplementedException();
            }
            return result;
        }

        protected override RateItem _adjustRateItem(string product, bool isBidCommand, RateItem item)
        {
            throw new NotImplementedException();
        }
    }
}
