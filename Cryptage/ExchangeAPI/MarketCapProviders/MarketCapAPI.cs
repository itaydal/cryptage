﻿using Common;
using Common.Types;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeAPI.MarketCapProviders
{
    public class MarketCapAPI
    {
        public static int LIMIT { get; private set; } = 5000;
        public static Dictionary<string, MarketCapItem> marketCap = null;
        public static Dictionary<string, MarketCapItem> MarketCap
        {
            get => marketCap ?? UpdateMarketCap();
            set => marketCap = value;
        }

        public static Dictionary<string, MarketCapItem> UpdateMarketCap()
        {
            var cryptoMarketCap = GetCryptoMarketCup();
            var fiatMarketCap = GetFiatMarketCup();

            var newMarketCap = cryptoMarketCap.Union(fiatMarketCap).ToDictionary(x => x.Key, x => x.Value);

            MessageBroker.PublishMessage(new MarketCapUpdate(newMarketCap));
            MarketCap = newMarketCap;
            return newMarketCap;
        }
        public static Dictionary<string, MarketCapItem> GetFiatMarketCup()
        {
            //TODO: implement real api call
            return new Dictionary<string, MarketCapItem>
            {
                {"USD", new MarketCapItem("USD", 1, 1/8500.0, 0.4, 1, false) },
                {"EUR", new MarketCapItem("EUR", 1.2, 1.2/8500.0, 0.3, 2, false) },
                {"GBP", new MarketCapItem("GBP", 1.36, 1.36/8500.0, 0.2, 3, false) },
                {"JPY", new MarketCapItem("JPY", 0.0092, 0.0092/8500.0, 0.05, 4, false) },
                {"PLN", new MarketCapItem("PLN", 0.269993, 0.269993/8500.0, 0.05, 5, false) }
            };
        }

        private static List<string> IgnoredSymbols { get; set; } = new List<string>
        {
            "SPD"
        };
        private static List<string> IgnoredIDs { get; set; } = new List<string>
        {
            "bitgem", "icoin", "uncoin", "get-protocol", "sovereign-hero", "comet", "betacoin",
            "blazecoin", "blockcat", "bitmark", "blocklancer", "kingn-coin", "rcoin", "netcoin",
            "catcoin"
        };

        public static Dictionary<string, MarketCapItem> GetCryptoMarketCup()
        {
            var globalUri = @"https://api.coinmarketcap.com/v1/global/";
            var uriTemplate = @"https://api.coinmarketcap.com/v1/ticker/?start={0}&limit={1}";

            JToken globalMarketData = JToken.Parse(Get(globalUri));
            
            var totalMarketCapUsd = globalMarketData["total_market_cap_usd"].ToObject<double>();

            var marketCapItems = new Dictionary<string, MarketCapItem>();
            for (int i = 0; ; i+= LIMIT)
            {
                var response = Get(string.Format(uriTemplate, i, LIMIT));
                if (response.Equals(string.Empty))
                    break;

                //dynamic marketCapArray = JsonConvert.DeserializeObject(response);
                JArray marketCapArray = JArray.Parse(response);

                foreach (var m in marketCapArray)
                {
                    var id = m["id"].ToString();
                    var symbol = m["symbol"].ToObject<string>();

                    if (IgnoredIDs.Contains(id))
                        continue;

                    if (IgnoredSymbols.Contains(symbol))
                        continue;

                    if (m["price_usd"].Type == JTokenType.Null || m["price_btc"].Type == JTokenType.Null)
                        continue;

                    double marketCapUsd;
                    if (m["market_cap_usd"].Type == JTokenType.Null)
                        marketCapUsd = Config.NumericEpsilon;
                    else
                        marketCapUsd = m["market_cap_usd"].ToObject<double>();

                    var rank = m["rank"].ToObject<int>();
                    var mci = new MarketCapItem(
                        m["symbol"].ToObject<string>(),
                        m["price_usd"].ToObject<double>(),
                        m["price_btc"].ToObject<double>(),
                        marketCapUsd / totalMarketCapUsd,
                        rank,
                        true);

                    if (!marketCapItems.ContainsKey(mci.Symbol))
                        marketCapItems.Add(mci.Symbol, mci);
                    else if (marketCapItems[mci.Symbol].Rank < mci.Rank)
                        marketCapItems[mci.Symbol] = mci;
                    else
                        Console.WriteLine(string.Format("GetCryptoMarketCup: already have symbol {0} with higher rank", mci.Symbol));
                }
            }
            
            return marketCapItems;
        }

        private static string Get(string uri)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (WebException)
            {
                return string.Empty;
            }
        }
    }

    
}
