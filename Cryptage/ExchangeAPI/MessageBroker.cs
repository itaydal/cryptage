﻿using Common.Types;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExchangeAPI
{
    public static class MessageBroker
    {
        public static event Action<ExecutionReport> ExecutionReportEvent = delegate { };
        public static event Action<OrdersUpdate> OrdersUpdateEvent = delegate { };
        public static event Action<EdgeInfoUpdate> EdgeInfoUpdateEvent = delegate { };
        public static event Action<ExchangeFundsSnapshot> ExchangeFundsSnapshotEvent = delegate { };
        public static event Action<MarketCapUpdate> MarketCapUpdateEvent = delegate { };

        public static void PublishMessage(ExecutionReport msg)
        {
            ExecutionReportEvent(msg);
        }
        public static void PublishMessage(OrdersUpdate msg)
        {
            OrdersUpdateEvent(msg);
        }
        public static void PublishMessage(EdgeInfoUpdate msg)
        {
            EdgeInfoUpdateEvent(msg);
        }
        public static void PublishMessage(ExchangeFundsSnapshot msg)
        {
            ExchangeFundsSnapshotEvent(msg);
        }
        public static void PublishMessage(MarketCapUpdate msg)
        {
            MarketCapUpdateEvent(msg);
        }
    }
}
