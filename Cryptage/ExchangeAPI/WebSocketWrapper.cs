﻿using System;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common;
using Common.Types;

namespace ExchangeAPI
{
    public enum CompressionMethod { None, Gzip, Zlib };
    public class WebSocketWrapper
    {
        private readonly Uri _uri;
        private readonly List<IPAddress> _ips;
        private ConcurrentQueue<(WebsocketHeaderOpcode opcode, byte[] payload, int len)> _messagesToSendQueue =
            new ConcurrentQueue<(WebsocketHeaderOpcode opcode, byte[] payload, int len)>();
        private ConcurrentQueue<byte[]> _pingReplyQueue = new ConcurrentQueue<byte[]>();
        public Thread ApplicationPinger = null;

        public int MessageQueueCount { get => _messagesToSendQueue.Count; }
        public long SubscribeCounter = 0;
        public long NumSubscribeProducts = 0;

        private Action<string, DelayMeasurement, WebSocketWrapper> _onMessage;
        private Action<WebSocketWrapper> _onConnected;
        private Action<WebSocketWrapper> _onDisconnected;
        private CompressionMethod _compressionMethod;
        public readonly bool _subscribe;

        private string globalGuid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

        private SslStream sslStream = null;
        private NetworkStream networkStream = null;
        private byte[] inBuffer = new byte[1 << 20]; // 1MB
        private MemoryStream fragmentsStream = new MemoryStream();
        private MemoryStream decompressedStream = new MemoryStream();

        private Task<int> readTask = null;

        private static readonly int WebsocketHeaderMinLen = 2;
        private static readonly int WebsocketHeaderMaxLen = WebsocketHeaderMinLen + sizeof(ulong);
        public enum WebsocketHeaderOpcode { ContinuationFrame = 0x0, TextFrame = 0x1, BinaryFrame = 0x2,
            CloseFrame = 0x8, PingFrame = 0x9, PongFrame = 0xA };
        private static readonly int WebsocketMaskLen = 4;
        private readonly bool _prioritySend;
        private readonly int _pingInterval;

        public WebSocketWrapper(string uri, List<IPAddress> AllowedIPs, 
            Action<string, DelayMeasurement, WebSocketWrapper> onMessage,
            Action<WebSocketWrapper> onConnect = null, Action<WebSocketWrapper> onDisconnect = null,
            CompressionMethod compressionMethod = CompressionMethod.None, bool subscribe = true, bool prioritySend = false, 
            int pingInterval = 50)
        {
            _uri = new Uri(uri);
            _ips = AllowedIPs;
            _compressionMethod = compressionMethod;
            _subscribe = subscribe;
            _onMessage = onMessage;
            _onConnected = onConnect;
            _onDisconnected = onDisconnect;
            _prioritySend = prioritySend;
            _pingInterval = pingInterval;

            new Thread(WorkerThread).Start();
            new Thread(PingerThread).Start();
        }

        private void PingerThread()
        {
            Thread.Sleep(30000);

            var sendPayload = new byte[10];
            var rnd = new Random();
            while (true)
            {
                rnd.NextBytes(sendPayload);
                var sendTime = DateTime.Now;
                _messagesToSendQueue.Enqueue((WebsocketHeaderOpcode.PingFrame, sendPayload, sendPayload.Length));

                var gotReply = false;
                while (DateTime.Now < sendTime + TimeSpan.FromSeconds(10))
                {
                    if (_pingReplyQueue.TryDequeue(out var recvPayload))
                    {
                        //ConsoleWriter.WriteLine("{0} - {1} recv pong frame <{2}>", DateTime.Now, _uri.Host, Convert.ToBase64String(recvPayload));
                        if (recvPayload.Length == sendPayload.Length && recvPayload.Zip(sendPayload, (a, b) => a == b).All(c => c))
                        {
                            gotReply = true;
                            break;
                        }
                    }
                    else
                        Thread.Sleep(10);
                }

                if (!gotReply)
                {
                    ConsoleWriter.WriteLine("{0} - {1} didn't get pong for <{2}>, sending close frame", 
                        DateTime.Now, _uri.Host, Convert.ToBase64String(sendPayload));
                    _messagesToSendQueue.Enqueue((WebsocketHeaderOpcode.CloseFrame, sendPayload, sendPayload.Length));
                }

                Thread.Sleep(_pingInterval * 1000);
            }
        }

        private bool Connect()
        {
            if (sslStream != null)
                Utils.SslDispose(sslStream, networkStream);

            (sslStream, networkStream) = Utils.SslConnectApi(_uri.Host, _ips, _uri.Port, _prioritySend);
            var MaxHttpHeaderLen = 24000;
            var buffer = new byte[MaxHttpHeaderLen];

            var key = new byte[16];
            new Random().NextBytes(key);
            var encodedKey = Convert.ToBase64String(key);

            var requestParams = new Dictionary<string, object>
            {
                { "headers", new Dictionary<string, string> {
                    { "Upgrade", "websocket" }, { "Connection", "Upgrade" }, { "Origin", _uri.Host },
                    { "Sec-WebSocket-Key", encodedKey }, { "Sec-WebSocket-Version", "13" }
                } }
            };

            var headerSize = Utils.CreateHttpRequest(_uri.AbsoluteUri, requestParams, buffer, 0);
            Utils.WriteSslStream(sslStream, buffer, headerSize);
            (HttpStatusCode status, var responseHeaders, string resonseData) = Utils.ReceiveHttpResponse(sslStream);
            if (status != HttpStatusCode.SwitchingProtocols)
                return false;

            if (!responseHeaders.TryGetValue("upgrade", out var upgrade) || !upgrade.ToLower().Equals("websocket"))
                return false;

            if (!responseHeaders.TryGetValue("connection", out var connection) || !connection.ToLower().Equals("upgrade"))
                return false;

            if (!responseHeaders.TryGetValue("sec-websocket-accept", out var serverHash))
                return false;

            Interlocked.Exchange(ref SubscribeCounter, 0);

            using (var sha = new SHA1CryptoServiceProvider())
            {
                var hash = sha.ComputeHash(Encoding.UTF8.GetBytes(encodedKey + globalGuid));
                return Convert.ToBase64String(hash).Equals(serverHash);
            }
        }

        public void SendMessage(string message)
        {
            var buffer = Encoding.UTF8.GetBytes(message);
            _messagesToSendQueue.Enqueue((WebsocketHeaderOpcode.TextFrame, buffer, buffer.Length));
        }

        private byte[] sendHeaderBuffer = new byte[WebsocketHeaderMaxLen];
        private void SendFrame(WebsocketHeaderOpcode opcode, byte[] payload, int payloadLen)
        {
            //var sendHeaderBuffer = new byte[WebsocketHeaderMaxLen];
            var fin = 1; var mask = 1;
            var rsv1 = 0; var rsv2 = 0; var rsv3 = 0;
            sendHeaderBuffer[0] = (byte)(fin << 7 | rsv1 << 6 | rsv2 << 5 | rsv3 << 4 | (int)opcode);
            int headerLen;
            if (payloadLen < 0x7E)
            {
                sendHeaderBuffer[1] = (byte)((mask << 7) | payloadLen);
                headerLen = WebsocketHeaderMinLen;
            }
            else if (payloadLen < (1 << 16))
            {
                sendHeaderBuffer[1] = (byte)((mask << 7) | 0x7E);
                for (int i = 0; i < 2; i++)
                    sendHeaderBuffer[2 + i] = (byte)(0xFF & (payloadLen >> (16 - 8 - 8 * i)));
                headerLen = WebsocketHeaderMinLen + sizeof(ushort);
            }
            else
            {
                sendHeaderBuffer[1] = (byte)((mask << 7) | 0x7F);
                for (int i = 0; i < 8; i++)
                    sendHeaderBuffer[2 + i] = (byte)(0xFF & (payloadLen >> (64 - 8 - 8*i)));
                headerLen = WebsocketHeaderMinLen + sizeof(ulong);
            }

            var frameLen = headerLen + WebsocketMaskLen + payloadLen;
            var frameBuffer = new byte[frameLen];
            Buffer.BlockCopy(sendHeaderBuffer, 0, frameBuffer, 0, headerLen);

            var maskKey = new byte[WebsocketMaskLen];
            new Random().NextBytes(maskKey);
            Buffer.BlockCopy(maskKey, 0, frameBuffer, headerLen, WebsocketMaskLen);

            for (int i = 0; i < payloadLen; i++)
                frameBuffer[headerLen + WebsocketMaskLen + i] = (byte)(payload[i] ^ maskKey[i % 4]);

            Utils.WriteSslStream(sslStream, frameBuffer, frameLen);
        }

        private (WebsocketHeaderOpcode opcode, int messageLen, bool fin) HandleReceivedHeader(int offset)
        {
            var fin = 0x1 & (inBuffer[offset] >> 7);
            var rsv123 = 0x7 & (inBuffer[offset] >> 4);
            if (rsv123 != 0)
                throw new APIException(string.Format("rsv123 != 0 {0}", rsv123));

            var opcode = (WebsocketHeaderOpcode)(0xF & inBuffer[offset]);

            var mask = 0x1 & (inBuffer[offset + 1] >> 7);
            if (mask != 0)
                throw new APIException("mask != 0");

            int messageLen = (0x7F & inBuffer[offset + 1]);
            return (opcode, messageLen, fin == 1);
        }

        private int ParseMessageLen(int offset, int fieldSize)
        {
            ulong messageLen = 0;
            for (int i = 0; i < fieldSize; i++)
                messageLen += (ulong)(inBuffer[offset + i] << (8 * (fieldSize - (i + 1))));

            if (messageLen > int.MaxValue)
                throw new APIException("too long message");

            return (int)messageLen;
        }

        private void HandleReceivedPayload(int offset, WebsocketHeaderOpcode opcode, bool fin, int len, DelayMeasurement measurement)
        {
            switch (opcode)
            {
                case WebsocketHeaderOpcode.TextFrame:
                case WebsocketHeaderOpcode.BinaryFrame:
                case WebsocketHeaderOpcode.ContinuationFrame:
                    if (fragmentsStream.Length < fragmentsStream.Position + len)
                        fragmentsStream.SetLength(fragmentsStream.Position + len);

                    fragmentsStream.Write(inBuffer, offset, len);
                    break;
                case WebsocketHeaderOpcode.PingFrame:
                case WebsocketHeaderOpcode.CloseFrame:
                case WebsocketHeaderOpcode.PongFrame:
                    var recvPayload = new byte[len];
                    var replyOpcode = opcode == WebsocketHeaderOpcode.PingFrame ? WebsocketHeaderOpcode.PongFrame : opcode;
                    Buffer.BlockCopy(inBuffer, offset, recvPayload, 0, len);
                    if (opcode == WebsocketHeaderOpcode.PongFrame)
                    {
                        _pingReplyQueue.Enqueue(recvPayload);
                        return;
                    }
                    else
                    {
                        _messagesToSendQueue.Enqueue((replyOpcode, recvPayload, recvPayload.Length));
                        if (opcode == WebsocketHeaderOpcode.CloseFrame)
                            throw new APIException("got close frame");
                        return;
                    }
                default:
                    throw new APIException(string.Format("unsuported opcode {0}", opcode));
            }

            if (fin)
            {
                measurement.Add("message receiving");
                string message;
                switch (_compressionMethod)
                {
                    case CompressionMethod.None:
                        message = Encoding.UTF8.GetString(fragmentsStream.GetBuffer(), 0, (int)fragmentsStream.Position);
                        break;
                    case CompressionMethod.Gzip:
                        fragmentsStream.Position = 0;
                        using (var commpressedStream = new GZipStream(fragmentsStream, CompressionMode.Decompress, true))
                        {
                            commpressedStream.CopyTo(decompressedStream);
                            message = Encoding.UTF8.GetString(decompressedStream.GetBuffer(), 0, (int)decompressedStream.Position);
                        }
                        break;
                    case CompressionMethod.Zlib:
                        fragmentsStream.Position = 0;
                        using (var commpressedStream = new DeflateStream(fragmentsStream, CompressionMode.Decompress, true))
                        {
                            commpressedStream.CopyTo(decompressedStream);
                            message = Encoding.UTF8.GetString(decompressedStream.GetBuffer(), 0, (int)decompressedStream.Position);
                        }
                        break;
                    default:
                        throw new APIException("unsupported compression method");
                }

                measurement.Add("message decompression");

                _onMessage(message, measurement, this);
                decompressedStream.Position = 0;
                fragmentsStream.Position = 0;
            }
        }

        private enum ReceiveState { Header, Size, Payload };
        private void WorkerThread()
        {
            bool wasClosed = true;

            int bytesNeeded;
            int readPosition;
            int writePosition;
            ReceiveState state;
            bool fin;
            DateTime arrivalTime = DateTime.MinValue;
            DelayMeasurement delayMeasurement = new DelayMeasurement(_uri.Host);
            WebsocketHeaderOpcode opcode = WebsocketHeaderOpcode.TextFrame;

            void InitReadState()
            {
                state = ReceiveState.Header;
                bytesNeeded = WebsocketHeaderMinLen;
                readPosition = 0;
                writePosition = 0;
                decompressedStream.Position = 0;
                fragmentsStream.Position = 0;
                fin = true;
            }
            InitReadState();

            while (true)
            {
                try
                {
                    while (true)
                    {
                        if (wasClosed)
                        {
                            wasClosed = false;
                            if (!Connect())
                                throw new APIException("connection failed");

                            InitReadState();
                            Task.Run(() => _onConnected(this));
                            readTask = sslStream.ReadAsync(inBuffer, 0, inBuffer.Length);
                        }
                        else if (_messagesToSendQueue.TryDequeue(out var msg))
                        {
                            SendFrame(msg.opcode, msg.payload, msg.len);
                            switch (msg.opcode)
                            {
                                case WebsocketHeaderOpcode.CloseFrame:
                                    throw new APIException("close frame sent to server");
                                case WebsocketHeaderOpcode.PingFrame:
                                    //ConsoleWriter.WriteLine("{0} - {1} sent ping frame <{2}>", DateTime.Now, _uri.Host, Convert.ToBase64String(msg.payload));
                                    break;
                                default:
                                    break;
                            }
                        }
                        else if (readTask.IsCompleted || bytesNeeded <= writePosition - readPosition)
                        {
                            if (readTask.IsCompleted)
                            {
                                writePosition += readTask.Result;
                                if (writePosition > inBuffer.Length / 2)
                                {
                                    var copySize = writePosition - readPosition;
                                    Buffer.BlockCopy(inBuffer, readPosition, inBuffer, 0, copySize);
                                    writePosition = copySize;
                                    readPosition = 0;
                                }
                                readTask = sslStream.ReadAsync(inBuffer, writePosition, inBuffer.Length - writePosition);
                            }
                            if (bytesNeeded <= writePosition - readPosition)
                            {
                                switch (state)
                                {
                                    case ReceiveState.Header:
                                        int messageLen;
                                        if (fin == true)
                                            delayMeasurement = new DelayMeasurement(_uri.Host);
                                        (opcode, messageLen, fin) = HandleReceivedHeader(readPosition);
                                        readPosition += bytesNeeded;
                                        if (messageLen < 0x7E)
                                        {
                                            state = ReceiveState.Payload;
                                            bytesNeeded = messageLen;
                                        }
                                        else if (messageLen == 0x7E)
                                        {
                                            state = ReceiveState.Size;
                                            bytesNeeded = sizeof(ushort);
                                        }
                                        else
                                        {
                                            state = ReceiveState.Size;
                                            bytesNeeded = sizeof(ulong);
                                        }
                                        break;
                                    case ReceiveState.Size:
                                        var offset = readPosition;
                                        readPosition += bytesNeeded;
                                        bytesNeeded = ParseMessageLen(offset, bytesNeeded);
                                        state = ReceiveState.Payload;
                                        break;
                                    case ReceiveState.Payload:
                                        HandleReceivedPayload(readPosition, opcode, fin, bytesNeeded, delayMeasurement);
                                        readPosition += bytesNeeded;
                                        bytesNeeded = WebsocketHeaderMinLen;
                                        state = ReceiveState.Header;
                                        break;
                                    default:
                                        throw new APIException("bad state");
                                }
                            }
                        }
                        else
                            Thread.Sleep(1);
                    }
                }
                catch (Exception e)
                {
                    wasClosed = true;
                    ConsoleWriter.WriteLine("{0}: {1} websocket: {2}", DateTime.UtcNow, _uri.Host, e.Message);

                    Task.Run(() => _onDisconnected(this)).Wait();
                    Thread.Sleep(500);
                }
            }
        }
    }
}