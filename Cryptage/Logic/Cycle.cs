using Logic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Types;
using System.Threading;
using Common;
using ExchangeAPI;

namespace Logic
{
    public class Cycle
    {
        private int hashCode = 0;
        private double approximateRate = 0.0;

        public CycleDescription Description { get; private set; }
        public int EdgeCount { get; private set; }
        public Edge[] Edges { get; private set; }
        public bool[] IsCrossEdge { get; private set; }
        public bool IsMarketCurrenciesCycle { get; private set; }
        public bool IsCrossingCycle { get; private set; }
        public double ApproximateRate { get { return approximateRate; } }
        public double MaxFlowRate { get; private set; }
        public double AdjustedMaxFlowRate { get; private set; }
        public RateItem[] MaxFlow { get; private set; }
        public RateItem[] AdjustedMaxFlow { get; private set; }
        public Dictionary<string, double> ExpectedProfit { get; private set; }
        public Dictionary<NodeKey, double> ExpectedFundsDiff { get; private set; }
        public double ExpectedFlowSizeUsd { get; private set; }
        public double ExpectedTotalUsdProfit { get; private set; }
        public int MostFundedNodeIdx { get; private set; }
        public FlowLimitReason MaxFlowLimitReason { get; private set; }
        public double LargestMinSizeUsd { get; private set; }
        public double FlowUpperLimitUsd { get; private set; }

        //TODO: remove
        public int[] candlesConsumed;
        public static long BaseMinSizeFilterCountBeforeMF = 0;
        public static long BaseMinSizeFilterCountFunds = 0;
        public static long BaseMinSizeFilterCountOrderbookDeplition = 0;
        public static long BaseMinSizeFilterCountRateDepletion = 0;
        public static long QuantizationFilterCount = 0;


        public Cycle(Edge[] edges, CycleDescription cycleDescription)
        {
            this.approximateRate = 0.0;
            this.MaxFlowRate = 0.0;
            this.MaxFlow = null;
            this.AdjustedMaxFlow = null;
            this.ExpectedProfit = null;
            this.ExpectedTotalUsdProfit = 0.0;
            this.MostFundedNodeIdx = -1;

            this.Description = cycleDescription;
            this.Edges = edges;
            this.EdgeCount = this.Edges.Length;
            this.IsCrossEdge = new bool[this.EdgeCount];
            FinalizeCycle();
        }

        private void FinalizeCycle()
        {
            if (!this.Edges.Select((e, i) => e.EdgeKey.Equals(this.Description.EdgeKeys[i])).All(b => b))
                throw new InvalidOperationException("Missmatch between edges and cycle description");

            if (!Edges[0].SrcCurrency.Equals(Edges[this.EdgeCount-1].DstCurrency))
                throw new InvalidOperationException("Can't finalize open cycle");

            var firstEdgeIdx = Edges.Select((e, i) => (e.ToString(), i)).Min().Item2;
            Utils.LeftShiftArray(Edges, firstEdgeIdx);

            Edge currEdge, nextEdge;
            IsMarketCurrenciesCycle = true;
            IsCrossingCycle = false;
            bool crossingExchange;
            for (int i = 0; i < EdgeCount; i++)
            {
                int j = (i + 1) % EdgeCount;
                currEdge = Edges[i];
                nextEdge = Edges[j];

                crossingExchange = !currEdge.DstNode.Exchange.Equals(nextEdge.SrcNode.Exchange);
                IsCrossEdge[j] = crossingExchange;
                IsCrossingCycle |= IsCrossEdge[j];

                if (crossingExchange)
                    IsMarketCurrenciesCycle &= Config.MarketCurrencies.Contains(currEdge.DstCurrency);
            }
            hashCode = this.ToString().GetHashCode();
        }

        public double CalcCurrRate()
        {
            approximateRate = 1.0;
            foreach (var edge in Edges)
                approximateRate *= edge.RefRate;

            return approximateRate;
        }

        public bool CalcMaxFlow(Dictionary<NodeKey, double> availableFunds, Dictionary<string, MarketCapItem> marketCap, 
            out Dictionary<NodeKey, double> fundsToLock)
        {
            if (!CalcFundsLimit(availableFunds, marketCap, out int originEdgeIdx, out double[] fundsLimit, out fundsToLock))
                return false;

            int edgeCount = EdgeCount;
            int i, j, k;
            candlesConsumed = new int[edgeCount];

            this.MaxFlowRate = 1;
            this.ExpectedProfit = new Dictionary<string, double>();
            this.ExpectedFundsDiff = new Dictionary<NodeKey, double>();
            this.ExpectedTotalUsdProfit = 0;

            IEnumerator<RateItem>[] ratebooks = new IEnumerator<RateItem>[edgeCount];
            RateItem[] bestRates = new RateItem[edgeCount];
            RateItem[] cumulativeFlowRates = new RateItem[edgeCount];
            for (i = 0; i < edgeCount; i++)
            {
                this.ExpectedProfit[Edges[i].SrcNode.Currency] = 0;
                this.ExpectedFundsDiff[Edges[i].SrcNode] = 0;
                this.ExpectedFundsDiff[Edges[i].DstNode] = 0;

                IEnumerator<RateItem> ratebook = Edges[i].GetRatebookEnumerator();
                ratebooks[i] = ratebook;
                if (!ratebook.MoveNext())
                    throw new Exception("Can't find max flow for cycle with an edge with an empty orderbook");
                bestRates[i] = ratebook.Current;
                cumulativeFlowRates[i] = new RateItem(0, 0);
            }

            RateItem currBestRate;
            double currRate;
            double currFlowVolume;
            double currNodeSizeLimit;
            int currFlowLimitIdx;

            bool keepRunning = true;
            while (keepRunning)
            {
                currRate = 1;
                currFlowLimitIdx = originEdgeIdx;
                currFlowVolume = Math.Min(bestRates[originEdgeIdx].SrcSize, fundsLimit[originEdgeIdx]);

                for (i = 0; i < edgeCount; i++)
                {
                    j = (originEdgeIdx + i) % edgeCount;
                    k = (j + 1) % edgeCount;
                    currBestRate = bestRates[j];
                    currRate *= currBestRate.Rate;

                    currNodeSizeLimit = Math.Min(bestRates[k].SrcSize, fundsLimit[k]);
                    if ((currNodeSizeLimit < currBestRate.Rate * currFlowVolume) && (k != originEdgeIdx))
                    {
                        currFlowLimitIdx = k;
                        currFlowVolume = currNodeSizeLimit;
                    }
                    else
                    {
                        currFlowVolume *= currBestRate.Rate;
                    }
                }

                if (currRate < 1)
                {
                    MaxFlowLimitReason = FlowLimitReason.RateDepletion;
                    break;
                }
                candlesConsumed[currFlowLimitIdx]++;

                for (i = edgeCount - 1; i >= 0; i--)
                {
                    j = (originEdgeIdx + i) % edgeCount;

                    cumulativeFlowRates[j].DstSize += currFlowVolume;
                    currFlowVolume /= bestRates[j].Rate;
                    cumulativeFlowRates[j].SrcSize += currFlowVolume;
                    
                    if (j == currFlowLimitIdx)
                    {
                        if (fundsLimit[j] < bestRates[j].SrcSize)
                        {
                            keepRunning = false;
                            MaxFlowLimitReason = FlowLimitReason.FundsDepletion;
                        }
                        if (!ratebooks[j].MoveNext())
                        {
                            keepRunning = false;
                            MaxFlowLimitReason = FlowLimitReason.OrderbookDeplition;
                        }

                        bestRates[j] = ratebooks[j].Current;
                    }
                    else
                    {
                        bestRates[j].ConsumeSrcSize(currFlowVolume);
                    }
                    fundsLimit[j] -= currFlowVolume;
                }
            }

            for (j = 0; j < edgeCount; j++)
            {
                this.MaxFlowRate *= cumulativeFlowRates[j].Rate;
                cumulativeFlowRates[j].DstSize /= Edges[j].EdgeInfoItem.FeeFactor;
            }

            bool isCurrEdgeAskBook;
            this.ExpectedFlowSizeUsd = 0;
            this.AdjustedMaxFlowRate = 1;
            RateItem[] adjustedFlow = new RateItem[edgeCount];
            double currSrcSize = double.NaN;
            for (i = 0; i < edgeCount; i++)
            {
                j = (originEdgeIdx + i) % edgeCount;

                Edge edge = Edges[j];
                RateItem flowRateItem = cumulativeFlowRates[j];

                if (!double.IsNaN(currSrcSize))
                    flowRateItem.SetSrcSizeFixedRate(currSrcSize);

                if (!Exchange.AdjustRateItem(edge.EdgeKey, flowRateItem, out RateItem adjustedFlowRateItem))
                {
                    switch (MaxFlowLimitReason)
                    {
                        case FlowLimitReason.FundsDepletion:
                            Interlocked.Increment(ref BaseMinSizeFilterCountFunds);
                            break;
                        case FlowLimitReason.RateDepletion:
                            Interlocked.Increment(ref BaseMinSizeFilterCountRateDepletion);
                            break;
                        case FlowLimitReason.OrderbookDeplition:
                            Interlocked.Increment(ref BaseMinSizeFilterCountOrderbookDeplition);
                            break;
                        default:
                            break;
                    }
                    return false;
                }

                currSrcSize = adjustedFlowRateItem.DstSize;
                adjustedFlow[j] = adjustedFlowRateItem;

                if ((!Double.IsNaN(currSrcSize)) && (1E-10 < adjustedFlowRateItem.SrcSize - flowRateItem.SrcSize))
                {
                    ConsoleWriter.WriteLine("!!!!!!!!!!!!!!! Negative expected profit case, edge - {0} !!!!!!!!!!!!!!!", edge);
                    ConsoleWriter.WriteLine("Last adjusted dst size {0}", currSrcSize);
                    ConsoleWriter.WriteLine("Flow rate {0} {1}-->(*{2})-->{3} {4}", flowRateItem.SrcSize, edge.SrcCurrency, flowRateItem.Rate, flowRateItem.DstSize, edge.DstCurrency);
                    ConsoleWriter.WriteLine("Adjsted rate  {0} {1}-->(*{2})-->{3} {4}", adjustedFlowRateItem.SrcSize, edge.SrcCurrency, adjustedFlowRateItem.Rate, adjustedFlowRateItem.DstSize, edge.DstCurrency);
                }

                var srcCurrPriceUsd = marketCap[Edges[j].SrcCurrency].PriceUsd;
                var dstCurrPriceUsd = marketCap[Edges[j].DstCurrency].PriceUsd;

                this.AdjustedMaxFlowRate *= adjustedFlowRateItem.Rate;

                this.ExpectedProfit[Edges[j].SrcCurrency] -= adjustedFlowRateItem.SrcSize;
                this.ExpectedProfit[Edges[j].DstCurrency] += adjustedFlowRateItem.DstSize;
                this.ExpectedFundsDiff[Edges[j].SrcNode] -= adjustedFlowRateItem.SrcSize;
                this.ExpectedFundsDiff[Edges[j].DstNode] += adjustedFlowRateItem.DstSize;

                this.ExpectedTotalUsdProfit -= adjustedFlowRateItem.SrcSize * srcCurrPriceUsd;
                this.ExpectedTotalUsdProfit += adjustedFlowRateItem.DstSize * dstCurrPriceUsd;
                this.ExpectedFlowSizeUsd = Math.Max(ExpectedFlowSizeUsd, adjustedFlowRateItem.SrcSize * srcCurrPriceUsd);

                isCurrEdgeAskBook = !Edges[j].EdgeKey.IsBidBook;
                if (fundsToLock.ContainsKey(Edges[j].SrcNode))
                    fundsToLock[Edges[j].SrcNode] += isCurrEdgeAskBook ? adjustedFlowRateItem.SrcSize / Config.AskReplySafetyFactor : adjustedFlowRateItem.SrcSize;
                else if (isCurrEdgeAskBook) // for the case of non crossing cycle - lock safty funds for ask reply command
                    fundsToLock[Edges[j].SrcNode] = adjustedFlowRateItem.SrcSize * (1 - Config.AskReplySafetyFactor) / Config.AskReplySafetyFactor;
            }

            this.MaxFlow = cumulativeFlowRates;
            this.AdjustedMaxFlow = adjustedFlow;

            if (adjustedFlow[j].DstSize < adjustedFlow[originEdgeIdx].SrcSize)
            {
                Interlocked.Increment(ref QuantizationFilterCount);
                return false;
            }

            return true;
        }
        
        private bool CalcFundsLimit(Dictionary<NodeKey, double> availableFunds, Dictionary<string, MarketCapItem> marketCap,
            out int originEdgeIdx, out double[] fundsLimit, out Dictionary<NodeKey, double> fundsToLock)
        {
            var edgeCount = EdgeCount;
            fundsToLock = new Dictionary<NodeKey, double>(edgeCount);
            fundsLimit = new double[edgeCount];
            originEdgeIdx = -1;
            Edge currEdge;

            NodeKey mostFundedNode = null;
            int mostFundedNodeIdx = -1;
            double mostFundedNodeFunds = double.MinValue;
            double mostFundedNodeUsd = double.MinValue;
            double leastFundedNodeUsd = double.MaxValue;
            int leastFundedNodeIdx = -1;
            double currAvailableFundsUsd;
            double currEdgeMinSizeUsd;
            bool currNodeShouldBeFunded;
            this.FlowUpperLimitUsd = double.MaxValue;
            this.LargestMinSizeUsd = 0;

            for (int i = 0; i < EdgeCount; i++)
            {
                currEdge = Edges[i];
                currNodeShouldBeFunded = IsCrossEdge[i] || !currEdge.EdgeKey.IsBidBook;

                if (!marketCap.TryGetValue(currEdge.SrcCurrency, out MarketCapItem currMarketCupSrc))
                    return false; //TODO: log

                MarketCapItem currMarketCupDst = null;
                if (!currEdge.EdgeKey.IsBidBook && !marketCap.TryGetValue(currEdge.DstCurrency, out currMarketCupDst))
                    return false; //TODO: log

                currEdgeMinSizeUsd = currEdge.EdgeInfoItem.MinSize * (currEdge.EdgeKey.IsBidBook ? currMarketCupSrc.PriceUsd : currMarketCupDst.PriceUsd);
                if (this.LargestMinSizeUsd < currEdgeMinSizeUsd)
                {
                    this.LargestMinSizeUsd = currEdgeMinSizeUsd;
                    originEdgeIdx = i;
                }

                if (!availableFunds.TryGetValue(currEdge.SrcNode, out double currAvailableFunds) || (currAvailableFunds < Config.NumericEpsilon))
                {
                    if (currNodeShouldBeFunded)
                        return false;
                    else
                    {
                        //TODO: restrict the case where there is a non crossing cycle without funds in any node - now this results with unlimited funds cycle
                        fundsLimit[i] = double.MaxValue;
                        continue;
                    }
                }

                currAvailableFundsUsd = currAvailableFunds * currMarketCupSrc.PriceUsd;
                if (mostFundedNodeUsd < currAvailableFundsUsd)
                {
                    mostFundedNodeFunds = currAvailableFunds;
                    mostFundedNodeUsd = currAvailableFundsUsd;
                    mostFundedNode = currEdge.SrcNode;
                    mostFundedNodeIdx = i;
                }
                if (currAvailableFundsUsd < leastFundedNodeUsd)
                {
                    leastFundedNodeUsd = currAvailableFundsUsd;
                    leastFundedNodeIdx = i;
                }

                if (!currNodeShouldBeFunded)
                {
                    fundsLimit[i] = double.MaxValue;
                }
                else if (IsCrossEdge[i])
                {
                    fundsToLock[currEdge.SrcNode] = 0; //marks that this should be filled after max flow calculation
                    fundsLimit[i] = Math.Min(currAvailableFunds, Config.MaxFlowSizeAllowedUsd / currMarketCupSrc.PriceUsd);
                    this.FlowUpperLimitUsd = Math.Min(this.FlowUpperLimitUsd, currAvailableFundsUsd);

                    if (!currEdge.EdgeKey.IsBidBook)
                        fundsLimit[i] *= Config.AskReplySafetyFactor;
                }
                else // ask reply edge in non crossing cycle
                {
                    double askReplyFundLimit = currAvailableFunds * Config.AskReplySafetyFactor / (1 - Config.AskReplySafetyFactor);
                    fundsLimit[i] = askReplyFundLimit;
                    this.FlowUpperLimitUsd = Math.Min(this.FlowUpperLimitUsd, askReplyFundLimit * currMarketCupSrc.PriceUsd);
                }
            }

            if (!this.IsCrossingCycle)
            {
                fundsToLock[mostFundedNode] = 0; //marks that this should be filled after max flow calculation
                fundsLimit[mostFundedNodeIdx] = Math.Min(mostFundedNodeFunds, Config.MaxFlowSizeAllowedUsd / marketCap[mostFundedNode.Currency].PriceUsd);
                this.FlowUpperLimitUsd = mostFundedNodeUsd;

                if (!Edges[mostFundedNodeIdx].EdgeKey.IsBidBook)
                    fundsLimit[mostFundedNodeIdx] *= Config.AskReplySafetyFactor;
            }

            if (this.FlowUpperLimitUsd < this.LargestMinSizeUsd)
            {
                Interlocked.Increment(ref BaseMinSizeFilterCountBeforeMF);
                return false;
            }

            this.MostFundedNodeIdx = mostFundedNodeIdx;
            return true;
        }
        

        public override string ToString()
        {
            if (EdgeCount == 2)
            {
                return string.Format(TwoEdgeCycleFormat, 
                    Edges[0].SrcNode.Currency,
                    Edges[0].Exchange, Edges[0].EdgeKey.IsBidBook ? "Bid" : "Ask",
                    Edges[0].DstNode.Currency,
                    Edges[1].Exchange, Edges[1].EdgeKey.IsBidBook ? "Bid" : "Ask",
                    Edges[1].DstNode.Currency);
            }
            else if (EdgeCount == 3)
            {
                return string.Format(ThreeEdgeCycleFormat,
                    Edges[0].SrcNode.Currency,
                    Edges[0].Exchange, Edges[0].EdgeKey.IsBidBook ? "Bid" : "Ask",
                    Edges[0].DstNode.Currency,
                    Edges[1].Exchange, Edges[1].EdgeKey.IsBidBook ? "Bid" : "Ask",
                    Edges[1].DstNode.Currency,
                    Edges[2].Exchange, Edges[2].EdgeKey.IsBidBook ? "Bid" : "Ask",
                    Edges[2].DstNode.Currency);
            }
            else if (EdgeCount == 4)
            {
                return string.Format(FourEdgeCycleFormat,
                    Edges[0].SrcNode.Currency,
                    Edges[0].Exchange, Edges[0].EdgeKey.IsBidBook ? "Bid" : "Ask",
                    Edges[0].DstNode.Currency,
                    Edges[1].Exchange, Edges[1].EdgeKey.IsBidBook ? "Bid" : "Ask",
                    Edges[1].DstNode.Currency,
                    Edges[2].Exchange, Edges[2].EdgeKey.IsBidBook ? "Bid" : "Ask",
                    Edges[2].DstNode.Currency,
                    Edges[3].Exchange, Edges[3].EdgeKey.IsBidBook ? "Bid" : "Ask",
                    Edges[3].DstNode.Currency);
            }
            else
            {
                var sb = new StringBuilder();
                sb.Append(Edges[0].SrcNode.Currency);
                for (int i = 0; i < EdgeCount; i++)
                {
                    sb.Append("-->");
                    sb.Append(string.Format("({0}-{1})", Edges[i].Exchange, Edges[i].EdgeKey.IsBidBook ? "Bid" : "Ask"));
                    sb.Append("-->");
                    sb.Append(Edges[i].DstNode.Currency);
                }
                return sb.ToString();
            }
        }

        private static readonly string TwoEdgeCycleFormat = "{0}-->({1}-{2})-->{3}-->({4}-{5})-->{6}";
        private static readonly string ThreeEdgeCycleFormat = "{0}-->({1}-{2})-->{3}-->({4}-{5})-->{6}-->({7}-{8})-->{9}";
        private static readonly string FourEdgeCycleFormat = "{0}-->({1}-{2})-->{3}-->({4}-{5})-->{6}-->({7}-{8})-->{9}-->({10}-{11})-->{12}";
    }

    public enum FlowLimitReason
    {
        FundsDepletion,
        RateDepletion,
        OrderbookDeplition,
    }
}
