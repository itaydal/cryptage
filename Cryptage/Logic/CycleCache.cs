﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Common.Types;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Logic
{
    public class CycleCache
    {
        private static CycleCache instance = null;
        private Thread dbWriterThread;

        private ConcurrentQueue<EdgeKey> edgeKeyWriteQueue = new ConcurrentQueue<EdgeKey>();
        private ConcurrentQueue<List<CycleDescription>> cycleWriteQueue = new ConcurrentQueue<List<CycleDescription>>();

        private bool isDbConnected = false;
        private bool isLoaded = false;

        private MongoClient dbClient = null;
        private IMongoDatabase database = null;
        private IMongoCollection<EdgeKey> edgeKeyCollection = null;
        private IMongoCollection<CycleDescription> cycleDescriptionCollection = null;


        private Dictionary<int, EdgeKey> edgeKeysById = null;
        private Dictionary<EdgeKey, int> idByEdgeKey = null;
        private int currEdgeKeysId = -1;

        private HashSet<CycleDescription> allCycles = null;
        private Dictionary<EdgeKey, List<CycleDescription>> cycleDescriptionsCache = null;
        private int currCycleDescriptionId = -1;

        public static CycleCache Instance
        {
            get
            {
                if (instance == null)
                    instance = new CycleCache();

                return instance;
            }
        }

        private CycleCache()
        {
            this.edgeKeysById = new Dictionary<int, EdgeKey>();
            this.idByEdgeKey = new Dictionary<EdgeKey, int>();
            this.allCycles = new HashSet<CycleDescription>();
            this.cycleDescriptionsCache = new Dictionary<EdgeKey, List<CycleDescription>>();

            dbWriterThread = new Thread(WriterLoop);
            dbWriterThread.Start();
        }

        private void WriterLoop()
        {
            try
            {
                while (true)
                {
                    if (isDbConnected)
                    {
                        if (edgeKeyWriteQueue.TryDequeue(out var edgeKey))
                        {
                            edgeKeyCollection.InsertOne(edgeKey);
                        }
                        else if (cycleWriteQueue.TryDequeue(out var cycles))
                        {
                            cycleDescriptionCollection.InsertMany(cycles);
                        }
                        else
                        {
                            Thread.Sleep(10);
                        }
                    }
                    else
                    {
                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {
                isDbConnected = false;
            }
        }

        public bool ConnectToDb()
        {
            if (isDbConnected)
                return true;

            try
            {
                dbClient = new MongoClient("mongodb://localhost:27017");
                database = dbClient.GetDatabase("Cryptage");
                if (database.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(5000))
                {
                    edgeKeyCollection = database.GetCollection<EdgeKey>("Edges");
                    cycleDescriptionCollection = database.GetCollection<CycleDescription>("Cycles");
                    isDbConnected = true;
                    return true;
                }
            }
            catch (Exception)
            {

            }

            dbClient = null;
            database = null;
            edgeKeyCollection = null;
            cycleDescriptionCollection = null;
            Console.WriteLine("Can't connect to DB, working with in-memory cache.");
            return false;
        }

        public bool LoadCycleCache()
        {
            if (isLoaded)
                return true;
            if (!ConnectToDb())
                return false;

            try
            {
                var edgeKeysCursor = edgeKeyCollection.Find(_ => true).ToList();
                foreach (var edgeKey in edgeKeysCursor)
                {
                    currEdgeKeysId = Math.Max(currEdgeKeysId, edgeKey.cacheId);
                    edgeKeysById.Add(edgeKey.cacheId, edgeKey);
                    idByEdgeKey.Add(edgeKey, edgeKey.cacheId);
                    cycleDescriptionsCache.Add(edgeKey, new List<CycleDescription>());
                }

                var cyclesCursor = cycleDescriptionCollection.Find(_ => true).ToList();
                foreach (var cycle in cyclesCursor)
                {
                    cycle.SetEdges(cycle.EdgeKeysIds.Select(eid => edgeKeysById[eid]));
                    currCycleDescriptionId = Math.Max(currCycleDescriptionId, cycle.cacheId);
                    allCycles.Add(cycle);

                    foreach (var edgeInCycle in cycle.EdgeKeys)
                        cycleDescriptionsCache[edgeInCycle].Add(cycle);
                }
                isLoaded = true;
                Console.WriteLine("Done loading cycles from DB");
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<EdgeKey> GetCachedEdges()
        {
            return new List<EdgeKey>(edgeKeysById.Values);
        }

        public bool TryGetCommonCycles(EdgeKey edgeKey, out List<CycleDescription> commonCycles)
        {
            return cycleDescriptionsCache.TryGetValue(edgeKey, out commonCycles);
        }

        public void PushEdgeKey(EdgeKey edgeKey)
        {
            if (idByEdgeKey.ContainsKey(edgeKey))
                return;

            edgeKey.cacheId = ++currEdgeKeysId;
            edgeKeysById.Add(edgeKey.cacheId, edgeKey);
            idByEdgeKey.Add(edgeKey, edgeKey.cacheId);
            cycleDescriptionsCache.Add(edgeKey, new List<CycleDescription>());

            if (isDbConnected)
                edgeKeyWriteQueue.Enqueue(edgeKey);
        }

        public void PushCycles(List<CycleDescription> cycleDescriptions)
        {
            var cyclesToPush = new List<CycleDescription>();
            foreach (var cycle in cycleDescriptions)
            {
                if (allCycles.Contains(cycle))
                    continue;

                cycle.SetEdgeIds(cycle.EdgeKeys.Select(e => idByEdgeKey[e]));
                cycle.cacheId = ++currCycleDescriptionId;
                allCycles.Add(cycle);
                cyclesToPush.Add(cycle);

                foreach (var edgeInCycle in cycle.EdgeKeys)
                    cycleDescriptionsCache[edgeInCycle].Add(cycle);
            }

            if (isDbConnected && (0 < cyclesToPush.Count))
                cycleWriteQueue.Enqueue(cyclesToPush);
        }
    }
}
