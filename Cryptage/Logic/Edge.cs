﻿using Common;
using Common.Types;
using Logic.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Logic
{
    public class Edge
    {
        private static Comparer<double> defaultComparer = Comparer<double>.Default;
        private Comparer<double> orderbookPriceComparer;
        private SortedDictionary<double, double> orderbook;
        private string stringRep;

        private double RefPriceInclusionValue;

        public EdgeKey EdgeKey { get; private set; }
        public ExchangeName Exchange { get { return EdgeKey.Exchange; } }
        public NodeKey BaseNode { get; private set; }
        public NodeKey QuoteNode { get; private set; }
        public string SrcCurrency { get; private set; }
        public NodeKey SrcNode { get; private set; }
        public string DstCurrency { get; private set; }
        public NodeKey DstNode { get; private set; }
        public double RefVolume { get; private set; }
        public double RefRate;
        public double RefPrice { get; private set; }
        public EdgeInfo EdgeInfoItem { get; private set; }

        public Edge(EdgeKey edgeKey, double refVolume, EdgeInfo edgeInfo)
        {
            this.EdgeKey = edgeKey;
            this.RefVolume = refVolume;
            this.EdgeInfoItem = edgeInfo;
            this.RefPrice = edgeKey.IsBidBook ? 0 : Double.MaxValue;
            this.RefPriceInclusionValue = edgeKey.IsBidBook ? 0 : Double.MaxValue;
            this.RefRate = 0;

            if (edgeKey.IsBidBook)
            {
                this.SrcCurrency = edgeKey.BaseCurrency;
                this.DstCurrency = edgeKey.QuoteCurrency;
            }
            else
            {
                this.SrcCurrency = edgeKey.QuoteCurrency;
                this.DstCurrency = edgeKey.BaseCurrency;
            }
            this.BaseNode = new NodeKey(EdgeKey.Exchange, EdgeKey.BaseCurrency);
            this.QuoteNode = new NodeKey(EdgeKey.Exchange, EdgeKey.QuoteCurrency);
            this.SrcNode = new NodeKey(EdgeKey.Exchange, SrcCurrency);
            this.DstNode = new NodeKey(EdgeKey.Exchange, DstCurrency);

            this.orderbook = new SortedDictionary<double, double>(orderbookPriceComparer);
            this.orderbookPriceComparer = edgeKey.IsBidBook ?
                Comparer<double>.Create((x, y) => -defaultComparer.Compare(x, y)) : defaultComparer;

            this.stringRep = string.Format("Edge: {0}-{1} in exchange {2}, {3}", 
                EdgeKey.BaseCurrency, EdgeKey.QuoteCurrency, 
                EdgeKey.Exchange, EdgeKey.IsBidBook ? "Bid Book" : "Ask Book");
        }

        public Edge(EdgeKey edgeKey, double refVolume, IDictionary<double, double> orderbook, EdgeInfo edgeInfo) : this(edgeKey, refVolume, edgeInfo)
        {
            ReplaceOrderbook(orderbook);
        }

        public bool ReplaceOrderbook(IDictionary<double, double> newOrderbook)
        {
            this.orderbook = new SortedDictionary<double, double>(orderbookPriceComparer);
            foreach (var kvp in newOrderbook)
            {
                if (Config.NumericEpsilon < kvp.Value)
                    this.orderbook[kvp.Key] = kvp.Value;
            }
            
            return UpdateRefPrice();
        }

        public bool UpdateOrderbook(IDictionary<double, double> orderbookChange, bool isDiffUpdate)
        {
            bool updateRefPrice = false;
            foreach (var order in orderbookChange)
            {
                if (isDiffUpdate)
                {
                    orderbook[order.Key] += order.Value;
                    if (orderbook[order.Key] < Config.NumericEpsilon)
                        orderbook.Remove(order.Key);
                }
                else
                {
                    if (Config.NumericEpsilon < order.Value)
                        orderbook[order.Key] = order.Value;
                    else
                        orderbook.Remove(order.Key);
                }
                    
                updateRefPrice = updateRefPrice || (EdgeKey.IsBidBook ? order.Key >= RefPriceInclusionValue : order.Key <= RefPriceInclusionValue);
            }

            if (updateRefPrice)
                return UpdateRefPrice();

            return false;
        }

        public void UpdateRefVolume(double newRefVolume)
        {
            this.RefVolume = newRefVolume;
            UpdateRefPrice();
        }

        public void UpdateEdgeInfo(EdgeInfo edgeInfo)
        {
            this.EdgeInfoItem = edgeInfo;
            UpdateRefPrice();
        }
        
        public IEnumerator<RateItem> GetRatebookEnumerator()
        {
            double feeFactor = EdgeInfoItem.FeeFactor;
            if (EdgeKey.IsBidBook)
            {
                foreach (var order in orderbook)
                {
                    yield return new RateItem(order.Value, order.Value * order.Key * feeFactor);
                }
            }
            else
            {
                foreach (var order in orderbook)
                {
                    yield return new RateItem(order.Value * order.Key / feeFactor, order.Value); 
                }
            }
        }

        public IEnumerator<KeyValuePair<double, double>> GetOrderbookEnumerator()
        {
            foreach (var order in orderbook)
            {
                yield return order;
            }
        }

        public KeyValuePair<double, double> GetBestCandle()
        {
            return orderbook.FirstOrDefault();
        }

        private bool UpdateRefPrice()
        {
            RefPrice = 0;
            double currVolume = 0;
            foreach (var order in orderbook)
            {
                RefPriceInclusionValue = order.Key;
                if (currVolume + order.Value < RefVolume)
                {
                    RefPrice += (order.Key * order.Value);
                    currVolume += order.Value;
                }
                else
                {
                    var remainingVolume = RefVolume - currVolume;
                    RefPrice += (order.Key * remainingVolume);
                    currVolume += remainingVolume;
                    break;
                }
                
            }

            double oldRate = RefRate;
            RefPrice /= currVolume;
            RefRate = EdgeKey.IsBidBook ? EdgeInfoItem.FeeFactor * RefPrice : EdgeInfoItem.FeeFactor / RefPrice;
            if (currVolume < RefVolume) //In the case where orderbook is too thin
            {
                RefPrice = EdgeKey.IsBidBook ? 0 : Double.MaxValue;
                RefPriceInclusionValue = EdgeKey.IsBidBook ? 0 : Double.MaxValue;
                RefRate = 0;
                return false;
            }

            return oldRate + Config.NumericEpsilon < RefRate;
        }

        public override string ToString()
        {
            return this.stringRep;
        }
    }
}
