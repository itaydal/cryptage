using Logic.Types;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Common.Types;
using Common;
using ExchangeAPI;
using System.IO;

namespace Logic
{
    public class MarketMonitor
    {
        private static MarketMonitor _instance = new MarketMonitor();

        private Queue<OrdersUpdate> _ordersUpdateBuffer;
        private ConcurrentQueue<OrdersUpdate> _ordersUpdateQueue;
        private ConcurrentQueue<ExchangeFundsSnapshot> _exchangeFundsSnapshotQueue;
        private ConcurrentQueue<MarketCapUpdate> _marketCapUpdateQueue;
        private ConcurrentQueue<EdgeInfoUpdate> _edgeInfosUpdateQueue;
        private ConcurrentQueue<ExecutionReport> _executionReportQueue;
        private ConcurrentQueue<Tuple<EdgeKey, Action<EdgeKey, double, double>>> _orderbookSampleRequestQueue;
        private Thread _messageThread;

        private bool edgesCreated = false;
        private HashSet<ExchangeName> RecievedEdgeInfos;
        private Dictionary<EdgeKey, Edge> edges;
        private Dictionary<string, List<EdgeKey>> edgesBySrcCurrency;
        private Dictionary<string, List<EdgeKey>> edgesByDstCurrency;
        private Dictionary<EdgeKey, HashSet<EdgeKey>> successors;
        private Dictionary<string, MarketCapItem> marketCap;
        private Dictionary<EdgeKey, EdgeInfo> edgeInfos;
        private Dictionary<EdgeKey, HashSet<Cycle>> availableCyclesByEdge;
        private HashSet<CycleDescription> availableCycles;

        private Dictionary<NodeKey, double> funds;
        private HashSet<NodeKey> fundSourceNodes;
        public double GetFund(NodeKey nodeKey) => funds[nodeKey];
        private Dictionary<EdgeKey, DateTime> lockedEdges;
        private Dictionary<Guid, CycleExecutionContainer> cyclesInExecution;

        public long MessageQueueCount { get { return _ordersUpdateQueue.Count; } }
        public int EdgeCount { get { return edges.Count; } }
        public int AvailableCyclesCount { get; private set; }
        public int AllCyclesCount { get { return availableCycles.Count; } }
        private double expectedCummulativeUdsProfit = 0;
        private double actualCummulativeUdsProfit = 0;
        public static long InternalCycleFilterCount = 0;
        public static long MarketCurrenciesCycleFilterCount = 0;
        public static long MinProfitFilterCount = 0;
        public static long OrderMessageDropCounter = 0;
        private Dictionary<EdgeKey, Tuple<DateTime, DateTime>> EdgesToSample = new Dictionary<EdgeKey, Tuple<DateTime, DateTime>>();

        private MarketMonitor()
        {
            RecievedEdgeInfos = new HashSet<ExchangeName>();
            edges = new Dictionary<EdgeKey, Edge>();
            edgesBySrcCurrency = new Dictionary<string, List<EdgeKey>>();
            edgesByDstCurrency = new Dictionary<string, List<EdgeKey>>();
            successors = new Dictionary<EdgeKey, HashSet<EdgeKey>>();
            marketCap = new Dictionary<string, MarketCapItem>();
            edgeInfos = new Dictionary<EdgeKey, EdgeInfo>();
            availableCyclesByEdge = new Dictionary<EdgeKey, HashSet<Cycle>>();
            availableCycles = new HashSet<CycleDescription>();

            funds = new Dictionary<NodeKey, double>();
            fundSourceNodes = new HashSet<NodeKey>();
            lockedEdges = new Dictionary<EdgeKey, DateTime>();
            cyclesInExecution = new Dictionary<Guid, CycleExecutionContainer>();

            _ordersUpdateBuffer = new Queue<OrdersUpdate>();
            _ordersUpdateQueue = new ConcurrentQueue<OrdersUpdate>();
            _exchangeFundsSnapshotQueue = new ConcurrentQueue<ExchangeFundsSnapshot>();
            _marketCapUpdateQueue = new ConcurrentQueue<MarketCapUpdate>();
            _edgeInfosUpdateQueue = new ConcurrentQueue<EdgeInfoUpdate>();
            _executionReportQueue = new ConcurrentQueue<ExecutionReport>();
            _orderbookSampleRequestQueue = new ConcurrentQueue<Tuple<EdgeKey, Action<EdgeKey, double, double>>>();
            _messageThread = new Thread(MessageLoop);
            _messageThread.Start();

            MessageBroker.ExecutionReportEvent += Update;
            MessageBroker.OrdersUpdateEvent += Update;
            MessageBroker.EdgeInfoUpdateEvent += Update;
            MessageBroker.ExchangeFundsSnapshotEvent += Update;
            MessageBroker.MarketCapUpdateEvent += Update;
        }

        private void MessageLoop()
        {
            CycleCache.Instance.LoadCycleCache();

            while (true)
            {

                if (_marketCapUpdateQueue.TryDequeue(out MarketCapUpdate marketCapMsg))
                    Update(marketCapMsg);
                else if (_exchangeFundsSnapshotQueue.TryDequeue(out ExchangeFundsSnapshot fundsMsg))
                    Update(fundsMsg);
                else if (_edgeInfosUpdateQueue.TryDequeue(out EdgeInfoUpdate edgeInfoMsg))
                    Update(edgeInfoMsg);
                else if(_executionReportQueue.TryDequeue(out ExecutionReport reportMsg))
                    Update(reportMsg);
                else if (_orderbookSampleRequestQueue.TryDequeue(out Tuple<EdgeKey, Action<EdgeKey, double, double>> sampleRequestMsg))
                    Update(sampleRequestMsg.Item1, sampleRequestMsg.Item2);
                else if (!_ordersUpdateQueue.IsEmpty)
                {
                    while (_ordersUpdateQueue.TryDequeue(out OrdersUpdate ordersUpdate))
                        _ordersUpdateBuffer.Enqueue(ordersUpdate);
                    HandleOrdersUpdate(_ordersUpdateBuffer);
                }
                else
                    Thread.Sleep(1);
            }
        }

        public static MarketMonitor GetInstance()
        {
            return _instance;
        }

        private void HandleOrdersUpdate(Queue<OrdersUpdate> ordersUpdatesQueue)
        {
            var dequeueTime = NtpAgent.GetAccurateCurrentUnixTime();
            var queueSizeBeforeUnpacking = ordersUpdatesQueue.Count;

            var testForCycles = new List<Tuple<Edge, OrdersUpdate>>();
            while (ordersUpdatesQueue.Count > 0)
            {
                OrdersUpdate ordersUpdate = ordersUpdatesQueue.Dequeue();
                ordersUpdate.Measurement.Add("algorithm queue");
                foreach (var edgeUpdate in ordersUpdate.Orders)
                {
                    var refRateUpdated = false;
                    if (!edges.TryGetValue(edgeUpdate.Item1, out var edge))
                    {
                        ConsoleWriter.WriteLine("Got order update for non existing edge: {0}", edgeUpdate.Item1);
                        continue;
                    }

                    if (ordersUpdate.OverrideOrderbooks)
                        refRateUpdated = edge.ReplaceOrderbook(edgeUpdate.Item2);
                    else
                        refRateUpdated = edge.UpdateOrderbook(edgeUpdate.Item2, ordersUpdate.IsDiffUpdate);

                    if (refRateUpdated)
                        testForCycles.Add(new Tuple<Edge, OrdersUpdate>(edge, ordersUpdate));

                    if (Config.SampleEdgesInExecution && EdgesToSample.ContainsKey(edgeUpdate.Item1))
                    {
                        var edgeSampleTimes = EdgesToSample[edgeUpdate.Item1];
                        if (DateTime.Now - edgeSampleTimes.Item2 < Config.SampleWindowDuration)
                        {
                            var suffix = String.Format("arrived after {0} ms", (ordersUpdate.Measurement.ArrivalTime - edgeSampleTimes.Item1).TotalMilliseconds);
                            PrintOrderbookTop(edge, suffix);
                        }
                        else
                            EdgesToSample.Remove(edgeUpdate.Item1);
                    }
                }
            }

            if (queueSizeBeforeUnpacking > Config.MaxQueueBeforeExecution)
            {
                Interlocked.Add(ref OrderMessageDropCounter, queueSizeBeforeUnpacking);
                return;
            }

            //TODO: processing potential cycles one order update at a time is a gridy solution that lock edges according to update order, 
            //  would be better to take all the the batch into consideration when picking which cycles to execute.
            foreach (var tc in testForCycles)
            {
                var cyclesFound = availableCyclesByEdge[tc.Item1.EdgeKey].Where(c => c.CalcCurrRate() > 1.0).ToList();
                if (cyclesFound.Count > 0)
                    ProcessPotentialCycles(cyclesFound, tc.Item1, tc.Item2.Measurement, dequeueTime);
            }
        }

        public void Update(OrdersUpdate ordersUpdate)
        {
            _ordersUpdateQueue.Enqueue(ordersUpdate);
        }
        public void Update(ExchangeFundsSnapshot exchangeFundsSnapshot)
        {
            if (!Thread.CurrentThread.Equals(_messageThread))
            {
                _exchangeFundsSnapshotQueue.Enqueue(exchangeFundsSnapshot);
                return;
            }

            if (Config.UseDummyFunds)
            {
                var initialFundsUsd = 30;
                if (false)
                {
                    var fundedCurrencies = new Dictionary<ExchangeName, List<string>>()
                    {
                        { ExchangeName.Binance, new List<string> { "BTC", "ETH", "USDT", "ONT", "BCH", "NEO", "ADA", "QSP", "EOS", "NAS" } },
                        { ExchangeName.HitBTC, new List<string> { "BTC", "ETH", "USDT", "ONT", "BCH", "NEO", "ADA", "QSP", "EOS", "NAS" } },
                        { ExchangeName.Huobi, new List<string> { "BTC", "ETH", "USDT", "ONT", "BCH", "NEO", "ADA", "QSP", "EOS", "NAS" } },
                        { ExchangeName.OKEX, new List<string> { "BTC", "ETH", "USDT", "ONT", "BCH", "NEO", "ADA", "QSP", "EOS", "NAS" } }
                    };
                    exchangeFundsSnapshot = new ExchangeFundsSnapshot(exchangeFundsSnapshot.Exchange,
                        fundedCurrencies[exchangeFundsSnapshot.Exchange].ToDictionary(c => c, c => initialFundsUsd / marketCap[c].PriceUsd));
                }
                else
                {
                    //exchangeFundsSnapshot = new ExchangeFundsSnapshot(exchangeFundsSnapshot.Exchange,
                    //    marketCap.Where(mc => mc.Value.Rank <= 10).ToDictionary(mc => mc.Key, mc => initialFundsUsd / mc.Value.PriceUsd));
                    exchangeFundsSnapshot = new ExchangeFundsSnapshot(exchangeFundsSnapshot.Exchange,
                        marketCap.ToDictionary(mc => mc.Key, mc => initialFundsUsd / mc.Value.PriceUsd));
                }
            }

            var toRemove = funds.Where(f => f.Key.Exchange.Equals(exchangeFundsSnapshot.Exchange) && !exchangeFundsSnapshot.CurrencyFunds.ContainsKey(f.Key.Currency)).ToList();
            ConsoleWriter.WriteLine(string.Format("Removing {0} funds and adding {1} funds for {2}",
                toRemove.Count, exchangeFundsSnapshot.CurrencyFunds.Count, exchangeFundsSnapshot.Exchange));
            foreach (var removed in toRemove)
            {
                RemoveFundedNode(removed.Key);
                funds.Remove(removed.Key);
            }
            foreach (var addedFunds in exchangeFundsSnapshot.CurrencyFunds)
            {
                var newNodeKey = new NodeKey(exchangeFundsSnapshot.Exchange, addedFunds.Key);
                funds[newNodeKey] = addedFunds.Value;
                if (edgesCreated)
                    CheckFundSourceChanged(newNodeKey);
            }
        }
        public void Update(MarketCapUpdate marketCapUpdate)
        {
            if (!Thread.CurrentThread.Equals(_messageThread))
            {
                _marketCapUpdateQueue.Enqueue(marketCapUpdate);
                return;
            }

            marketCap = new Dictionary<string, MarketCapItem>(marketCapUpdate.MarketCapItems);
            foreach (var mc in marketCapUpdate.MarketCapItems)
            {
                double newRefVolume = Config.RefVolumeUsd / mc.Value.PriceUsd;
                if (edgesBySrcCurrency.TryGetValue(mc.Key, out List<EdgeKey> edgesToUpdate))
                {
                    edgesToUpdate.ForEach(e =>
                    {
                        if (e.IsBidBook)
                            this.edges[e].UpdateRefVolume(newRefVolume);
                    });
                }
                if (edgesByDstCurrency.TryGetValue(mc.Key, out edgesToUpdate))
                {
                    edgesToUpdate.ForEach(e =>
                    {
                        if (!e.IsBidBook)
                            this.edges[e].UpdateRefVolume(newRefVolume);
                    });
                }
            }
        }
        public void Update(EdgeInfoUpdate edgeInfosUpdate)
        {
            if (!Thread.CurrentThread.Equals(_messageThread))
            {
                _edgeInfosUpdateQueue.Enqueue(edgeInfosUpdate);
                return;
            }

            foreach (var kvp in edgeInfosUpdate.EdgeInfos)
            {
                this.RecievedEdgeInfos.Add(kvp.Key.Exchange);
                this.edgeInfos[kvp.Key] = kvp.Value;
                if (edges.TryGetValue(kvp.Key, out Edge edge))
                    edge.UpdateEdgeInfo(kvp.Value);
            }

            if (!edgesCreated && (this.RecievedEdgeInfos.Count == Config.EnabledExchanges.Count))
                CreateAllEdges();
        }
        public void Update(ExecutionReport report)
        {
            if (!Thread.CurrentThread.Equals(_messageThread))
            {
                _executionReportQueue.Enqueue(report);
                return;
            }

            if (!cyclesInExecution.TryGetValue(report.ExecutionCycleId, out CycleExecutionContainer executionRecord))
            {
                //TODO: log error and maybe release resources
                return;
            }

            executionRecord.reports.Add(report);
            if (executionRecord.reports.Count < executionRecord.requests.Count)
                return;

            var actualProfit = new Dictionary<string, double>();
            var actualFundsDiff = new Dictionary<NodeKey, double>();
            double actualTotalUsdProfit = 0;

            cyclesInExecution.Remove(report.ExecutionCycleId);
            if (Config.SampleEdgesInExecution)
            {
                var reportTime = DateTime.Now;
                foreach (var edge in executionRecord.cycle.Edges)
                    EdgesToSample[edge.EdgeKey] = new Tuple<DateTime, DateTime>(EdgesToSample[edge.EdgeKey].Item1, reportTime);
            }

            DateTime releaseTime = DateTime.UtcNow + Config.CooldownPeriod;
            foreach (var e in executionRecord.cycle.Edges)
            {
                lockedEdges[e.EdgeKey] = releaseTime;
                actualFundsDiff[e.SrcNode] = 0;
                actualFundsDiff[e.DstNode] = 0;
                actualProfit[e.SrcCurrency] = 0;
                actualProfit[e.DstCurrency] = 0;
            }

            double actualFlowSize = 0;
            foreach (var r in executionRecord.reports)
            {
                if (!r.IsSuccessful)
                {
                    //TODO: log and decide what should be done
                }
                
                foreach (var er in r.ExecutionResult)
                {
                    actualProfit[er.Item1.SrcNode.Currency] -= er.Item2.SrcSize;
                    actualFundsDiff[er.Item1.SrcNode] -= er.Item2.SrcSize;
                    actualTotalUsdProfit -= er.Item2.SrcSize * marketCap[er.Item1.SrcNode.Currency].PriceUsd;
                    actualProfit[er.Item1.DstNode.Currency] += er.Item2.DstSize;
                    actualFundsDiff[er.Item1.DstNode] += er.Item2.DstSize;
                    actualTotalUsdProfit += er.Item2.DstSize * marketCap[er.Item1.DstNode.Currency].PriceUsd;
                    actualFlowSize = Math.Max(actualFlowSize, er.Item2.SrcSize * marketCap[er.Item1.SrcNode.Currency].PriceUsd);
                }
            }

            foreach (var fd in actualFundsDiff)
            {
                if (!executionRecord.lockedFunds.TryGetValue(fd.Key, out double lockedFunds))
                    lockedFunds = 0;

                if (funds.TryGetValue(fd.Key, out double currAvailableFunds))
                {
                    if (Config.NumericEpsilon < currAvailableFunds + fd.Value + lockedFunds)
                        funds[fd.Key] = currAvailableFunds + fd.Value + lockedFunds;
                    else
                        funds.Remove(fd.Key);
                }
                else if (Config.NumericEpsilon < fd.Value + lockedFunds)
                    funds[fd.Key] = fd.Value + lockedFunds;

                CheckFundSourceChanged(fd.Key);
            }

            var isExecutionSuccsessful = executionRecord.reports.All(r => r.IsSuccessful);
            if (isExecutionSuccsessful)
            {
                expectedCummulativeUdsProfit += executionRecord.cycle.ExpectedTotalUsdProfit;
                actualCummulativeUdsProfit += actualTotalUsdProfit;
            }
            PrintCycle(executionRecord.cycle, actualTotalUsdProfit, actualProfit, actualFundsDiff, actualFlowSize, isExecutionSuccsessful);
            //TODO: push execution details to db
        }
        public void Update(EdgeKey edgeKey, Action<EdgeKey, double, double> callback)
        {
            if (!Thread.CurrentThread.Equals(_messageThread))
            {
                _orderbookSampleRequestQueue.Enqueue(new Tuple<EdgeKey, Action<EdgeKey, double, double>>(edgeKey, callback));
                return;
            }

            if (edges.TryGetValue(edgeKey, out Edge edge))
            {
                var bestCandle = edge.GetBestCandle();
                callback(edgeKey, bestCandle.Key, bestCandle.Value);
            }
            else
            {
                callback(edgeKey, 0.0, 0.0);
            }
        }

        private void CreateAllEdges()
        {
            if (edgesCreated)
                return;

            var edgesToCreate = new HashSet<EdgeKey>(this.edgeInfos.Select(ei => ei.Key));
            var cachedEdges = CycleCache.Instance.GetCachedEdges();

            foreach (var cachedEdgeKey in cachedEdges)
            {
                if (!edgesToCreate.Contains(cachedEdgeKey))
                    continue;

                AddEdge(cachedEdgeKey);
                edgesToCreate.Remove(cachedEdgeKey);
            }
            foreach (var cachedEdgeKey in cachedEdges)
                AddCyclesForEdge(cachedEdgeKey);

            foreach (var newEdgeKey in edgesToCreate)
            {
                AddEdge(newEdgeKey);
                AddCyclesForEdge(newEdgeKey);
            }

            fundSourceNodes = new HashSet<NodeKey>(funds.Keys.Where(n => Config.MinFundSourceThresholdUsd < GetNodeFundsWorthUsd(n)));
            ConsoleWriter.WriteLine("Done loading edges");
            edgesCreated = true;
            Exchange.LoadWebsockets();
        }
        private void AddEdge(EdgeKey edgeKey)
        {
            if (edges.ContainsKey(edgeKey))
            {
                ConsoleWriter.WriteLine("Trying to add existing edge! {0}", edgeKey);
                return;
            }
            if (!marketCap.TryGetValue(edgeKey.BaseCurrency, out var baseSymbolMarketCap))
            {
                ConsoleWriter.WriteLine("Trying to create edge with no MarketCap: {0}", edgeKey.BaseCurrency);
                return;
            }
            if (!marketCap.TryGetValue(edgeKey.QuoteCurrency, out var quoteSymbolMarketCap))
            {
                ConsoleWriter.WriteLine("Trying to create edge with no MarketCap: {0}", edgeKey.QuoteCurrency);
                return;
            }
            if (!edgeInfos.TryGetValue(edgeKey, out var edgeInfo))
            {
                ConsoleWriter.WriteLine("Trying to create edge with no EdgeInfo: {0}", edgeKey);
                return;
            }

            double RefVolume = Config.RefVolumeUsd / baseSymbolMarketCap.PriceUsd;
            var edge = new Edge(edgeKey, RefVolume, edgeInfo);
            edges.Add(edge.EdgeKey, edge);
            availableCyclesByEdge.Add(edge.EdgeKey, new HashSet<Cycle>());

            if (!edgesBySrcCurrency.ContainsKey(edge.SrcCurrency))
                edgesBySrcCurrency[edge.SrcCurrency] = new List<EdgeKey>();
            if (!edgesBySrcCurrency.ContainsKey(edge.DstCurrency))
                edgesBySrcCurrency[edge.DstCurrency] = new List<EdgeKey>();
            if (!edgesByDstCurrency.ContainsKey(edge.SrcCurrency))
                edgesByDstCurrency[edge.SrcCurrency] = new List<EdgeKey>();
            if (!edgesByDstCurrency.ContainsKey(edge.DstCurrency))
                edgesByDstCurrency[edge.DstCurrency] = new List<EdgeKey>();

            edgesBySrcCurrency[edge.SrcCurrency].Add(edge.EdgeKey);
            edgesByDstCurrency[edge.DstCurrency].Add(edge.EdgeKey);
            successors[edge.EdgeKey] = new HashSet<EdgeKey>();

            foreach (var succCandidate in edgesBySrcCurrency[edge.DstCurrency])
            {
                if (edge.SrcNode.Equals(succCandidate.DstNode))
                    continue;

                if ((!edge.Exchange.Equals(succCandidate.Exchange)) && (GetNodeFundsWorthUsd(succCandidate.SrcNode) < Config.MinFundSourceThresholdUsd))
                    continue;

                successors[edge.EdgeKey].Add(succCandidate);
            }

            foreach (var predCandidat in edgesByDstCurrency[edge.SrcCurrency])
            {
                if (predCandidat.SrcNode.Equals(edge.DstNode))
                    continue;

                if ((!edge.Exchange.Equals(predCandidat.Exchange)) && (GetNodeFundsWorthUsd(edge.SrcNode) < Config.MinFundSourceThresholdUsd))
                    continue;

                successors[predCandidat].Add(edge.EdgeKey);
            }
        }
        private void CheckFundSourceChanged(NodeKey nodeKey)
        {
            var currentlyFundSourceNodes = fundSourceNodes.Contains(nodeKey);
            var nodeFundsUsd = GetNodeFundsWorthUsd(nodeKey);

            if (!currentlyFundSourceNodes && (Config.MinFundSourceThresholdUsd < nodeFundsUsd))
            {
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                AddFundedNode(nodeKey);
                ConsoleWriter.WriteLine("@@@@@@@@@@@ AddFundedNode for {0} took {1} ms @@@@@@@@@@@", nodeKey, sw.ElapsedMilliseconds);
            }
            else if (currentlyFundSourceNodes && (nodeFundsUsd < Config.MinFundSourceThresholdUsd))
            {
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                RemoveFundedNode(nodeKey);
                ConsoleWriter.WriteLine("@@@@@@@@@@@ RemoveFundedNode for {0} took {1} ms @@@@@@@@@@@", nodeKey, sw.ElapsedMilliseconds);
            }
        }
        private void AddFundedNode(NodeKey nodeKey)
        {
            fundSourceNodes.Add(nodeKey);

            if (!edgesBySrcCurrency.ContainsKey(nodeKey.Currency))
                edgesBySrcCurrency[nodeKey.Currency] = new List<EdgeKey>();
            if (!edgesByDstCurrency.ContainsKey(nodeKey.Currency))
                edgesByDstCurrency[nodeKey.Currency] = new List<EdgeKey>();

            var predEdges = edgesByDstCurrency[nodeKey.Currency];
            var succEdges = edgesBySrcCurrency[nodeKey.Currency].Where(e => e.Exchange.Equals(nodeKey.Exchange));

            foreach (var se in succEdges)
                foreach (var pe in predEdges)
                {
                    if (pe.SrcNode.Equals(se.DstNode))
                        continue;

                    successors[pe].Add(se);
                }

            foreach (var se in succEdges)
                AddCyclesForEdge(se);
        }
        private void RemoveFundedNode(NodeKey nodeKey)
        {
            fundSourceNodes.Remove(nodeKey);

            if (!edgesBySrcCurrency.ContainsKey(nodeKey.Currency))
                edgesBySrcCurrency[nodeKey.Currency] = new List<EdgeKey>();
            if (!edgesByDstCurrency.ContainsKey(nodeKey.Currency))
                edgesByDstCurrency[nodeKey.Currency] = new List<EdgeKey>();

            var succEdges = edgesBySrcCurrency[nodeKey.Currency].Where(e => e.Exchange.Equals(nodeKey.Exchange));
            var predEdges = edgesByDstCurrency[nodeKey.Currency].Where(e => !e.Exchange.Equals(nodeKey.Exchange));

            foreach (var se in succEdges)
                foreach (var pe in predEdges)
                    successors[pe].Remove(se);

            foreach (var se in succEdges)
            {
                var cyclesToRemove = availableCyclesByEdge[se].Where(c => !IsValidCycle(c)).ToList();
                AvailableCyclesCount -= cyclesToRemove.Count;

                foreach (var cycle in cyclesToRemove)
                {
                    availableCycles.Remove(cycle.Description);
                    foreach (var edgeInCycle in cycle.Edges)
                        availableCyclesByEdge[edgeInCycle.EdgeKey].Remove(cycle);
                }
            }

        }
        private void AddCyclesForEdge(EdgeKey edgeKey)
        {
            if (!CycleCache.Instance.TryGetCommonCycles(edgeKey, out List<CycleDescription> commonCycleDescriptions))
            {
                commonCycleDescriptions = FindCommonCycles(edgeKey);
                CycleCache.Instance.PushEdgeKey(edgeKey);
                CycleCache.Instance.PushCycles(commonCycleDescriptions);
            }

            var commonCycles = new List<Cycle>();
            foreach (var cd in commonCycleDescriptions)
            {
                if (availableCycles.Contains(cd))
                    continue;
                if (!IsValidCycle(cd))
                    continue;

                var edgeCount = cd.EdgeKeys.Length;
                var cycleEdgeArr = new Edge[edgeCount];
                var allEdgesExist = true;
                for (int i = 0; i < edgeCount; i++)
                {
                    allEdgesExist &= this.edges.TryGetValue(cd.EdgeKeys[i], out var edge);
                    cycleEdgeArr[i] = edge;
                }

                if (allEdgesExist)
                    commonCycles.Add(new Cycle(cycleEdgeArr, cd));
            }

            AvailableCyclesCount += commonCycles.Count;

            foreach (var cycle in commonCycles)
            {
                availableCycles.Add(cycle.Description);
                foreach (var edgeInCycle in cycle.Edges)
                    availableCyclesByEdge[edgeInCycle.EdgeKey].Add(cycle);
            }
        }

        private List<CycleDescription> FindCommonCycles(EdgeKey commonEdge)
        {
            return BFS(commonEdge, commonEdge, 0, Config.MaxEdgesInCycle) ?? new List<CycleDescription>();
        }
        private List<CycleDescription> BFS(EdgeKey currEdge, EdgeKey targetEdge, int currDepth, int maxDepth)
        {
            if ((currEdge == targetEdge) && (currDepth > 0))
                return new List<CycleDescription> { new CycleDescription(currDepth) };
            if (currDepth == maxDepth)
                return null;

            List<CycleDescription> cycles = null;
            foreach (var nextEdge in successors[currEdge])
            {
                //Filter for two independent cycles scenario as well as trivial two link loop. example:
                //1) BTC->(binance-ask)->ETH->(binance-bid)->BTC->(bitfinex-ask)->LRC->(bitfinex-bid)->BTC
                //2) BTC->(binance-ask)->ETH->(binance-bid)->BTC
                //if (crossingExchange && currEdge.SrcCurrency.Equals(nextEdge.DstCurrency))
                    //continue;

                var tmpCycles = BFS(nextEdge, targetEdge, currDepth + 1, maxDepth);
                if (tmpCycles == null)
                    continue;

                if (cycles == null)
                    cycles = new List<CycleDescription>();

                foreach (var tc in tmpCycles)
                {
                    tc.PushEdge(currEdge, currDepth);
                    cycles.Add(tc);
                }
            }

            return cycles;
        }
        private bool IsValidCycle(Cycle cycle)
        {
            return IsValidCycle(cycle.Edges.Select(e => e.EdgeKey).ToList());
        }
        private bool IsValidCycle(CycleDescription cycleDescription)
        {
            return IsValidCycle(cycleDescription.EdgeKeys);
        }
        private bool IsValidCycle(IList<EdgeKey> cycleEdgeKeys)
        {
            int edgeCount = cycleEdgeKeys.Count;
            EdgeKey currEdge, nextEdge;
            bool crossingExchange;
            HashSet<NodeKey> FundsSrc = new HashSet<NodeKey>();
            HashSet<NodeKey> FundsDst = new HashSet<NodeKey>();
            for (int i = 0; i < edgeCount; i++)
            {
                currEdge = cycleEdgeKeys[i];
                nextEdge = cycleEdgeKeys[(i + 1) % edgeCount];

                crossingExchange = nextEdge.Exchange != currEdge.Exchange;
                if (!crossingExchange && currEdge.SrcCurrency.Equals(nextEdge.DstCurrency))
                    return false;
                if (!nextEdge.IsBidBook && !funds.ContainsKey(nextEdge.SrcNode))
                    return false; //TODO: should we keep that?
                if (crossingExchange && (GetNodeFundsWorthUsd(nextEdge.SrcNode) < Config.MinFundSourceThresholdUsd))
                    return false;
                if (Config.RestrictAskReplyToFinalCmd && !Config.ExchangesWithMarketCmd.Contains(currEdge.Exchange) && !crossingExchange && !currEdge.IsBidBook)
                    return false;

                if (crossingExchange)
                {
                    //In order to avoid cycles like:
                    //ENJ-->(Binance-Bid)-->ETH-->(OKEX-Ask)-->ENJ-->(Binance-Bid)-->BTC-->(OKEX-Ask)-->ENJ
                    if (FundsSrc.Contains(nextEdge.SrcNode))
                        return false;

                    FundsSrc.Add(nextEdge.SrcNode);
                    FundsDst.Add(currEdge.DstNode);
                }
            }

            if (cycleEdgeKeys.Distinct().Count() < edgeCount)
                return false;

            if (Config.FilterByAllowedCrossNodes && !Config.AllowedCrossNodesGroups.Contains(FundsSrc) && !Config.AllowedCrossNodesGroups.Contains(FundsDst))
                return false;

            if (Config.FilterByRequiredCrossNodes && !Config.RequiredCrossNodesGroups.Intersect(FundsSrc).Any() && !Config.RequiredCrossNodesGroups.Intersect(FundsDst).Any())
                return false;

            return true;
        }


        private void ProcessPotentialCycles(IEnumerable<Cycle> potentialCycles, Edge triggeringEdge, 
            DelayMeasurement updateMeasurement, DateTime dequeueTime)
        {
            if (!PickBestCycle(potentialCycles, out Cycle bestCycle, out Dictionary<NodeKey, double> fundsToLock))
                return;

            //TODO: It is possible that there are funds locked by different cycles (not currently present at 'funds' dict) 
            //      that would allow an edge to be StandaloneFundedSrc=True though this test shows it isn't. deal with it.
            var standaloneFundedSrcNodes = bestCycle.Edges
                .Select((e, i) => funds.TryGetValue(e.SrcNode, out var nodeFunds) && bestCycle.AdjustedMaxFlow[i].SrcSize < nodeFunds)
                .ToArray();

            foreach (var edge in bestCycle.Edges)
            {
                lockedEdges[edge.EdgeKey] = DateTime.MaxValue;
            }
            foreach (var kvp in fundsToLock)
            {
                funds[kvp.Key] -= kvp.Value;
            }
            if (Config.SampleEdgesInExecution)
            {
                foreach (var edge in bestCycle.Edges)
                {
                    EdgesToSample[edge.EdgeKey] = new Tuple<DateTime, DateTime>(updateMeasurement.ArrivalTime, DateTime.MaxValue);
                }
            }

            var requests = BuildExecutionRequests(bestCycle, standaloneFundedSrcNodes, updateMeasurement, out Guid executionCycleId);
            cyclesInExecution.Add(executionCycleId, new CycleExecutionContainer(bestCycle, fundsToLock, requests));

            ConsoleWriter.WriteLine("---------------- Cycle sent for execution - {0} ---------------", DateTime.Now);
            ConsoleWriter.WriteLine(bestCycle);
            ConsoleWriter.WriteLine("Triggering edge - {0}", triggeringEdge);
            if (Config.SampleEdgesInExecution)
            {
                foreach (var edge in bestCycle.Edges)
                    PrintOrderbookTop(edge);
            }

            updateMeasurement.Add("algorithm");
            if (Config.SendExecutionRequests)
            {
                Exchange.HandleExecutionRequests(requests);
            }
            else
            {
                new Thread(() =>
                {
                    Thread.Sleep(1000);
                    foreach (var r in requests)
                    {
                        ConsoleWriter.WriteLine("{0}", r.TriggerUpdateMeasurement);
                        var executionResult = r.Commands.Select(ec => new Tuple<EdgeKey, RateItem>(ec.Edge, new RateItem(ec.AdjustedRateItem.SrcSize, ec.AdjustedRateItem.DstSize))).ToList();
                        Update(new ExecutionReport(r.ExecutionCycleId, r.Exchange, executionResult, true));
                    }

                }).Start();
            }
        }
        private bool PickBestCycle(IEnumerable<Cycle> potentialCycles, out Cycle bestCycle, out Dictionary<NodeKey, double> fundsToLock)
        {
            fundsToLock = null;
            bestCycle = null;
            int minEdgeCount = Config.MaxEdgesInCycle;
            double maxProfit = Config.MinUsdProfitForExecution;
            bool result = false;
            bool maxProfitLimit = false;

            if (!Config.ExecuteInternalCycleGroups && potentialCycles.Any(c => !c.IsCrossingCycle))
            {
                Interlocked.Increment(ref InternalCycleFilterCount);
                return false;
            }

            if (!Config.ExecuteMarketCurrenciesCycleGroups && potentialCycles.Any(c => c.IsMarketCurrenciesCycle))
            {
                Interlocked.Increment(ref MarketCurrenciesCycleFilterCount);
                return false;
            }

            foreach (var cycle in potentialCycles.OrderByDescending(c => c.ApproximateRate).Take(Config.MaxFlowCalculationLimit))
            {
                if (!cycle.Edges.All(e => !lockedEdges.TryGetValue(e.EdgeKey, out DateTime releaseTime) || releaseTime < DateTime.UtcNow))
                    continue;

                if (!cycle.CalcMaxFlow(funds, marketCap, out Dictionary<NodeKey, double> currFundsToLock) || (cycle.MaxFlowRate < Config.MinRateForExecution))
                    continue;

                if ((Config.PreferShorterCycles && (cycle.EdgeCount < minEdgeCount) && (Config.MinUsdProfitForExecution < cycle.ExpectedTotalUsdProfit)) || 
                    (((cycle.EdgeCount == minEdgeCount) || !Config.PreferShorterCycles) && (maxProfit < cycle.ExpectedTotalUsdProfit)))
                {
                    minEdgeCount = cycle.EdgeCount;
                    maxProfit = cycle.ExpectedTotalUsdProfit;
                    bestCycle = cycle;
                    fundsToLock = currFundsToLock;
                    result = true;
                }
                else
                    maxProfitLimit = true;
            }

            if (!result && maxProfitLimit)
                Interlocked.Increment(ref MinProfitFilterCount);

            return result;
        }
        private List<ExecutionRequest> BuildExecutionRequests(Cycle cycle, bool[] standaloneFundedSrcNodes, 
            DelayMeasurement triggerUpdateMeasurement, out Guid executionCycleId)
        {
			executionCycleId = Guid.NewGuid();
            int edgesAdded = 0;
            int edgeCount = cycle.EdgeCount;


			int i = 0, j = 1;
			Edge currEdge;
            ExecutionRequest currRequest;
            
            if (!cycle.IsCrossingCycle)
            {
				var requestsList = new List<ExecutionRequest>();
				currRequest = new ExecutionRequest(executionCycleId, cycle.Edges[0].EdgeKey.Exchange, triggerUpdateMeasurement);
				requestsList.Add(currRequest);

				for (i = 0; i < edgeCount; i++)
				{
					j = (cycle.MostFundedNodeIdx + i) % edgeCount;
					currEdge = cycle.Edges[j];
					currRequest.Commands.Add(new ExecutionCommand(currEdge.EdgeKey, cycle.AdjustedMaxFlow[j], cycle.MaxFlow[j], 
                        standaloneFundedSrcNodes[j]));
				}

				return requestsList;
            }
   
			var requestsDict = new Dictionary<ExchangeName, ExecutionRequest>();
            while (edgesAdded < edgeCount)
            {
                currEdge = cycle.Edges[j];

                if (requestsDict.ContainsKey(currEdge.EdgeKey.Exchange))
                {
                    currRequest = requestsDict[currEdge.EdgeKey.Exchange];
                    currRequest.Commands.Add(new ExecutionCommand(currEdge.EdgeKey, cycle.AdjustedMaxFlow[j], cycle.MaxFlow[j], standaloneFundedSrcNodes[j]));
                    edgesAdded++;
                }
                else if (!cycle.Edges[i].EdgeKey.Exchange.Equals(currEdge.EdgeKey.Exchange))
                {
                    currRequest = new ExecutionRequest(executionCycleId, currEdge.EdgeKey.Exchange, triggerUpdateMeasurement);
                    requestsDict.Add(currEdge.EdgeKey.Exchange, currRequest);
                    currRequest.Commands.Add(new ExecutionCommand(currEdge.EdgeKey, cycle.AdjustedMaxFlow[j], cycle.MaxFlow[j], standaloneFundedSrcNodes[j]));
                    edgesAdded++;
                }

                i = (i + 1) % edgeCount;
                j = (i + 1) % edgeCount;
            }

            return requestsDict.Values.ToList();
        }

        private double CalcTotalFundsWorthUsd()
        {
            double totalFundsWorthUsd = 0.0;
            foreach (var f in funds)
            {
                totalFundsWorthUsd += f.Value * marketCap[f.Key.Currency].PriceUsd;
            }
            return totalFundsWorthUsd;
        }
        private double GetNodeFundsWorthUsd(NodeKey nodeKey)
        {
            double totalFundsWorthUsd = 0.0;
            if (funds.TryGetValue(nodeKey, out var nodeFunds))
                totalFundsWorthUsd = nodeFunds * marketCap[nodeKey.Currency].PriceUsd;

            return totalFundsWorthUsd;
        }

        private void PrintCycle(Cycle cycle, double actualTotalUsdProfit, Dictionary<string, double> actualProfit, 
            Dictionary<NodeKey, double> actualFundsDiff, double actualFlowSize, bool isExecutionSuccsessful)
        {
            //var expectedFlowSize = cycle.MaxFlow.Select((mf, i) => Math.Abs(mf.SrcSize * marketCap[cycle.Edges[i].SrcCurrency].PriceUsd)).Max();
            var calculatedFlowSize = cycle.ExpectedTotalUsdProfit / (cycle.MaxFlowRate - 1);

            ConsoleWriter.WriteLine("---------------- Cycle Executed - {0} ----------------", DateTime.Now);
            ConsoleWriter.WriteLine(cycle);

            ConsoleWriter.WriteLine("Expected total profit: {0} Actual total profit: {1} USD with gain {2}%",
                Math.Round(cycle.ExpectedTotalUsdProfit, 4),
                Math.Round(actualTotalUsdProfit, 4),
                Math.Round((cycle.MaxFlowRate - 1) * 100, 4));

            ConsoleWriter.WriteLine("Expected flow size {0:F3} USD, Actual flow size {1:F3}, Calc flow size {2:F3}", cycle.ExpectedFlowSizeUsd, actualFlowSize, calculatedFlowSize);
            ConsoleWriter.WriteLine("Cummulative expected profit {0:F3} USD", expectedCummulativeUdsProfit);
            ConsoleWriter.WriteLine("Cummulative actual profit {0:F3} USD", actualCummulativeUdsProfit);
            ConsoleWriter.WriteLine("Total funds worth {0:F3} USD, with {1} funded nodes", CalcTotalFundsWorthUsd(), funds.Count);
            ConsoleWriter.WriteLine("Largest min size {0} USD", cycle.LargestMinSizeUsd);
            ConsoleWriter.WriteLine("Execution succsessful {0}", isExecutionSuccsessful);
            
            ConsoleWriter.WriteLine("Expected profit: ");
            foreach (var ep in cycle.ExpectedProfit)
            {
                ConsoleWriter.WriteLine("{0} Profit:\tExpected {1},\tActual {2}", 
                                  ep.Key, 
                                  Math.Round(ep.Value, 7), 
                                  Math.Round(actualProfit[ep.Key], 7));
            }

            ConsoleWriter.WriteLine("Funds diff: ");
            foreach (var ep in cycle.ExpectedFundsDiff)
            {
                ConsoleWriter.WriteLine("{0} Funds diff:\tExpected {1},\tActual {2}",
                                  ep.Key, Math.Round(ep.Value, 7), Math.Round(actualFundsDiff[ep.Key], 7));
            }

            ConsoleWriter.WriteLine("Expected flow size: ");
            for (int i = 0; i < cycle.EdgeCount; i++)
            {
                var flowCurrency = cycle.Edges[i].SrcCurrency;
                ConsoleWriter.WriteLine("{0} Flow size:\tExpected {1},\tUSD worth {2}",
                                  flowCurrency, 
                                  Math.Round(cycle.MaxFlow[i].SrcSize, 7), 
                                  Math.Round(cycle.MaxFlow[i].SrcSize * marketCap[flowCurrency].PriceUsd, 7));
            }

            ConsoleWriter.Write("Candles consumed: ");
            foreach (var cc in cycle.candlesConsumed)
            {
                ConsoleWriter.Write("{0}, ", cc);
            }
            ConsoleWriter.WriteLine();

            ConsoleWriter.WriteLine("Funds:");
            foreach (var f in funds)
            {
                var usdWorth = f.Value * marketCap[f.Key.Currency].PriceUsd;
                if (usdWorth > 0.5)
                    ConsoleWriter.WriteLine("{0}: {1},\tUSD worth {2}", f.Key, f.Value, usdWorth);
            }
        }

        private void PrintOrderbookTop(Edge edge, string suffix = "")
        {
            ConsoleWriter.Write("{0}:\t", edge);
            var orderbookEnumerator = edge.GetOrderbookEnumerator();
            for (int i = 0; i < Config.NumberOfPricesToSample; i++)
            {
                if (!orderbookEnumerator.MoveNext())
                    break;
                ConsoleWriter.Write("({0}, {1}),\t", orderbookEnumerator.Current.Key, orderbookEnumerator.Current.Value);
            }
            ConsoleWriter.WriteLine(suffix);
        }
    }
}
