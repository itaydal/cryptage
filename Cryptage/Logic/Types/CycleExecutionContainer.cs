﻿using System;
using System.Collections.Generic;
using System.Text;
using Common.Types;

namespace Logic.Types
{
    class CycleExecutionContainer
    {
        public Cycle cycle;
        public Dictionary<NodeKey, double> lockedFunds;
        public List<ExecutionRequest> requests;
        public List<ExecutionReport> reports;

        public CycleExecutionContainer(Cycle cycle, Dictionary<NodeKey, double> lockedFunds, List<ExecutionRequest> requests)
        {
            this.cycle = cycle;
            this.lockedFunds = lockedFunds;
            this.requests = requests;
            this.reports = new List<ExecutionReport>();
        }
    }
}
