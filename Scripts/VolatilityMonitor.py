#import matplotlib
#matplotlib.use('GTKAgg')
import pandas as pd
import numpy as np
import ccxt
import requests
import datetime


vol_time_scale = pd.Timedelta('365 days')


def calc_parkinson_vol(ohlcv_df):
    dt = np.append(np.nan, np.diff(ohlcv_df.index) / (np.timedelta64(1, 's') * vol_time_scale.total_seconds()))
    ret = np.log(ohlcv_df.high / ohlcv_df.low)
    vol = np.sqrt(np.power(ret, 2) / dt / (4 * np.log(2)))
    return vol


def fetch_ohlcv_df_hitbtc(symbol):
    uri = 'https://api.hitbtc.com/api/2/public/candles/{}'.format(symbol.replace('/', ''))
    params = {'params': {'limit': 600, 'period': 'M1'}}
    response = requests.session().get(uri, **params)
    if response.status_code != 200:
        return None

    ohlcv = response.json()
    if len(ohlcv) == 0:
        return None

    ohlcv_df = pd.DataFrame(ohlcv, dtype=np.float)
    ohlcv_df.rename(index=str, copy=False, inplace=True, columns={'max': 'high', 'min': 'low', 'timestamp': 'time'})
    ohlcv_df.drop('volumeQuote', axis=1, inplace=True)
    ohlcv_df.time = pd.to_datetime(ohlcv_df.time)
    ohlcv_df = ohlcv_df[['time', 'open', 'high', 'low', 'close', 'volume']]
    ohlcv_df.set_index('time', inplace=True)

    ohlcv_df = ohlcv_df.asfreq('1T')
    closes = ohlcv_df.close.fillna(method='pad')
    ohlcv_df = ohlcv_df.apply(lambda x: x.fillna(closes))
    ohlcv_df = ohlcv_df.iloc[-600:]

    return ohlcv_df


def fetch_ohlcv_df(exchange, symbol):

    if exchange.name.lower() == 'hitbtc':
        ohlcv_df = fetch_ohlcv_df_hitbtc(symbol)
    else:
        try:
            ohlcv = exchange.fetch_ohlcv(symbol, '1m', limit=600)
            ohlcv_df = pd.DataFrame(ohlcv, columns=['time', 'open', 'high', 'low', 'close', 'volume'])
            ohlcv_df.time = pd.to_datetime(ohlcv_df.time, unit='ms')
            ohlcv_df.set_index('time', inplace=True)
        except:
            ohlcv_df = None

    if ohlcv_df is None:
        print 'Cant fetch OHLC in {} for {}'.format(exchange.name, symbol)
        return None

    ohlcv_df['volatility'] = calc_parkinson_vol(ohlcv_df)
    ohlcv_df['volatility_mw'] = ohlcv_df.volatility.rolling(window=20, min_periods=10).mean()
    return ohlcv_df


def get_all_symbols_volatility_df(exchange, since=pd.datetime.min):
    exchange.load_markets()
    dfs = []
    fetched_symbols = []
    for symbol in exchange.symbols:
        if not exchange.markets[symbol]['active']:
            print '{}: {} inactive, skipping.'.format(exchange.name, symbol)
            continue

        print '{}: {}'.format(exchange.name, symbol)
        ohlc_df = fetch_ohlcv_df(exchange, symbol)
        if ohlc_df is not None:
            dfs.append(ohlc_df)
            fetched_symbols.append(symbol)

    vol_df = pd.concat([df.volatility_mw for df in dfs], axis=1)
    vol_df.columns = fetched_symbols
    return vol_df.loc[since:]


def get_mean_vol_df(exchange, since=pd.datetime.min):
    vol_df = get_all_symbols_volatility_df(exchange, since)
    dfs = []
    col_names = []
    base_currencies = set([symbol.split('/')[0] for symbol in vol_df])
    for base in base_currencies:
        dfs.append(vol_df.iloc[:, vol_df.columns.str.startswith('{}/'.format(base))].mean(axis=1))
        col_names.append('{}-{}'.format(base, exchange.name))

    mean_vol_df = pd.concat(dfs, axis=1)
    mean_vol_df.columns = col_names
    return mean_vol_df


exchanges = {'hitbtc': ccxt.hitbtc(),
             'binance': ccxt.binance(),
             'huobi': ccxt.huobipro(),
             'okex': ccxt.okex()}
exchanges['okex'].options['warnOnFetchOHLCVLimitArgument'] = False


since = pd.datetime.utcnow() - pd.Timedelta('10h')
mean_vol_dfs = [get_mean_vol_df(exchange, since) for exchange in exchanges.values()]
all_mean_vol_df = pd.concat(mean_vol_dfs, axis=1)
base_currencies = set([symbol.split('-')[0] for symbol in all_mean_vol_df.columns])
min_vol = []
filtered_base_currencies = []

for base in base_currencies:
    base_cols = all_mean_vol_df.columns.str.startswith('{}-'.format(base))
    if base_cols.sum() < 3:
        continue
    min_vol.append(all_mean_vol_df.iloc[:, base_cols].mean(axis=1))
    filtered_base_currencies.append(base)

min_vol_df = pd.concat(min_vol, axis=1)
min_vol_df.columns = filtered_base_currencies

min_vol_df.iloc[:, min_vol_df.fillna(method='ffill').iloc[-1].values > 0.5].plot()
min_vol_df.iloc[:, min_vol_df.fillna(method='ffill').mean(axis=0).values > 0.5].plot()


