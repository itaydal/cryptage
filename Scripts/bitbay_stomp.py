import stomp
import select
import socket
import threading
from websocket import create_connection


def broker(ws):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("127.0.0.1", 11111))
    s.listen(1)
    client, address = s.accept()
    while True:
        r, _, _ = select.select([client, ws.sock], [], [], 0.1)
        if client in r:
            data = client.recv(0xFFFF)
            print "CLIENT->SERVER: " + data
            print "CLIENT->SERVER: " + data.encode("hex")
            ws.send(data)
        if ws.sock in r:  
            data = ws.recv()
            print "SERVER->CLIENT: " + data
            print "SERVER->CLIENT: " + data.encode("hex")
            client.send(data)

ws = create_connection("wss://api.bitbay.net/bitbay-websocket/websocket")
t = threading.Thread(target=broker, args=(ws,))
t.start()
t.join()

            
