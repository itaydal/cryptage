import requests
import json


def binance_parser(data):
    def converter(currency):
        conv = {"BCC": "BCH", "IOTA": "MIOTA", "YOYO": "YOYOW", "BQX": "ETHOS"}
        if currency in conv.keys():
            return conv[currency]
        else:
            return currency

    result = {}
    data = json.loads(data)
    for item in data:
        result[converter(item["assetCode"])] = item["assetName"].lower().replace(" ", "").replace("-", "")
    return result


def hitbtc_parser(data):
    def converter(currency):
        conv = {"BQX": "ETHOS", "EMGO": "MGO", "IOTA": "MIOTA", "USD": "USDT"}
        if currency in conv.keys():
            return conv[currency]
        else:
            return currency

    result = {}
    data = json.loads(data)
    for item in data:
        result[converter(item["id"])] = item["fullName"].lower().replace(" ", "").replace("-", "")
    return result


def coinmarketcap_parser(data):
    def ignore(_id):
        f = ["bitgem", "icoin", "uncoin", "get-protocol", "sovereign-hero", "comet", "betacoin", "blazecoin",
             "blockcat", "bitmark", "blocklancer", "kingn-coin", "rcoin", "netcoin", "catcoin"]
        return _id in f

    result = {}
    data = json.loads(data)
    for item in data:
        _id = item["id"]
        if ignore(_id):
            continue
        result[item["symbol"]] = _id.lower().replace(" ", "").replace("-", "")
    return result


exchange_info = {
    "Binance": {
        "url": "https://www.binance.com/assetWithdraw/getAllAsset.html",
        "parser": binance_parser
    },
    "HitBTC": {
        "url": "https://api.hitbtc.com/api/2/public/currency",
        "parser": hitbtc_parser
    },
    "CoinMarketCap": {
        "url": "https://api.coinmarketcap.com/v1/ticker/?start=0&limit=5000",
        "parser": coinmarketcap_parser
    }
}

reference = "CoinMarketCap"
currency_info = {}

for exchange, info in exchange_info.items():
    response = requests.request(method='GET', url=info["url"])
    for currency, currency_name in info["parser"](response.content).items():
        if currency not in currency_info.keys():
            currency_info[currency] = {exchange: currency_name}
        else:
            currency_info[currency][exchange] = currency_name

non_consistent = {}
for currency, names in currency_info.items():
    if len(names) < 2:
        continue

    non_consistent[currency] = {}

    for e1, n1 in names.items():
        for e2, n2 in names.items():
            if (e1 == e2) or (e1 != reference and e2 != reference):
                continue
            if (n1 not in n2) and (n2 not in n1):
                non_consistent[currency].update({e1: n1, e2: n2})


for currency, names in non_consistent.items():
    if not names:
        continue

    print "%s: %s" % (currency, names)

print
reference_currencies = {k: v[reference] if reference in v.keys() else "" for k, v in currency_info.items()}

for exchange in filter(lambda e: e != reference, exchange_info.keys()):
    for currency, names in filter(lambda (c, n): exchange in n.keys(), non_consistent.items()):
        name = names[exchange]
        for c, n in reference_currencies.items():
            if not n or c == currency:
                continue

            if (n in name) or (name in n):
                print "%s: suggestion for %s (%s): %s (%s)" % (exchange, currency, name, c, n)