import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import re


def edge_contains(df, pat):
    return df.edge1.str.contains(pat, regex=False) | df.edge2.str.contains(pat, regex=False) | \
           df.edge3.str.contains(pat, regex=False) | df.edge4.str.contains(pat, regex=False)


def fund_src_node_contains(df, pat):
    return df.fund_src_node1.str.contains(pat, regex=False) | df.fund_src_node2.str.contains(pat, regex=False) | \
           df.fund_src_node3.str.contains(pat, regex=False) | df.fund_src_node4.str.contains(pat, regex=False)


def fund_dst_node_contains(df, pat):
    return df.fund_dst_node1.str.contains(pat, regex=False) | df.fund_dst_node2.str.contains(pat, regex=False) | \
           df.fund_dst_node3.str.contains(pat, regex=False) | df.fund_dst_node4.str.contains(pat, regex=False)


def parse_edge(edge_str):
    #example: 'USDT->TRX Ask book (Binance) '
    src_curr = re.findall('\w*-', edge_str)[0][:-1]
    dst_curr = re.findall('>\w* ', edge_str)[0][1:-1]
    exchange = re.findall('\(\w*\)', edge_str)[0][1:-1]
    return src_curr, dst_curr, exchange


def get_edges(row):
    edges = []
    if row['edge1'] != 'None':
        edges.append(parse_edge(row['edge1']))
    if row['edge2'] != 'None':
        edges.append(parse_edge(row['edge2']))
    if row['edge3'] != 'None':
        edges.append(parse_edge(row['edge3']))
    if row['edge4'] != 'None':
        edges.append(parse_edge(row['edge4']))
    return edges


def get_funds_flow_stats(df):
    src_nodes = pd.value_counts(list(df.fund_src_node1) + list(df.fund_src_node2) + list(df.fund_src_node3) + list(df.fund_src_node4))
    src_nodes.name = 'src_nodes'
    dst_nodes = pd.value_counts(list(df.fund_dst_node1) + list(df.fund_dst_node2) + list(df.fund_dst_node3) + list(df.fund_dst_node4))
    dst_nodes.name = 'dst_nodes'
    funds_flow_stats = pd.concat([src_nodes, dst_nodes], axis=1).fillna(0)
    funds_flow_stats['rel_diff'] = np.abs((funds_flow_stats.src_nodes - funds_flow_stats.dst_nodes) / (funds_flow_stats.src_nodes + funds_flow_stats.dst_nodes))
    funds_flow_stats['limiting_count'] = np.minimum(funds_flow_stats.src_nodes, funds_flow_stats.dst_nodes)
    funds_flow_stats['limiting_count_rel'] = funds_flow_stats.limiting_count / funds_flow_stats.limiting_count.sum()
    funds_flow_stats['total_count'] = funds_flow_stats.src_nodes + funds_flow_stats.dst_nodes
    funds_flow_stats['total_count_rel'] = funds_flow_stats.total_count / funds_flow_stats.total_count.sum()
    return funds_flow_stats[funds_flow_stats.index != "None"]


def refine_extreme_nodes(df, funds_flow_stats, max_rel_diff=0.95):
    all_nodes = set(df.fund_src_node1).union(set(df.fund_src_node2)).union(set(df.fund_src_node3)).union(set(df.fund_src_node4)) \
                    .union(set(df.fund_dst_node1)).union(set(df.fund_dst_node2)).union(set(df.fund_dst_node3)).union(set(df.fund_dst_node4)) - {'None'}
    allowed_nodes = set(funds_flow_stats.index[funds_flow_stats.rel_diff < max_rel_diff])
    not_allowed_nodes = all_nodes - allowed_nodes

    not_allowed_rows = np.zeros(shape=df.shape[0], dtype=np.bool)
    for n in not_allowed_nodes:
        print 'removed node {}'.format(n)
        not_allowed_rows |= fund_src_node_contains(df, n) | fund_dst_node_contains(df, n)

    return df[~not_allowed_rows]


def refine_to_allowed_nodes(df, allowed_nodes):
    all_nodes_list = list(df.fund_src_node1) + list(df.fund_src_node2) + list(df.fund_src_node3) + list(df.fund_src_node4) + \
                     list(df.fund_dst_node1) + list(df.fund_dst_node2) + list(df.fund_dst_node3) + list(df.fund_dst_node4)
    all_nodes_vc = pd.value_counts(all_nodes_list)

    all_nodes = set(all_nodes_list) - {'None'}
    not_allowed_nodes = all_nodes - allowed_nodes

    available_df = df
    for n in list(all_nodes_vc[list(not_allowed_nodes)].sort_values(ascending=False).index):
        print n
        available_df = available_df[~fund_src_node_contains(available_df, n)]
        available_df = available_df[~fund_dst_node_contains(available_df, n)]


def get_lifetime_median_per_node(df):
    all_nodes = set(df.fund_src_node1).union(set(df.fund_src_node2)).union(set(df.fund_src_node3)).union(set(df.fund_src_node4)) \
                        .union(set(df.fund_dst_node1)).union(set(df.fund_dst_node2)).union(set(df.fund_dst_node3)).union(set(df.fund_dst_node4)) - {'None'}

    res = []
    for n in all_nodes:
        print n
        res.append((n + ' src', df[fund_src_node_contains(df, n)].lifetime_ms.median()))
        res.append((n + ' dst', df[fund_dst_node_contains(df, n)].lifetime_ms.median()))

    return pd.DataFrame(res, columns=['node', 'lifetime_median'])


def percentile(q):
    f = lambda x: np.percentile(x, q=q)
    f.__name__ = 'percentile_{}'.format(q)
    return f


def get_cross_node_group_stat(df):
    df.loc[:, 'src_nodes'] = df[['fund_src_node1', 'fund_src_node2', 'fund_src_node3', 'fund_src_node4']].apply(lambda row: frozenset(set(row) - {'None'}), axis=1)
    df.loc[:, 'dst_nodes'] = df[['fund_dst_node1', 'fund_dst_node2', 'fund_dst_node3', 'fund_dst_node4']].apply(lambda row: frozenset(set(row) - {'None'}), axis=1)

    agg_funcs = {'cycle_desc': ['count'], 'lifetime_ms': [percentile(25), 'median']}
    src_nodes_agg = df.groupby('src_nodes').agg(agg_funcs)
    dst_nodes_agg = df.groupby('dst_nodes').agg(agg_funcs)

    nodes_agg = src_nodes_agg.join(dst_nodes_agg, lsuffix='_src', rsuffix='_dst', how='inner')
    nodes_agg['rel_diff'] = np.abs((nodes_agg[('cycle_desc_dst', 'count')] - nodes_agg[('cycle_desc_src', 'count')]) /
                                   (nodes_agg[('cycle_desc_dst', 'count')] + nodes_agg[('cycle_desc_src', 'count')]))
    nodes_agg['limiting_count'] = np.minimum(nodes_agg[('cycle_desc_dst', 'count')], nodes_agg[('cycle_desc_src', 'count')])
    nodes_agg['prec_25_min'] = np.minimum(nodes_agg[('lifetime_ms_dst', 'percentile_25')], nodes_agg[('lifetime_ms_src', 'percentile_25')])
    nodes_agg['median_min'] = np.minimum(nodes_agg[('lifetime_ms_dst', 'median')], nodes_agg[('lifetime_ms_src', 'median')])

    return nodes_agg


def plot_lifetime_ms_rolling():
    df_uni = df.groupby('cycle_group').first()
    df_uni.time = pd.to_datetime(df_uni.time)
    df_uni = df_uni.set_index('time', drop=True)
    df_uni.sort_index(inplace=True)
    import matplotlib.pyplot as plt
    plt.ion()
    df_uni.lifetime_ms.rolling('60s').mean().plot()


df = pd.read_csv(os.path.expanduser('~/data/cryptage/CycleData.csv'), index_col=False,
                 names=['time', 'cycle_desc', 'rate_change_ms', 'lifetime_ms', 'approximate_rate',
                        'max_flow_rate', 'adjusted_flow_rate',
                        'expected_flow_size_usd', 'expected_profit_usd', 'largest_min_size_usd',
                        'cycle_group', 'edge_count', 'starting_edge', 'ending_edge',
                        'edge1', 'edge2', 'edge3', 'edge4',
                        'funded_node1', 'funded_node2', 'funded_node3', 'funded_node4',
                        'fund_src_node1', 'fund_src_node2', 'fund_src_node3', 'fund_src_node4',
                        'fund_dst_node1', 'fund_dst_node2', 'fund_dst_node3', 'fund_dst_node4',
                        'queue_count'])

#df = df.iloc[10000:-10000]  # for 10/07 (2)
df = df[~df.cycle_desc.str.contains('HOT|BCD|BCX|SBTC')]
print 'df loaded'

big_coins = ['BTC', 'ETH', 'USDT']
#df_uni = df[~df.fund_src_node1.str.contains('|'.join(big_coins)) & ~df.fund_src_node2.str.contains('|'.join(big_coins)) & (df.fund_src_node3 == 'None')]

#df_uni = df.sort_values('expected_profit_usd').groupby('cycle_group').first()
#df_uni = df.sort_values('edge_count').groupby('cycle_group').first()
#df_uni = df.groupby('cycle_group').first()
df_uni = df
print 'got unique df'

flow_stats_uni = get_funds_flow_stats(df_uni)
print 'got flow stats'

refined_dfs = [(df_uni, flow_stats_uni)]
for i in range(5):
    df_to_refine, df_to_refine_funds_flow_stats = refined_dfs[-1]
    refined_df = refine_extreme_nodes(df_to_refine, df_to_refine_funds_flow_stats)
    refined_dfs.append((refined_df, get_funds_flow_stats(refined_df)))
    print i, refined_dfs[-2][0].shape[0], refined_dfs[-1][0].shape[0]
    if refined_dfs[-2][0].shape[0] == refined_dfs[-1][0].shape[0]:
        break


ref_df = refined_dfs[-1][0]
ref_df_nodes_agg = get_cross_node_group_stat(ref_df)

#df.sort_values('max_flow_rate', ascending=False)
#df[~df.cycle_desc.str.contains('XRP')].sort_values('max_flow_rate', ascending=False).head(10000)
#ref_df_nodes_agg.sort_values('limiting_count', ascending=False)
#ref_df[ref_df.src_nodes == frozenset(['USDT (Binance)', 'BNT (OKEX)'])].sort_values('lifetime_ms')


