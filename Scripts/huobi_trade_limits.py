import json

import requests
import time
import os

interval = 0.5
urls = {"symbols": "https://api.huobi.pro/v1/common/symbols",
        "limits": "https://api.huobi.pro/v1/common/exchange?symbol=%s"}

session = requests.session()
symbol_info = session.get(urls["symbols"]).json()["data"]

trade_limits_file = r"C:\Users\Arbitrator\Google Drive\Cryptage\huobi_trade_limits.json"
if os.path.exists(trade_limits_file):
    with open(trade_limits_file, "rb") as fid:
        trade_limits = json.load(fid)
else:
    trade_limits = {}

for info in symbol_info:
    symbol = info["symbol"]
    quote = info["quote-currency"]
    base = info["base-currency"]
    key = base + "-" + quote
    if key in trade_limits.keys():
        continue

    limits = session.get(urls["limits"] % symbol).json()["data"]
    print "Got trade limits for %s" % key
    trade_limits[key] = {"min_base_size": limits["market-sell-order-must-greater-than"],
                         "min_quote_size": limits["limit-order-must-greater-than"]}
    with open(trade_limits_file, "wb") as fid:
        json.dump(trade_limits, fid, indent=4)
    time.sleep(interval)


