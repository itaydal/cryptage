import urllib

import requests
import hmac
import hashlib
import json

import time

keys_path = r"C:\Users\Arbitrator\Documents\cryptage\Cryptage\Cryptage\ApiKeys.json"
keys = json.load(open(keys_path, "rb"))

api_secret = keys["Bittrex"]["ApiSecret"]
api_key = keys["Bittrex"]["ApiKey"]

if False:
    request_url = "https://bittrex.com/api/v2.0/key/market/tradesell?apikey=%s&nonce=1" % api_key
    data = {"ordertype": "market", "quantity": 0.013, "target": 1, "conditiontype": "none",
            "marketname": "ETH-BCH", "timeInEffect": "GOOD_TIL_CANCELLED", "rate": None}
else:
    request_url = "https://bittrex.com/api/v1.1/market/selllimit?apikey=%s&nonce=%d" % (api_key, 1)
    data = {"quantity": 0, "target": 1, "conditiontype": "none", "rate": 0.1, "market": "ETH-BCH",
            "timeInEffect": "GOOD_TIL_CANCELLED"}

request_url = request_url.encode() + "&" + urllib.urlencode(data)
apisign = hmac.new(api_secret.encode(), request_url, hashlib.sha512).hexdigest()

resp = requests.request(method="OPTIONS", url=request_url, headers={"apisign": apisign, "quantity": "0.1"}, timeout=10)
print resp
print resp.content
