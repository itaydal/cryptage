import time
from StringIO import StringIO

from websocket import create_connection
import json
import threading
import zlib
import gzip


def okex_sub(url):
    sub_msg = []
    for symbol in ["eth_btc"]:
        sub_msg += [{"event": "addChannel", "channel": "ok_sub_spot_%s_depth" % symbol}]
    return sub_msg


def okex_parser(ws, msgs, computer_time, name):
    for msg in msgs:
        channel = msg["channel"]
        if "depth" in channel:
            symbol = channel.replace("ok_sub_", "").replace("_depth", "")
            try:
                timestamp = int(msg["data"]["timestamp"]) / 1000.0

                delay = int(1000 * (computer_time - timestamp))
                print "%s (%d): %s: computer: %f, exchange: %f (%d)" % (
                    name, len(msgs), symbol, computer_time, timestamp, delay)
            except:
                pass


def hitbtc_sub(url):
    symbol = "BITSBTC"
    if "st." in url:
        return [1, "orderbook", symbol]
    else:
        return {"method": "subscribeOrderbook", "id": symbol, "params": {"symbol": symbol}}


hitbtc_totals = [{"asks": 0, "bids": 0}, {"asks": 0, "bids": 0}]


def hitbtc_parser(ws, msg, computer_time, name):
    if isinstance(msg, dict):
        if "result" in msg.keys():
            return
        bids = msg["params"]["bid"]
        asks = msg["params"]["ask"]
        seq = msg["params"]["sequence"]
    else:
        bids = msg[3]["bid"]
        asks = msg[3]["ask"]

    index = int(name)
    hitbtc_totals[index]["asks"] += len(asks)
    hitbtc_totals[index]["bids"] += len(bids)
    print "hitbtc: %s: computer: %f, bids: %d (%d), asks: %d (%d)" % \
          (name, computer_time, hitbtc_totals[index]["bids"], len(bids), hitbtc_totals[index]["asks"], len(asks))


def huobi_sub(url):
    symbol = "elfbtc"
    return {"id": symbol, "sub": "market." + symbol + ".depth.step0", "pick": ["bids.5", "asks.5"]}


def huobi_parser(ws, msg, computer_time, name):
    if "status" in msg.keys():
        return
    if "ping" in msg.keys():
        ws.send(json.dumps({"pong": msg["ping"]}))
        return

    timestamp = msg["ts"] / 1000.0
    delay = int(1000 * (computer_time - timestamp))
    print "%s: computer: %f, exchange: %f (%d)" % (
        name, computer_time, timestamp, delay)


info = {"okex": {"urls": ["wss://real.okex.com:10441/websocket",
                          # "wss://real.okex.com:10440/websocket",
                          # "wss://okexcomreal.bafang.com:10441/websocket",
                          "wss://okexcomrealtest.bafang.com:10441/websocket",
                          ],
                 "sub": okex_sub,
                 "parser": okex_parser, "compressed": False},
        "hitbtc": {"urls": ["wss://st.hitbtc.com",
                            "wss://api.hitbtc.com/api/2/ws"
                            ],
                   "sub": hitbtc_sub,
                   "parser": hitbtc_parser, "compressed": False},
        "huobi": {"urls": ["wss://www.huobi.com/-/s/pro/ws",
                           "wss://api.huobi.pro/ws"
                           ],
                  "sub": huobi_sub,
                  "parser": huobi_parser, "compressed": True
                  }
        }

exchange = "huobi"
num_websockets = len(info[exchange]["urls"])


def listener(name):
    url = info[exchange]["urls"][int(name)]
    parser = info[exchange]["parser"]
    ws = create_connection(url)
    ws.send(json.dumps(info[exchange]["sub"](url)))
    while True:
        data = ws.recv()
        computer_time = time.time()
        if info[exchange]["compressed"]:
            data = gzip.GzipFile(fileobj=StringIO(data), mode="rb").read()
        parser(ws, json.loads(data), computer_time, name)


tt = []
for i in range(num_websockets):
    t = threading.Thread(target=listener, args=(str(i),))
    t.start()
    tt += [t]
    #time.sleep(0.25)

for t in tt:
    t.join()
